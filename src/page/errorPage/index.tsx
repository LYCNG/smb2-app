import { useContext, useEffect } from 'react';
import { useTranslation } from 'react-i18next';
import { useNavigate } from 'react-router-dom';
import { AuthContext, OfficeType } from '../../provider/authProvider';



function ErrorPage() {
    const {
        auth,
        logout,
        changeOffice
    } = useContext(AuthContext);
    const { t } = useTranslation();
    const navigate = useNavigate();

    useEffect(() => { 
        localStorage.clear();
        logout()
        changeOffice(OfficeType.NONE);
        setTimeout(() => navigate("/"), 2000);
    }, []);
  return (
      <div>
          頁面跳轉中，若3秒沒挑轉請<a href="login">按此</a>重新登入。
      </div>
  )
}

export default ErrorPage;