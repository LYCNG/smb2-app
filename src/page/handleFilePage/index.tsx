import React, { useState,useRef,useEffect  } from 'react';

import { Box, Button, Divider,  Paper, Stack } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import UploadFileOutlinedIcon from '@mui/icons-material/UploadFileOutlined';
import { makeStyles } from '@mui/styles';
import RestartAltOutlinedIcon from '@mui/icons-material/RestartAltOutlined';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    "& .MuiTableCell-root": {
      borderLeft: "1px solid rgba(224, 224, 224, 1)"
    }
  }
});

function HandleFilePage() {
    const classes = useStyles();
    const [file, setFile] = useState<File|null>(null);
    const [array, setArray] = useState<{ [index: string]: string }[]>([]);
    const [fileName, setFileName] = useState<string>("");
    const csvInputRef = useRef<HTMLInputElement | null>(null);

    const fileReader = new FileReader();

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files) {
            setFileName(e.target.files[0].name)
            setFile(e.target.files[0]);
            return;
         };
    };

    const handleInputClick = () => { 
        if (csvInputRef && csvInputRef.current) {
            csvInputRef.current.click();
        };
    };

    const csvFileToArray = (string: string) => {

        const csvHeader = string.slice(0, string.indexOf("\n")).split(",");
     
        const csvRows = string.slice(string.indexOf("\n") + 1).split("\n");

        const array = csvRows.map(i => {
            const values = i.split(",");
            const obj = csvHeader.reduce((object:{[index: string]:string}, header, index) => {
                object[header] = values[index].replaceAll(`"`,'');
                return object;
            }, {});
            return obj;
        });
        console.log(array)
        setArray(array);
    };

    const handleOnSubmit = () => {
    
        if (file) {
            fileReader.onload = function (event) {
                const csvOutput = event?.target?.result;
                csvFileToArray(csvOutput as string);
            };
            fileReader.readAsText(file);
        };
    };

    const headerKeys = Object.keys(Object.assign({}, ...array));

    const handleFileClear = () => { 
        setFile(null);
        setFileName('');
        setArray([]);
        return;
    };

    useEffect(() => { 
        if (file) { 
            handleOnSubmit();
        };
    }, [file]);

    return (
        <Paper sx={{ width: "90%", m: 'auto', p: 2 }}>
            <Stack sx={{border:1,p:2}} direction="row" alignItems="center" justifyContent={"space-between"}>
                <input
                    ref={csvInputRef}
                    style={{ display: 'none' }}
                    type={"file"}
                    id={"csvFileInput"}
                    accept={".csv"}
                    onChange={handleOnChange}
                />
                <Button
                    variant="contained"
                    color="success"
                    sx={{width:'10%'}}
                    startIcon={<UploadFileOutlinedIcon />}
                    onClick={handleInputClick}
                >
                    匯入CSV
                </Button>
                <Button
                    variant="contained"
                    color="error"
                    sx={{width:'8%'}}
                    startIcon={<RestartAltOutlinedIcon  />}
                    onClick={handleFileClear}
                >
                    清除
                </Button>
               
            </Stack>
             <p style={{ marginLeft: 5 }}>
                    File_Name: {fileName}
                </p>
            <Divider />
            {array.length === 0 && 
                <Box
                    onClick={handleInputClick}
                    sx={{ border: 1, borderColor: '#9E9E9E', fontSize: 24, fontWeight: 'bold', color: '#747373', width: '100%', height: 300, textAlign: 'center', m: 'auto', pt: 20 }}>
                    請先上傳csv檔案。
                </Box>}
            {array.length > 0 &&
                <TableContainer component={Paper} sx={{ maxWidth: 1200, overflow: 'auto', mt: 5 }}>
                    <Table size="small" aria-label="csv-demo-table" className={classes.table}>
                    <TableHead sx={{bgcolor:'green'}}>
                        <TableRow>
                            {headerKeys.map((key,index) => (
                                <TableCell key={`header-` + index}
                                    sx={{ color: 'white', minWidth: key.length * 25 }}
                                >
                                    {key}
                                </TableCell>
                            ))}
                        </TableRow>
                    </TableHead>
                    <TableBody>
                        {array.map((item, index) =>
                            <TableRow key={index}>
                                {Object.values(item).map((val,cindex) => (
                                    <TableCell key={`row-`+index+`-c`+cindex}>{val}</TableCell>
                                ))}
                            </TableRow>
                        )}
                    </TableBody>
                </Table>
            </TableContainer>}
        </Paper>


        // <table>
        //     <thead>
        //     <tr key={"header"}>
                // {headerKeys.map((key) => (
                // <th>{key}</th>
                // ))}
        //     </tr>
        //     </thead>

        //     <tbody>
        //     {array.map((item) => (
        //         <tr key={item.id}>
        //         {Object.values(item).map((val) => (
        //             <td>{val}</td>
        //         ))}
        //         </tr>
        //     ))}
        //     </tbody>
        // </table>
        // </div>
    );
};

export default HandleFilePage;