import { Box, IconButton } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import OpenInNewOutlinedIcon from '@mui/icons-material/OpenInNewOutlined';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import { TFunction } from 'react-i18next';

export const WaitColumns = (
    handleCheck: (id: string) => void,
    handleDelete: (id: string) => void,
    t:TFunction<"translation", undefined>,
): GridColDef[] => [
  { field: 'id', headerName: 'ID', width: 90 ,align:'center',headerAlign:'center'},
  
  {
    field: 'datetime',
      headerName: t("operator.pickingTable.date"),
    align: 'right',
     headerAlign:'right',
    width: 200,
  },

  {
        field: 'orderid',
        headerName: t("operator.pickingTable.oddNumber"),
        align: 'right',
        headerAlign: 'right',
        width: 250,
    },
     {
        field: 'type',
        headerName: t("operator.pickingTable.type"),
        align: 'right',
         headerAlign: 'right',
         valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)
    },
    {
        field: 'applicant',
        headerName: t("operator.pickingTable.filler"),
        align: 'right',
        headerAlign:'right',
        width: 150,

    },
   {
        field: 'status',
        headerName: t("operator.pickingTable.status"),
        align: 'right',
       headerAlign: 'right',
        valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)
    },

    {
        field: 'action',
        headerName: t("operator.pickingTable.action"),
        align: 'right',
        headerAlign: 'right',
        width:150,
        renderCell: (params: GridValueGetterParams) => {
            const { orderid} = params.row;
            return (
                <div>
                     <IconButton color="success" onClick = {()=> handleCheck(orderid)}>
                        <OpenInNewOutlinedIcon/>
                    </IconButton>
                    /
                    <IconButton color="error" onClick={()=>handleDelete(orderid)}>
                        <DeleteOutlineOutlinedIcon />
                    </IconButton>
                </div>
            )
        }
    }
];


export const PendingColumns = (
      t:TFunction<"translation", undefined>,
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 90, align: 'center', headerAlign: 'center' },
    { field: 'handletime', headerName: t("operator.warehouseReceiveTable.date"), width: 180 ,align:'center',headerAlign:'center'},
    {
        field: 'taker',
        headerName: t("operator.warehouseReceiveTable.odder"),
        align: 'right',
        headerAlign:'right',
        width: 150,

    },
    {
        field: 'datetime',
        headerName: t("operator.warehouseReceiveTable.fillDate"),
        align: 'right',
        headerAlign:'right',
        width: 200,
   
    },

    {
        field: 'orderid',
        headerName: t("operator.warehouseReceiveTable.oddNumber"),
        align: 'right',
        headerAlign: 'right',
        width: 200,
    },

    {
        field: 'type',
        headerName: t("operator.warehouseReceiveTable.type"),
        align: 'right',
        headerAlign: 'right',
          valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)
    },
    {
        field: 'applicant',
        headerName: t("operator.warehouseReceiveTable.filler"),
        align: 'right',
        headerAlign:'right',
        width: 120,

    },
    {
        field: 'status',
        headerName: t("operator.warehouseReceiveTable.status"),
        align: 'right',
        headerAlign: 'right',
        width: 120,
          valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)
    },
];



export const FinishColumns = (
      t:TFunction<"translation", undefined>,
): GridColDef[] => [
{ field: 'id', headerName: 'ID', width: 50, align: 'center', headerAlign: 'center' },
    { field: 'finishdate', headerName: t("operator.receiveCompleteTable.date"), width: 150 ,align:'center',headerAlign:'center'},
    {
        field: 'handler',
        headerName: t("operator.receiveCompleteTable.handler"),
        align: 'right',
        headerAlign:'right',
        width: 120,

    },
    {
        field: 'datetime',
        headerName: t("operator.receiveCompleteTable.oddDate"),
        align: 'right',
        headerAlign:'right',
        width: 150,
    },
    {
        field: 'taker',
        headerName: t("operator.receiveCompleteTable.odder"),
        align: 'right',
        headerAlign:'right',
        width: 120,
    },
    {
        field: 'handletime',
        headerName: t("operator.receiveCompleteTable.fillDate"),
        align: 'right',
        headerAlign:'right',
        width: 150,
    },
    {
        field: 'orderid',
        headerName: t("operator.receiveCompleteTable.oddNumber"),
        align: 'right',
        headerAlign:'right',
        width: 150,
    },
    {
        field: 'type',
        headerName: t("operator.receiveCompleteTable.type"),
        align: 'right',
        headerAlign:'right',
        width: 90,
         valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)
    },
    {
        field: 'applicant',
        headerName: t("operator.receiveCompleteTable.filler"),
        align: 'right',
        headerAlign:'right',
        width: 120,
    },
    {
        field: 'status',
        headerName: t("operator.receiveCompleteTable.status"),
        align: 'right',
        headerAlign:'right',
        width: 90,
        valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)
    },
];