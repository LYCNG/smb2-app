import styled from "@emotion/styled";
import AddBoxSharpIcon from '@mui/icons-material/AddBoxSharp';
import AssignmentTurnedInOutlinedIcon from '@mui/icons-material/AssignmentTurnedInOutlined';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import TabContext from '@mui/lab/TabContext';
import TabPanel from '@mui/lab/TabPanel';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, IconButton, Toolbar } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FaListAlt, FaRegListAlt } from 'react-icons/fa';
import { RiLuggageDepositFill } from 'react-icons/ri';
import { useNavigate } from 'react-router-dom';
import api from '../../api';
import CheckDataLog, { CheckListType } from '../../components/check-data-modal';
import { FinishColumns, PendingColumns, WaitColumns } from './column';

const Item = styled.div`
    background:white;
    text-align:center;
    height:600px;
`;

const ItemTitle = styled.div`
    font-size:1.5em;
    margin:auto;
    width:"100%";
    display: flex;
    justifyContent: space-between;
     align-items: center;
     max-height:50px
`;

const GridBox = (props: {
    rows: any[],
    columns:GridColDef[];
}) => {
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('sm'));
    return (
        <Box
            sx={{
                pt: 2, m: 'auto', height: 550, width: { xs: '100%', sm: '100%', md: '95%', lg: '95%' },
                '& .MuiDataGrid-columnHeaders': { bgcolor: "#5D6D7E",color:'white' }
            }}
        >
            <DataGrid
                sx={{ 
                    fontSize:18,
                    '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                        outline: 'none',
                    },
                }}
                density={small?'compact':"comfortable"}
                rows={props.rows}
                columns={props.columns}
                pageSize={5}
                rowsPerPageOptions={[5]}
                experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
    );
}


interface OperatorTableType{
    waitInTable: any[];
    pendingTable: any[];
    waitOutTable: any[];
    finishTable: any[];
};

const initTable: OperatorTableType = {
    waitInTable: [],
    pendingTable: [],
    waitOutTable: [],
    finishTable: []
};


function MainOperate() {

    const navigate = useNavigate();
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();

    const [tabValue, setTabValue] = React.useState("1");
    const [deleteTarget, setDeleteTarget]
        = useState<string>("");
    const [operatorTable, setOperatorTable]
        = useState<OperatorTableType>(initTable);
    const [checkData, setCheckData]
        = useState<CheckListType | null>(null);
  
    const handleCheck = (orderId: string) => getCheckOrder(orderId);
    
    const handleCheckDelete = (orderId: string) => setDeleteTarget(orderId);
    
    const handleChangeTab = (event: React.SyntheticEvent, newValue: string) => { 
        setTabValue(newValue);
    };

    const mapKey = (data: any[]) => {
        return data.map((item, index) => {
            return {
                ...item, id: index
            };
        })
    };

    //api
     const getCheckOrder = async (orderId:string) => { 
        try {
            const res = await api.get("api/operate/orders/wait/"+orderId);
            const data = await  res.data;
            setCheckData({ orderId: orderId,data:data });
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar(err.message, { variant: 'error' });
        }
    };
    const getOperationData = async () => {
        try {
            const res = await api.get("/api/operate/orders");
            const { wait_in, wait_out, pending, finish } = await res.data;
            setOperatorTable({
                waitInTable: mapKey(wait_in),
                pendingTable: mapKey(pending),
                waitOutTable: mapKey(wait_out),
                finishTable: mapKey(finish)
            });
         
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("讀取資料失敗",{variant:"error"})
        }
     };
     const handleDelete = async (order_id:string) => {
        try {
            await api.delete(`/api/operate/wait/${order_id}`);
            enqueueSnackbar("刪除成功", { variant: "success" });
            return setDeleteTarget("");
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("刪除失敗", { variant: "error" });
        }
    };
    useEffect(() => { 
        getOperationData();
    }, []);

    return (
        <Box sx={{ bgcolor: 'white',width:'100%',height:"100vh"}}>
            <Toolbar sx={{pt:2,pb:2}}>
                <Typography variant='h4' component={"div"} sx={{fontWeight:'bold'}}>
                    OP主頁面
                </Typography>
            </Toolbar>
            <Divider />
            <Box>
                <TabContext value={tabValue} >
                 <Tabs
                    value={tabValue}
                    onChange={handleChangeTab}
                     variant="scrollable"
                        scrollButtons
                          allowScrollButtonsMobile
                    aria-label="scrollable auto tabs"
                    sx={{ border: 1, borderColor: 'divider' }}
                >
                    <Tab label={t("operator.picking") } value="1"   sx={{fontSize:'1em',fontWeight:'bold'}}/>
                    <Tab label={ t("operator.treasury")} value="2" sx={{fontSize:'1em',fontWeight:'bold'}}/>
                    <Tab label={ t("operator.warehouseReceive")} value="3" sx={{fontSize:'1em',fontWeight:'bold'}}/>
                    <Tab label={ t("operator.receiveComplete")} value="4" sx={{fontSize:'1em',fontWeight:'bold'}}/>
                </Tabs>
                 <TabPanel value="1">
                    <Item>
                        <ItemTitle >
                            <h3 style={{ marginLeft: 40 }}>
                                <FaListAlt fontSize={"large"} style={{ marginRight: '8px' }} />
                                {t("operator.picking")}
                            </h3>
                            <IconButton color="warning" onClick={()=>navigate("/operator/picking")}>
                                <AddBoxSharpIcon fontSize='large'/>
                            </IconButton>
                        </ItemTitle>
                        <GridBox rows={operatorTable.waitOutTable} columns={WaitColumns( handleCheck,handleCheckDelete,t)} />
                    </Item>
                </TabPanel>
                <TabPanel value="2">
                     <Item>
                        <ItemTitle>
                                <h3 style={{ marginLeft: 40 }}>
                                    <RiLuggageDepositFill style={{ marginRight: '8px' }} />
                                    {t("operator.treasury")}
                                </h3>
                            <IconButton color="warning" onClick={()=>navigate("/operator/treasury")}>
                                <AddBoxSharpIcon fontSize='large'/>
                            </IconButton>
                         </ItemTitle>
                       <GridBox rows={operatorTable.waitInTable} columns={WaitColumns(handleCheck,handleCheckDelete,t)} />
                    </Item>
                </TabPanel>
                <TabPanel value="3">
                    <Item>
                         <ItemTitle>
                                <h3 style={{ marginLeft: 40 }}>
                                    <FaRegListAlt fontSize={"large"} style={{ marginRight: '8px' }} />
                                    {t("operator.warehouseReceive")}
                                </h3>
                        </ItemTitle>
                        <GridBox rows={operatorTable.pendingTable} columns={PendingColumns(t)} /> 
                    </Item> 
                </TabPanel>
                <TabPanel value="4">
                     <Item>
                        <ItemTitle>
                                <h3 style={{ marginLeft: 40 }}>
                                    <AssignmentTurnedInOutlinedIcon style={{ marginRight: '8px' }} />
                                    {t("operator.receiveComplete")}
                                </h3>
                         </ItemTitle>
                         <GridBox rows={operatorTable.finishTable} columns={FinishColumns(t)} />
                    </Item>
                    </TabPanel>
                </TabContext>
            </Box>
            <CheckDataLog checkData={checkData} handleClose={() => setCheckData(null)} />
            <RemoveDialog targetId={deleteTarget} handleClose={()=>setDeleteTarget("")} nextFunction={()=> handleDelete(deleteTarget) } />
        </Box>
    );
};

export default MainOperate;

const RemoveDialog = (props: {
    targetId: string;
    nextFunction:()=>void;
    handleClose: () => void;
}) => { 
    const { targetId,nextFunction,handleClose} = props;
    return (
        <Dialog
            open={Boolean(targetId)}
            onClose={handleClose}
            id="check-logout-modal"
            maxWidth={"xs"}
            fullWidth={true}
        >
            <DialogTitle id="remove-alert-title" sx={{display:'flex',alignItems:'center',bgcolor:"#5D6D7E",color:'white',fontWeight:'bold'}}>
                <ErrorOutlineOutlinedIcon color="warning" fontSize='large' sx={{mr:2}} /> 刪除警告
            </DialogTitle>
            <DialogContent sx={{m:'auto',mt:3}}>
                <Typography variant='h5' component={'div'} sx={{color:'red',fontWeight:'bold'}}>
                     確定刪除: { targetId} ?
                </Typography>
            </DialogContent>
            <DialogActions id="remove-alert-action">
                <Button onClick={handleClose} autoFocus variant='outlined'>
                    取消
                </Button>
                <Button onClick={nextFunction} color="error">確定刪除</Button>
            </DialogActions>
        </Dialog>
    );
};

