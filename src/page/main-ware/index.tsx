import styled from "@emotion/styled";
import ArrowForwardOutlinedIcon from '@mui/icons-material/ArrowForwardOutlined';
import AssignmentTurnedInOutlinedIcon from '@mui/icons-material/AssignmentTurnedInOutlined';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import TabContext from '@mui/lab/TabContext';
import TabPanel from '@mui/lab/TabPanel';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, Toolbar } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import Tab from '@mui/material/Tab';
import Tabs from '@mui/material/Tabs';
import Typography from '@mui/material/Typography';
import useMediaQuery from '@mui/material/useMediaQuery';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { FaListAlt, FaRegListAlt } from 'react-icons/fa';
import { RiLuggageDepositFill } from 'react-icons/ri';
import { useNavigate } from 'react-router-dom';
import api from '../../api';
import CheckDataLog, { CheckListType } from '../../components/check-data-modal';
import PrintDialog, { OrderType } from '../../components/print-card-modal';
import { WareContext } from '../../provider/wareProvider/index';
import { FinishColumns, PendingOutColumns, WaitColumns } from "./column";

const Item = styled.div`
    background:white;
    text-align:center;
    height:600px;
`;
const ItemTitle = styled.div`
    font-size:1.5em;
    margin:auto;
    width:"100%";
    display: flex;
    justifyContent: space-between;
     align-items: center;
     max-height:50px
`;
const GridBox = (props: {
  rows: any[],
  columns: GridColDef[];
}) => {
  const theme = useTheme();
  const small = useMediaQuery(theme.breakpoints.down('sm'));
  return (
    <Box
      sx={{
        pt: 2, m: 'auto', height: 550, width: { xs: '100%', sm: '100%', md: '95%', lg: '95%' },
        '& .MuiDataGrid-columnHeaders': { bgcolor: "#5D6D7E", color: 'white' }
      }}
    >
      <DataGrid
        sx={{
          fontSize: 18,
          '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
            outline: 'none',
          },
        }}
        density={small ? 'compact' : "comfortable"}
        rows={props.rows}
        columns={props.columns}
        pageSize={15}
        rowsPerPageOptions={[5]}
        // showCellRightBorder
        experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  );
};

interface WareTableType{
    waitTable: any[];
    pendingInTable: any[];
    pendingOutTable: any[];
    finishTable: any[];
};
const initTable: WareTableType = {
    waitTable: [],
    pendingInTable: [],
    pendingOutTable: [],
    finishTable: []
};

type DataType  = "management" | "operate";

function MainWare() {
  const navigate = useNavigate();
  const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();

  const { transport } = useContext(WareContext);
  //資料查看對照表
  const pathData = {
    "management": "api/management/orders/",
    "operate": "api/operate/orders/wait/"
  };

  //state---------------------------------------------------
  const [tabValue, setTabValue] = React.useState("1");
  
  const [printData, setPrintData] = useState<OrderType | null>(null);
  
  const [deleteTarget, setDeleteTarget]
    = useState<string>("");
  
  const [wareTable, setWareTable]
    = useState<WareTableType>(initTable);
 
  const [checkData, setCheckData]
    = useState<CheckListType | null>(null);
  
  const [checkType, setCheckType] = useState<string | null>(null);
  
  //function----------------------------------------------
  /**變換 tab index */
  const handleChangeTab = (event: React.SyntheticEvent, newValue: string) => { 
        setTabValue(newValue);
  };
  
  const handleCheckDelete = (orderId: string) => setDeleteTarget(orderId);


  const handleNavigate = (dataType:string,orderId:string,type:string) => { 
    //導向頁面stock in ,order,stock out
    console.log(dataType)
    const pack = {
      orderId: orderId,
      type:type
    };
    
    if (dataType === 'operate') {
      return transport("order", pack);
    };
    if (dataType === 'management') { 
      return transport('stockin', pack);
    };
    if (dataType === 'stockOut') { 
      return transport("stockout", pack);
    };
  };

  const handlePrint = async (data: any) => {
    const { orderid, type } = data;
    const res = await api.get(`/api/print/${type}/${orderid}`);
    const inAction = res.data.filter((ele: any) => ele.action === 'in')[0];
    const { product_name, product_number, location, quantity, flag_number } = inAction;
    setPrintData({
      date: data.datetime,
      location: location,
      flag_number: flag_number,
      product_name: product_name,
      product_number: product_number,
      quantity: quantity,
      type: data.type
    });
  };
  /**陣列插入key:id */
  const mapKey = (data: any[]) => {
    return data.map((item, index) => {
      return {
        ...item, id: index
      };
    });
  };
  //api-----------------------------------------------------
  
  const handleDelete = async (order_id:string) => {
    try {
      if (tabValue === '1') {
           await api.delete(`/api/operate/wait/${order_id}`);
      } else {
         await api.delete(`/api/management/pending/${order_id}`); 
      };
        enqueueSnackbar("刪除成功", { variant: "success" });
    } catch (err: any) {
        console.log(err.message);
        enqueueSnackbar("刪除失敗", { variant: "error" });
    }
  };
  /**設定查詢資料類別 */
  const handleCheck = async (dataType: DataType, orderId: string, type: string) => { 
        if (dataType === "management") { 
            setCheckType(type);
        };
        try {   
            let path = pathData[dataType];
          const res = await api.get(path + orderId);
          const data = await res.data;//get data list
          setCheckData({ orderId: orderId, data: data });
            // setCheckData({ ...data, orderId: orderId });
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar(err.message, { variant: 'error' });
        }
    };
  /**讀取倉儲資料 */
  const getDataByAsync = async () => {
    try {
      const res = await api.get("/api/management/orders");
        const { wait, pending_in, pending_out, finish } = res.data;
        setWareTable({
            waitTable: mapKey(wait),
            pendingInTable: mapKey(pending_in),
            pendingOutTable: mapKey(pending_out),
            finishTable: mapKey(finish)
        });
        return;
    } catch (err: any) {
        return enqueueSnackbar(err.message, { variant: "error" });
    };
  };
  //useEffect--------------------------------------
  useEffect(() => {
    getDataByAsync();
  }, []);
  
  return (
      <Box sx={{ bgcolor: 'white', height: "100vh" }}>
        <Toolbar sx={{ pt: 2, pb: 2 }}>
          <Typography variant='h4' component={"div"} sx={{ fontWeight: 'bold' }}>
            倉管主頁面
          </Typography>
        </Toolbar>
        <Divider />
        <Box>
          <TabContext value={tabValue} >
            <Tabs
              value={tabValue}
              onChange={handleChangeTab}
              variant="scrollable"
              scrollButtons
              allowScrollButtonsMobile
              aria-label="scrollable auto tabs"
              sx={{ border: 1, borderColor: 'divider' }}
            >
              <Tab label={t("warehouse.main.orderList")} value="1" sx={{ fontSize: '1em', fontWeight: 'bold' }} />
              <Tab label={t("warehouse.main.stockInList")} value="2" sx={{ fontSize: '1em', fontWeight: 'bold' }} />
              <Tab label={t("warehouse.main.stockOutList")} value="3" sx={{ fontSize: '1em', fontWeight: 'bold' }} />
              <Tab label={t("warehouse.main.finishList")} value="4" sx={{ fontSize: '1em', fontWeight: 'bold' }} />
            </Tabs>
            <TabPanel value="1">
              <Item>
                <ItemTitle><FaListAlt fontSize={"large"} style={{ marginRight: '8px' }} />
                  <h3>{t("warehouse.main.orderList")}</h3>
                  <Button
                    onClick={() => navigate("order")}
                    sx={{ ml: 2 }} size="small" variant="contained"
                    endIcon={<ArrowForwardOutlinedIcon />}
                  >
                    Go
                  </Button>
                </ItemTitle>
                <GridBox rows={wareTable.waitTable}
                  columns={WaitColumns("operate", handleCheck, handleCheckDelete, t, handleNavigate)} />
              </Item>
            </TabPanel>
            <TabPanel value="2">
              <Item>
                <ItemTitle>
                  <RiLuggageDepositFill style={{ marginRight: '8px' }} />
                  <h3>{t("warehouse.main.stockInList")}</h3>
                  <Button
                    onClick={() => navigate("stockin")}
                    sx={{ ml: 2 }} size="small" variant="contained"
                    endIcon={<ArrowForwardOutlinedIcon />}
                  >
                    Go
                  </Button>
                </ItemTitle>
                <GridBox rows={wareTable.pendingInTable}
                  columns={WaitColumns("management", handleCheck, handleCheckDelete, t, handleNavigate, handlePrint)} />
              </Item>
            </TabPanel>
            <TabPanel value="3">
              <Item>
                <ItemTitle>
                  <FaRegListAlt fontSize={"large"} style={{ marginRight: '8px' }} />
                  <h3>{t("warehouse.main.stockOutList")}</h3>
                </ItemTitle>
                <GridBox rows={wareTable.pendingOutTable} columns={PendingOutColumns(handleCheckDelete, handleCheck, t,handleNavigate)} />
              </Item>
            </TabPanel>
            <TabPanel value="4">
              <Item>
                <ItemTitle>
                  <AssignmentTurnedInOutlinedIcon fontSize={"large"} style={{ marginRight: '8px' }} />
                  <h3> {t("warehouse.main.finishList")}</h3>
                </ItemTitle>
                <GridBox rows={wareTable.finishTable} columns={FinishColumns(t)} />
              </Item>
            </TabPanel>
          </TabContext>
        </Box>
        {printData && <PrintDialog open={Boolean(printData)} handleClose={() => setPrintData(null)} order={printData} />}
        <CheckDataLog checkData={checkData} handleClose={() => { setCheckData(null); setCheckType(null) }} type={checkType} />
        <RemoveDialog targetId={deleteTarget} handleClose={() => setDeleteTarget("")} nextFunction={() => handleDelete(deleteTarget)} />
      </Box>
  );
};

export default MainWare;


const RemoveDialog = (props: {
    targetId: string;
    nextFunction:()=>void;
    handleClose: () => void;
}) => { 
    const { targetId,nextFunction,handleClose} = props;
    return (
        <Dialog
            open={Boolean(targetId)}
            onClose={handleClose}
            id="check-logout-modal"
            maxWidth={"xs"}
            fullWidth={true}
        >
            <DialogTitle id="remove-alert-title" sx={{display:'flex',alignItems:'center',bgcolor:"#5D6D7E",color:'white',fontWeight:'bold'}}>
                <ErrorOutlineOutlinedIcon color="warning" fontSize='large' sx={{mr:2}} /> 刪除警告
            </DialogTitle>
            <DialogContent sx={{m:'auto',mt:3}}>
                <Typography variant='h5' component={'div'} sx={{color:'red',fontWeight:'bold'}}>
                     確定刪除: { targetId} ?
                </Typography>
            </DialogContent>
            <DialogActions id="remove-alert-action">
                <Button onClick={handleClose} autoFocus variant='outlined'>
                    取消
                </Button>
                <Button onClick={nextFunction} color="error">
                  確定刪除
                </Button>
            </DialogActions>
        </Dialog>
    );
};





