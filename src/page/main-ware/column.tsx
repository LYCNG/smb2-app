import ArrowForwardSharpIcon from '@mui/icons-material/ArrowForwardSharp';
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import OpenInNewOutlinedIcon from '@mui/icons-material/OpenInNewOutlined';
import { IconButton } from '@mui/material';
import { GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { TFunction } from 'react-i18next';

// warehouse.main
type DataType = "management" | "operate";

export const WaitColumns = (
    dataType:DataType,
    handleCheck: (dataType:DataType,id: string,type:string) => void,
    handleDelete: (id: string) => void,
    t: TFunction<"translation", undefined>,
    handleNavigate:(dataType:string,orderId:string,type:string)=>void,
    handlePrint?: (data: any) => void,
    
): GridColDef[] => [
  { field: 'id', headerName: t("warehouse.main.no"), width: 90 ,align:'center',headerAlign:'center'},
  
  {
    field: 'datetime',
      headerName: t("warehouse.main.date"),
    align: 'right',
     headerAlign:'right',
    width: 200,
  },

  {
        field: 'type',
        headerName: t("warehouse.main.type"),
        align: 'right',
        headerAlign: 'right',
      width: 120,
             valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)
        },
    {
        field: 'status',
        headerName: t("operator.pickingTable.status"),
        align: 'right',
        headerAlign: 'right',
        width: 120,
          valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)
    },
{
        field: 'orderid',
        headerName: t("warehouse.main.order"),
        align: 'right',
        headerAlign:'right',
        width: 250,
    },
    {
        field: 'applicant',
        headerName: t("operator.pickingTable.filler"),
        align: 'right',
        headerAlign:'right',
        width: 120,

    },
    {
        field: 'print',
        headerName: t("warehouse.main.print"),
        align: 'right',
        headerAlign:'right',
        width: 120,
        hide:!Boolean(handlePrint),
        renderCell: (params: GridValueGetterParams) => {
            const data = params.row;
            return (
                <IconButton onClick={()=> handlePrint && handlePrint(data)} color="primary">
                    <LocalPrintshopOutlinedIcon />
                </IconButton>
            );
        }
    },

    {
        field: 'action',
        headerName: t("warehouse.main.action"),
        align: 'right',
        headerAlign: 'right',
        width:150,
        renderCell: (params: GridValueGetterParams) => {
            const { orderid,type} = params.row;
            return (
                <div>
                     <IconButton color="success" onClick = {()=> handleCheck(dataType,orderid,type)}>
                        <OpenInNewOutlinedIcon/>
                    </IconButton>
                    /
                    <IconButton color="error" onClick={()=>handleDelete(orderid)}>
                        <DeleteOutlineOutlinedIcon />
                    </IconButton>
                </div>
            )
        }
    },
    {
        field: 'navigate',
        headerName: t("warehouse.main.navigate"),
        align: 'right',
        headerAlign: 'right',
         renderCell: (params: GridValueGetterParams) => {
             const { orderid, type } = params.row;
            //  console.log(dataType, orderid);
            return (
                <div>
                    <IconButton color="primary" onClick = {()=> handleNavigate(dataType,orderid,type)}>
                        <ArrowForwardSharpIcon />
                    </IconButton>
                </div>
            )
        }
    }
];

// translate對應表: "no"|"date"|"order"|"status"|"filler"|"type"|"mcode"|"delete";

export const PendingOutColumns = (
    handleDelete: (id: string) => void,
     handleCheck: (dataType:DataType,id: string,type:string) => void,
    t: TFunction<"translation", undefined>,
    handleNavigate:(dataType:string,orderId:string,type:string)=>void
): GridColDef[] => [
    { field: 'id', headerName: t("warehouse.main.no"), width: 90, align: 'center', headerAlign: 'center' },
    { field: 'datetime', headerName: t("warehouse.main.date"), width: 250 ,align:'center',headerAlign:'center'},
    {
        field: 'orderid',
        headerName: t("warehouse.main.order"),
        align: 'right',
        headerAlign:'right',
        width: 250,

    },
    {
        field: 'status',
        headerName: t("warehouse.main.status"),
        align: 'right',
        headerAlign:'right',
        width: 120,
        valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)
    },

    {
        field: 'machine_code',
        headerName: t("warehouse.main.mcode"),
        align: 'right',
        headerAlign: 'right',
        width: 120,
    },

    {
        field: 'applicant',
        headerName: t("warehouse.main.filler"),
        align: 'right',
        headerAlign: 'right',
        width: 120,
    },
    {
        field: 'delete',
        headerName: t("warehouse.main.action"),
        align: 'right',
        headerAlign:'right',
        width: 150,
        renderCell: (params: GridValueGetterParams) => {
            const { orderid } = params.row;
            return (
                <>
                    <IconButton color="success" onClick = {()=> handleCheck("management",orderid,"out")}>
                        <OpenInNewOutlinedIcon/>
                    </IconButton>
                    /
                    <IconButton onClick={()=>handleDelete(orderid)} color="error">
                        <DeleteOutlineOutlinedIcon  />
                    </IconButton>
                </>
            );
        }
        },
        {
         field: 'navigate',
        headerName: t("warehouse.main.navigate"),
        align: 'right',
        headerAlign: 'right',
         renderCell: (params: GridValueGetterParams) => {
             const { orderid, type } = params.row;
            //  console.log(dataType, orderid);
            return (
                <div>
                    <IconButton color="primary" onClick = {()=> handleNavigate('stockOut',orderid,type)}>
                        <ArrowForwardSharpIcon />
                    </IconButton>
                </div>
            )
        }
    }
];


//finish 對應表 "no"|"order"|"finishDate"|"handler"|"status"|"type";
export const FinishColumns = (
      t:TFunction<"translation", undefined>,
): GridColDef[] => [
    { field: 'id', headerName: t("warehouse.main.no"), width: 80, align: 'center', headerAlign: 'center' },
    {
        field: 'orderid',
        headerName: t("warehouse.main.order"),
        align: 'right',
        headerAlign:'right',
        width: 250,
    },
    {
        field: 'type',
        headerName: t("warehouse.main.type"),
        align: 'right',
        headerAlign:'right',
        width: 150,
             valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)
    },
        {
            field: 'finishdate',
            headerName: t("warehouse.main.finishDate"),
            width: 250,
            align: 'right',
            headerAlign: 'right'
        },
    {
        field: 'handler',
        headerName: t("warehouse.main.handler"),
        align: 'right',
        headerAlign:'right',
        width: 150,

    },
    {
        field: 'status',
        headerName: t("warehouse.main.status"),
        align: 'right',
        headerAlign:'right',
        width: 150,
            valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)
        },
    {
        field: 'delete',
        headerName: `${t(`warehouse.delete`)} / ${t(`warehouse.print`)}`,
        align: 'right',
        headerAlign:'right',
        width: 150,
        hide:true,
        renderCell: (params: GridValueGetterParams) => {
            const { orderid } = params.row;
            return (
                <>
                <IconButton onClick={()=>alert("delete")} color="error" sx={{ fontSize:"large" }}>
                    <DeleteOutlineOutlinedIcon  />
                </IconButton>
                /
                <IconButton onClick={()=>alert("click")} color="primary">
                    <LocalPrintshopOutlinedIcon />
                </IconButton></>
                
            );
        }
    },
];