import { Visibility, VisibilityOff } from '@mui/icons-material';
import AccountCircle from "@mui/icons-material/AccountCircle";
import CircleCheckedFilled from '@mui/icons-material/CheckCircle';
import ClearIcon from '@mui/icons-material/Clear';
import PasswordIcon from "@mui/icons-material/Password";
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import CircleUnchecked from '@mui/icons-material/RadioButtonUnchecked';
import LoadingButton from '@mui/lab/LoadingButton';
import { Box, Button, Checkbox, Dialog, DialogActions, DialogContent, DialogTitle, FormControl, FormControlLabel, FormGroup, IconButton, InputAdornment, OutlinedInput, TextField } from '@mui/material';
import { useTheme } from '@mui/material/styles';
import useMediaQuery from '@mui/material/useMediaQuery';
import CryptoJS from 'crypto-js';
import { useSnackbar } from 'notistack';
import { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import api from '../../api';
import { imageSrc } from '../../App';
import { AuthContext, OfficeType } from '../../provider/authProvider';

const rememberToken = "smb-remember";

export interface SignDataType{
    name: string;
    employeeid: string;
    password: string;
};

export interface LoginDataType {
    employeeid: string;
    password: string;
};


function Login() {
    const {
       auth,login
    } = useContext(AuthContext);
    
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    const [showPassword, setShowPassword] = useState<boolean>(false);
    const [showSign, setShowSign] = useState<boolean>(false);
    const [netLoading, setNetLoading] = useState<boolean>(false);

    const [checkOffice, setCheckOffice]
        = useState<OfficeType>(OfficeType.NONE);
    const [loginData, setLoginData]
        = useState<LoginDataType>({ employeeid: "", password: "" });
    const [signData, setSignData]
        = useState<SignDataType>({ name: "", employeeid: "", password: "" });
    const [remember, setRemember]
        = useState<boolean>(false);

    /**
     * * 加密使用者資料
     */
    const encryptoToken = (name: string, pass: string) => {
        const userToken = JSON.stringify({ name: name, pass: pass });
        const encrypted = CryptoJS.AES.encrypt(userToken, rememberToken );
        return encrypted.toString();
    };
    /**
     * * 解密使用者資料
     */
    const decrpytoToken = (userToken:string) => { 
        const decrypted = CryptoJS.AES.decrypt(userToken, rememberToken );
        const parse = decrypted.toString(CryptoJS.enc.Utf8);
        return JSON.parse(parse);
    };

    const handleLogin = () => { 
        setNetLoading(true)
        const { employeeid, password } = loginData;
        if (!employeeid||!employeeid.trim()) {
            enqueueSnackbar('Please enter your account', { variant: 'error' });
            setNetLoading(false);
            return;
        };
        if (!password||!password.trim()) {
            enqueueSnackbar('Please enter your password', { variant: 'error' });
            setNetLoading(false);
            return; 
        };
        login(employeeid.trim(), password.trim());
        setNetLoading(false);
        return
    };

    const handleSignUp = async () => { 
     
        if (checkOffice ==="NONE") {
            return enqueueSnackbar('Please select role.', { variant: 'warning' });
        }
        const signBody = {
            ...signData,
            permission:checkOffice
        };

        const { password,employeeid,name} = signData;
        try {
            await api.post(`api/account/signup?password=${password}&employeeid=${employeeid}&name=${name}&permission=${checkOffice}`);
            return enqueueSnackbar('sign up success', { variant: 'success' });
        } catch (err: any) {
            console.log(err.message);
             return enqueueSnackbar('Please select role.', { variant: 'error' });
        };
    };

    useEffect(() => { 
        if (remember) { 
            const { employeeid, password } = loginData;
            const userToken = encryptoToken(employeeid, password);
            localStorage.setItem(rememberToken , userToken);
        };
    }, [remember, loginData]);
    
    useEffect(() => { 
        setCheckOffice(OfficeType.NONE);
        const userToken = localStorage.getItem(rememberToken);
        if (userToken) { 
            setRemember(true)
            const { name, pass } = decrpytoToken(userToken);
            setLoginData({ employeeid: name, password: pass });
        };
    }, []);

    // useHotkeys('enter', () => handleLogin(), [loginData]);

  return (
      <Box>
          <Box id="logo-contain" sx={{margin:'auto',mt:5,width:{xs:300,sm:400,md:500,lg:600}}} >
            <img
                alt="logo"
                src={imageSrc}
                style={{ margin: 'auto',width:"100%" }}

            />
          </Box>
          <Box
              id="login-container" 
              boxShadow={20}
                sx={{
                    minWidth:"300px",
                    width: "40%",
                    background: "#C0392B ",
                    margin: "auto",
                    marginTop: 10,
                    padding: "2em",
                    display: "flex",
                    flexDirection: "column",
                    alignItems: "center",
                    borderRadius:10
                }}
            >
              <h1 style={{fontSize:48,fontWeight:'bold',fontFamily:'sans-serif'}}>Login.</h1>
             <Box sx={{ display: "flex", alignItems: "flex-end",margin:'auto',width:250 }}>
    
                <TextField
                    id="login-username"
                    placeholder='Employee ID'
                      sx={{ bgcolor: 'white' }}
                      value={loginData.employeeid}
                    InputProps={{
                        startAdornment: (
                            <InputAdornment position="start">
                            <AccountCircle />
                            </InputAdornment>
                        ),
                      }}
                    onChange={(e)=>setLoginData({...loginData,employeeid:e.target.value})}
                />
              </Box>
               <FormControl sx={{ m: 1,mt:5, width: 250 }} variant="outlined">
                    <OutlinedInput
                        id="login-password"
                        type={showPassword ? 'text' : 'password'}
                        placeholder='Password'
                        sx={{bgcolor:'white'}}
                        value={loginData.password}
                      onChange={(e) => setLoginData({ ...loginData, password: e.target.value })}
                      startAdornment={
                          <InputAdornment position="start">
                              <PasswordIcon />
                          </InputAdornment>
                      }
                        endAdornment={
                            <InputAdornment position="end">
                                <IconButton
                                    aria-label="toggle password visibility"
                                    onClick={() => setShowPassword(prev => !prev)}
                                    // onMouseDown={handleMouseDownPassword}
                                    edge="end"
                                >
                                    {showPassword ? <VisibilityOff /> : <Visibility />}
                                </IconButton>
                            </InputAdornment>
                        }
                       
                    />
                </FormControl>
       
              <FormControlLabel control={<Checkbox checked={remember} color="success" onChange={()=>setRemember(!remember)} />} label="Remember me." />
            <Box  mt={5} width={200} sx={{display:'flex',justifyContent:"space-between"}}>
                <LoadingButton variant="contained" color="success" onClick={handleLogin} loading={netLoading}>
                    login
                </LoadingButton>
                <Button variant="contained" color="warning" onClick={()=>setShowSign(true)} >
                    sign up
                </Button>
            </Box>
          </Box>
          <Dialog
                fullScreen={ fullScreen}
                open={showSign}
                // TransitionComponent={Transition}
                fullWidth={true}
                maxWidth={"sm"}
                keepMounted
              onClose={() => setShowSign(false)}
              
                aria-describedby="alert-dialog-slide-description"
            >
              <DialogTitle sx={{display:'flex',justifyContent:'space-between',borderBottom:'1px solid'}}>
                  {"Sign Account"}
                  <IconButton color="inherit" onClick={()=>setShowSign(false)}>
                        <ClearIcon />
                  </IconButton>
              </DialogTitle>
                <DialogContent sx={{bgcolor:'#FFA99D'}}>
                    <Box
                      sx={{
                          height:'100%',
                        display: "flex",
                        flexDirection: "column",
                        alignItems: "center",
                          justifyContent: 'center',
                        alignContent:'center'
                        
                    }}>
                      <Box sx={{ display: "flex", alignItems: "flex-end",width:250,mt:2 }}>
                            <TextField
                                id="-sign-username"
                                placeholder='Name'
                                sx={{ bgcolor: 'white' }}
                                value={signData.name}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <AccountCircle />
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={(e)=>setSignData({...signData,name:e.target.value})}
                            />
                        </Box>
                        <Box sx={{ display: "flex", alignItems: "flex-end",width:250,mt:2 }}>
                            <TextField
                                id="sign-employeeid"
                                placeholder='Employee ID'
                                sx={{ bgcolor: 'white' }}
                                value={signData.employeeid}
                                InputProps={{
                                    startAdornment: (
                                        <InputAdornment position="start">
                                            <PersonOutlineIcon />
                                        </InputAdornment>
                                    ),
                                }}
                                onChange={(e)=>setSignData({...signData,employeeid:e.target.value})}
                            />
                        </Box>
                       <FormControl sx={{ m:'auto', width: 250,mt:2 }} variant="outlined">
                            <OutlinedInput
                                id="sign-password"
                                type={showPassword ? 'text' : 'password'}
                                placeholder='Password'
                                sx={{bgcolor:'white'}}
                                value={signData.password}
                                onChange={(e)=>setSignData({...signData,password:e.target.value})}
                                endAdornment={
                                    <InputAdornment position="end">
                                        <IconButton
                                            aria-label="toggle password visibility"
                                            onClick={() => setShowPassword(prev => !prev)}
                                            // onMouseDown={handleMouseDownPassword}
                                            edge="end"
                                        >
                                            {showPassword ? <VisibilityOff /> : <Visibility />}
                                        </IconButton>
                                    </InputAdornment>
                                }
                            
                            />
                        </FormControl>
                        <FormGroup sx={{display:'flex',flexDirection:'row',mt:2}}>
                            <FormControlLabel
                              control={
                                  <Checkbox
                                      checked={checkOffice===OfficeType.OPERATOR}
                                      value="operator" onChange={()=>setCheckOffice(OfficeType.OPERATOR)}
                                      icon={<CircleUnchecked />} checkedIcon={<CircleCheckedFilled />}
                                  />}
                                label="OP"
                            />
                            <FormControlLabel
                              control={
                                  <Checkbox
                                      checked={checkOffice===OfficeType.WAREHOUSE}
                                      value="warehouse" onChange={()=>setCheckOffice(OfficeType.WAREHOUSE)}
                                      icon={<CircleUnchecked />} checkedIcon={<CircleCheckedFilled />}
                                  />}
                                label="Warehouse"
                            />
                       
                        </FormGroup>
                     
                    </Box>
                </DialogContent>
                <DialogActions >
                  <Button sx={{m:'auto'}}onClick={()=>handleSignUp()} variant="contained" color="success">Sign up</Button>
                </DialogActions>
            </Dialog>
      </Box>
  )
}

export default Login