import { Box, Button, FormControl, IconButton, Stack, TextField,Checkbox,Divider, Tooltip } from '@mui/material';
import React from 'react'
import {useState,useEffect,useMemo} from 'react';
import Paper from '@mui/material/Paper';
import ClearIcon from '@mui/icons-material/Clear';
import SearchIcon from '@mui/icons-material/Search';
import Collapse from '@mui/material/Collapse';
import CircleIcon from '@mui/icons-material/Circle';
import { useTranslation } from 'react-i18next';
import MenuItem from '@mui/material/MenuItem';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import api from '../../api';
import { useSnackbar } from 'notistack';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { TooltipProps, tooltipClasses } from '@mui/material/Tooltip';
import PlayCircleSharpIcon from '@mui/icons-material/PlayCircleSharp';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import WarningAmberIcon from '@mui/icons-material/WarningAmber';
import NotInterestedRoundedIcon from '@mui/icons-material/NotInterestedRounded';
import ArrowCircleRightRoundedIcon from '@mui/icons-material/ArrowCircleRightRounded';
import ArrowCircleUpRoundedIcon from '@mui/icons-material/ArrowCircleUpRounded';
import Typography from '@mui/material/Typography';
import ModeEditRoundedIcon from '@mui/icons-material/ModeEditRounded';
import CloudUploadSharpIcon from '@mui/icons-material/CloudUploadSharp';
import CheckCircleSharpIcon from '@mui/icons-material/CheckCircleSharp';
import { styled } from '@mui/material/styles';
import CustomModal from '../../components/modal';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import ExpandLessRoundedIcon from '@mui/icons-material/ExpandLessRounded';
const formColor = '#FFD48D';

const cellColor = "#FF625B";

const BlackTooltip = styled(({ className, ...props }: TooltipProps) => (
  <Tooltip {...props} arrow classes={{ popper: className }} />
))(({ theme }) => ({
  [`& .${tooltipClasses.arrow}`]: {
    color: theme.palette.common.black,
  },
  [`& .${tooltipClasses.tooltip}`]: {
      backgroundColor: theme.palette.common.black,
      fontSize:16
  },
}));

const AlertModal = (
    props: {
        employeeId: string|null,
        handleClose: () => void;
        handleNext: () => void;
}) => {
    const { employeeId,handleClose,handleNext} = props;
    return (
        <Dialog
            open={Boolean(employeeId)}
            onClose={handleClose}
        >
            <DialogTitle
                id="remove-alert-title"
                sx={{ bgcolor: '#EDC10B', fontWeight: 'bold', color: 'red', display: 'flex', alignItems: 'center' }}
            >
                <WarningAmberIcon sx={{mr:1}} color="error"/>刪除請求確認
            </DialogTitle>
            <DialogContent>
                <DialogContentText id="remove-alert-description" sx={{mt:3,color:'black',fontSize:'1.2em'}}>
                    確定要刪除工號: {employeeId} 的請求嗎?
                </DialogContentText>
            </DialogContent>
              <Divider />
            <DialogActions>
                <Button onClick={handleClose} color="error" variant='outlined' size="small">取消</Button>
                <Button onClick={handleNext} autoFocus variant='outlined' color="success" size="small">
                    確認
                </Button>
            </DialogActions>
        </Dialog>
    );
};


const containsText = (text:string, searchText:string) =>
    text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;
interface AccountType{ 
    id: number;
    name: string;
    employee_id: string;
    password: string;
    permission: string;
    status: string;
    auditors:string
};

function AuthManagePage() {
    
    const { t } = useTranslation();
    const { enqueueSnackbar} = useSnackbar();
    const [searchCollapse, setSearchCollapse]
        = React.useState(false);
    const [searchState, setSearchState]
        = useState<{ name: string, number: string }>({ name: "", number: "" });

    const [editTarget, setEditTarget] = useState<string>('');

    const [passwordValue, setPasswordValue] = useState<string>("");

    const [dataset, setDataset]
        = useState<AccountType[]>([]);
    
    const displayDataset = useMemo(() =>
        dataset.filter(ele => containsText(ele.name, searchState.name)).filter(ele=>containsText(ele.employee_id, searchState.number)),
        [dataset, searchState]
    );

    const handleSearchByName = () => {
        const target = searchState.name;
    };
    const handleSearchByNumber = () => { 
        const target = searchState.number;
    };

    const handleEditPassword = (event: React.ChangeEvent<HTMLInputElement>) => { 
        setPasswordValue(event.target.value);
    };
    const handleChange = (event: SelectChangeEvent,index:number) => { 
        const value = event.target.value;
        const targetIndex = dataset.findIndex(ele => ele.id === index);
        const target = dataset[targetIndex];
        handleUpdateDataset(targetIndex, { ...target,permission:value });
    };
    const handleUpdateDataset = (index:number,target:AccountType) => { 
        const updated = [...dataset];
        updated.splice(index, 1, target)
        setDataset([...updated]);
    };

    //api--------------------------------------------------
    /**
     * * 更新員工密碼
     */
    const handleSubmitNewPasswordAsync = async (employeeId: string, newPassword: string) => { 
        if (!newPassword.trim()) {
            return enqueueSnackbar("請輸入新密碼", { variant: 'warning' });
        };
        if (newPassword.indexOf(" ") > -1) {
            return enqueueSnackbar("含有無效字元", { variant: 'warning' });
         };
        try {
            await api.put("/api/account/pass", {
                "employeeid": employeeId,
                "password":newPassword
            });
            enqueueSnackbar("密碼更新成功", { variant: 'success' });
            getAccount();
        } catch (err: any) {
             console.log(err.message);
            enqueueSnackbar(err.message, { variant: 'error' });
        }
    };
    /**
     * * 更新員工職位
     */
    const handleSubmitPermissionAsync = async (employeeId: string) => {
        try {
            const targetIndex = dataset.findIndex(ele => ele.employee_id === employeeId);
            const target = dataset[targetIndex];
            await api.put("api/account/permission", {
                employeeid: target.employee_id,
                permission: target.permission
            });
            enqueueSnackbar("更新職位成功", { variant: 'success' });
            getAccount();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message, { variant: 'error' });
        };
    };
    /**
     * * 通過pai
     */
    const handleApprovedAsync = async (employeeId:string) => { 
        try {
            await api.put("api/account/approved", { employeeid: employeeId });
            enqueueSnackbar("通過成功", { variant: 'success' });
            getAccount();
        } catch (err: any) { 
             console.log(err.message);
            enqueueSnackbar(err.message, { variant: 'error' });
        };
    };
    /**
     * * 撤回通過pai
     */
    const handleUnApprovedAsync = async (employeeId: string) => {
        try {
            await api.put("api/account/unapproved", { employeeid: employeeId, });
            enqueueSnackbar("撤回成功", { variant: 'success' });
            getAccount();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message, { variant: 'error' });
        };
    };
    const handleRemoveAccountAsync = async (employeeId: string) => {
        if (!window.confirm(`確定刪除 ${employeeId} ?`)) return;
        try {
            await api.delete(`api/account?employeeid=${employeeId}`); 
            enqueueSnackbar("刪除成功", { variant: 'success' });
            getAccount();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message, { variant: 'error' });
        };
     };
    const columns: GridColDef[] = [
     { field: 'employee_id', headerName:  t("authManage.employeeID"), width: 150,headerAlign:'left',align: 'left' },
    { field: 'name', headerName: t("authManage.name"), width: 120,headerAlign:'right',align: 'right'  },
    {
        field: 'permission',
        headerName:  t("authManage.office"),
        width: 250,headerAlign:'right',align: 'right',
        renderCell: (params: GridValueGetterParams) => 
            <Stack direction={"row"} >
                <BlackTooltip title="變更職位" arrow>
                    <IconButton color="primary" onClick={()=>handleSubmitPermissionAsync(params.row.employee_id)}>
                        <ArrowCircleUpRoundedIcon />
                    </IconButton>
                </BlackTooltip>
                <FormControl sx={{ m: 1, minWidth: 150 }} size="small">
                    <Select
                        id="demo-select-small"
                        value={params.row.permission}
                        onChange={(event: SelectChangeEvent)=>handleChange(event,params.row.id)}
                    >
                        <MenuItem value={""}>None</MenuItem>
                        <MenuItem value={"OPERATOR"}>{t("authManage.operator")}</MenuItem>
                        <MenuItem value={"WAREHOUSE"}>{t("authManage.warehouse")}</MenuItem>
                        <MenuItem value={"ADMIN"}>{t("authManage.admin")}</MenuItem>
                    </Select>
                </FormControl>
            </Stack>
          
    },
        {
        // NotInterestedRoundedIcon
        field: 'status',
        headerName:  t("authManage.status"),
        width: 180,headerAlign:'right',align: 'right',
        renderCell: (params: GridValueGetterParams) =>
            <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center',width:'100%' }}>
                {params.row.status === "通過" ?
                    <BlackTooltip title="審核不通過" arrow >
                        <IconButton color="error" onClick={()=>handleUnApprovedAsync(params.row.employee_id)}>
                            <NotInterestedRoundedIcon />
                        </IconButton>
                    </BlackTooltip> :
                    <BlackTooltip title="審核通過" arrow>
                        <IconButton color="success" onClick={()=>handleApprovedAsync(params.row.employee_id)}>
                            <ArrowCircleRightRoundedIcon />
                        </IconButton>
                    </BlackTooltip>
                }
            
                <div style={{fontWeight:'bold',color:params.row.status === "通過" ?"#18AB18":"#D92222"}}>
                    {params.row.status}
                    {/* <Checkbox color={params.row.status === "通過" ? "success" : "error"} checked={true} checkedIcon={<CircleIcon />} />   */}
                </div>
               
            </div>
        
    },
    {
        field: 'auditors',
        headerName: t("authManage.checker"),
        width: 120, headerAlign: 'right', align: 'right',
      
        },
      {
          field: 'password', headerName: t("authManage.updatePassword"),
          width: 150, headerAlign: 'right', align: 'right',
            renderCell: (params: GridValueGetterParams) => {
                const { employee_id } = params.row;
                return (
                    <Button startIcon={<ModeEditRoundedIcon />} variant="outlined" size="small" onClick={()=>setEditTarget(employee_id)}>
                        {t("authManage.updatePassword")}
                    </Button>
                );
            }
        },
    {
        field: 'delete',
        headerName: "刪除",
        width: 120, headerAlign: 'center', align: 'center',sortable:false,
        renderCell: (params: GridValueGetterParams) =>
            <IconButton color="error" onClick={()=>handleRemoveAccountAsync(params.row.employee_id)}>
                <ClearIcon  fontSize="large"/>
            </IconButton>
    },
    ];
    
    const getAccount = async () => { 
        try { 
            const res = await api.get("api/account");
            const data = res.data;
            const result: AccountType[] = data.map((ele: any, index: number) => {
                return {
                    id: index,
                    ...ele
                }
            });
            // console.log(result)
            setDataset(result);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message,{variant:'error'})

        }
    }; 


    const form = () => (
        <React.Fragment>
            <FormControl sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <h2 style={{ paddingRight: 10, width: 100 }}>
                    {t("authManage.name")}
                </h2>
                <TextField
                    label={t("authManage.name")}
                    value={searchState.name}
                    size="small" sx={{ bgcolor: 'white' }}
                    onChange={(e) => setSearchState({ ...searchState, name: e.target.value })}
                
                />
              
            </FormControl>
            <FormControl sx={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
                <h2 style={{ paddingRight: 10, width: 100 }}>
                    {t("authManage.employeeID")}
                </h2>
                <TextField
                    label={t("authManage.employeeID")}
                    value={searchState.number}
                    size="small" sx={{ bgcolor: 'white' }}
                    onChange={(e) => setSearchState({ ...searchState, number: e.target.value })}
                
                />
                
            </FormControl>
        </React.Fragment>
    );


    useEffect(() => { 
        getAccount();
    }, []);

    return (
        <Box sx={{ mt: 10, m: 'auto', width: '90%' }}>
            <CustomModal open={Boolean(editTarget)} onClose={()=>setEditTarget("")} maxWidth="sm">
                <DialogTitle sx={{background:'#7B241C',color:'white'}}>
                    員工編號:{ editTarget} - 密碼更改
                </DialogTitle>
                <DialogContent>
                    <Stack sx={{ m: 'auto', mt: 6, width: 350 }} direction="row" alignItems={"center"} justifyContent="space-around">
                        <p>新密碼:</p>
                        <TextField onChange={handleEditPassword} value={passwordValue} sx={{width:200}} size="small"/>
                    </Stack>
                </DialogContent>
                <DialogActions>
                    <Button color='error' onClick={()=>setEditTarget("")} variant="outlined">取消</Button>
                    <Button 
                        variant="outlined"
                        color='success'
                        onClick={() => handleSubmitNewPasswordAsync(editTarget, passwordValue)}
                    >送出</Button>
                </DialogActions>
            </CustomModal>
            <h1>{ t("authManage.title")}</h1>
            <Paper sx={{ width: '100%', overflow: 'hidden', justifyContent: 'center' }}>
                <Box sx={{display: {  md: 'none', lg: 'none' },height:50,mt:2}}>
                    <Button
                        sx={{ ml:2 }}
                        variant="outlined"
                        startIcon={searchCollapse?<ExpandLessRoundedIcon fontSize="large" />:<ExpandMoreRoundedIcon fontSize="large" />}
                        onClick={()=> setSearchCollapse((prev) => !prev)}
                    >
                        {t("authManage.search")}
                    </Button>
                </Box>
                <Collapse in={searchCollapse} sx={{ display: { md: 'none', lg: 'none', bgcolor: formColor } }}>
                    <Paper sx={{bgcolor:formColor,pl:5}}>
                        <Stack
                            direction="column"
                            justifyContent="center"
                        >
                            {form()}
                        </Stack>
                        </Paper>
                </Collapse>
                <Box
                    sx={{
                        display: { xs: 'none', sm: "none", md: 'flex', lg: 'flex' },
                        justifyContent: 'space-around',
                        height: 80, width: "100%", m: 'auto',bgcolor:formColor
                    }}
                >
                    {form()}
                </Box>
                <Box sx={{ height: 400, width: '100%' }}>
                    <DataGrid
                         sx={{ 
                            fontSize:18,
                            '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                outline: 'none',
                            },
                        }}
                    rows={displayDataset}
                    columns={columns}
                    pageSize={15}
                    rowsPerPageOptions={[15]}
                    // checkboxSelection
                    disableSelectionOnClick
                    experimentalFeatures={{ newEditingApi: true }}
                />
                    </Box>
            </Paper>
        </Box>
    );
};

export default AuthManagePage;