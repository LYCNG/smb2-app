import React, { createContext, useState } from 'react';
import { useNavigate } from 'react-router-dom';


export type Page = 'stockin' | 'order' | 'stockout' | '';

interface DataType { 
    orderId: string;
    type: string;//purchase
};

const initData: DataType = {
    orderId: "",
    type:"", //purchase
};

type WareContextType = {
    dataPack: DataType;
    transport: (address: Page, data: any) => void;
    onSignFor: () => void;
};

export const WareContext = createContext<WareContextType>({
    dataPack:initData ,
    transport: (address: Page, data: any) => { },
    onSignFor:()=>{}
});

function WareProvider(props: {
    children: React.ReactNode
}) {    
    const navigate = useNavigate();

    const [dataPack, setDataPack]
        = useState<DataType>(initData);
    
    /**
     * * 打包運送至目的地
     * @param address 導向位置
     * @param data 包裹內容
     */
    const transport = (
        address: Page,
        data:DataType
    ) => { 
        setDataPack(data);
        navigate(address);
    };
    
    /**
     * * 簽收後初始化
     */
    const onSignFor = () => setDataPack(initData);
    
    return (
        <WareContext.Provider value={{ dataPack, transport, onSignFor }}>
            {props.children}
        </WareContext.Provider>
    );
};

export default WareProvider;