import { useSnackbar } from 'notistack';
import React, { createContext, useState } from "react";
import api from "../../api";

export enum OfficeType  {
  ADMIN = "ADMIN",
  OPERATOR = "OPERATOR",
  WAREHOUSE = "WAREHOUSE",
  MASTER='MASTER',
  NONE = "NONE"
};

type UserType={
    username: string;
    office: OfficeType;
    employee_id: string;
    isAuth: boolean;
};
const initUser: UserType = {
    username: "",
    office: OfficeType.NONE,
    employee_id: "",
    isAuth: false
};
type AuthContextType = {
    auth: UserType;
    // setAuth: React.Dispatch<SetStateAction<UserType>>;
    login: (username: string, password: string) => void;
    logout: () => void;
    checkAuth: () => void;
    changeOffice:(params:OfficeType)=>void
};

export const AuthContext =createContext<AuthContextType>({
    auth: initUser,
    login: (username: string, password: string) => { },
    logout: () => { },
    checkAuth: () => { },
    changeOffice:(params:OfficeType)=>{}
});


export const smbToken = "smb-access-token";
export const smbStorage = "smb-auth";

function AuthProvider(props: { children: React.ReactNode }) {

    const { enqueueSnackbar } = useSnackbar();
    
    const [auth, setAuth]
        = useState<UserType>(initUser);
    
    const getOfficeType = (permission: string) => { 
        switch (permission.toUpperCase()) {
            case 'ADMIN':
                return OfficeType.ADMIN;
            case 'OPERATOR': //
               return OfficeType.OPERATOR
          
            case 'WAREHOUSE': //倉管
                return OfficeType.WAREHOUSE;
            default:
               return OfficeType.NONE
        };
    }; 
    const login = async (username: string, password: string) => { 
        try {
            const res = await api.post(`api/account/signin?employeeid=${username}&password=${password}`);
            const {
                name, employeeid, permission, AccessToken
            } = await res.data;
            if (!employeeid) throw new Error("login failed");
            localStorage.setItem(smbToken, AccessToken);
            localStorage.setItem(smbStorage , JSON.stringify({
                isAuth: true, office: permission,
                username: name, employee_id: employeeid
            }));
            setAuth({
                isAuth: true,
                office: getOfficeType(permission),
                username: name,
                employee_id: employeeid
            });
            enqueueSnackbar('Login success!', { variant: 'success' });
        } catch (err: any) {
            enqueueSnackbar(err.message,{ variant: 'error' });
        };
    };
 
    const logout = () => { 
        localStorage.removeItem(smbToken);
        localStorage.removeItem(smbStorage);
        setAuth(initUser);
    };
    
    const checkAuth =() => { 
        const storage = localStorage.getItem(smbStorage);
        if (storage) {
            const { isAuth, office, username, employee_id } = JSON.parse(storage);
            setAuth({
                isAuth: isAuth,
                office:office as OfficeType,
                username: username,
                employee_id: employee_id
            });
            enqueueSnackbar('checkAuthSuccess', { variant: 'success' });
        } else {
            enqueueSnackbar("未登入", { variant: 'error' });
        };
    };

    const changeOffice = (office:OfficeType) => 
        setAuth({...auth,office:office})
    
    return (
        <AuthContext.Provider value={{ auth, login,logout,checkAuth, changeOffice }}>
            {props.children}
        </AuthContext.Provider>
    );
};

export default AuthProvider;