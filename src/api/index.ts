import axios from "axios";

import { AxiosRequestConfig } from "axios";
import { smbStorage, smbToken } from "../provider/authProvider";
const { REACT_APP_URL } = process.env
// const url = REACT_APP_URL ? REACT_APP_URL : window.location.origin;

const local = window.location.origin;
const url = "https://192.168.0.16:5000/"; 

axios.defaults.timeout = 10000;

const axiosApi = axios.create({
  baseURL:local,
  //baseURL: local,
  headers: {
    'Authorization': `Bearer ${localStorage.getItem("smb-access-token")}`, 
  "Content-Type": "application/json",
    "Access-Control-Allow-Origin": "*",
    "Access-Control-Allow-Methods": "GET,POST,PUT,PATCH,DELETE,HEAD,OPTIONS",
    "Access-Control-Allow-Headers":
      "Content-Type, Origin, Accept, Authorization, Content-Length, X-Requested-With",
  },
});
axiosApi.interceptors.request.use((config:AxiosRequestConfig)  => {
  let token = localStorage.getItem(smbToken);
  if (config&&config.headers) {
    config.headers["Authorization"] = "Bearer " + token;
    return config;
  };
});
axiosApi.interceptors.response.use(
    response => {
    return response;
  }, error => {
    if (error.code === 'ERR_NETWORK') {
        localStorage.removeItem(smbToken);
        localStorage.removeItem(smbStorage);
        //return window.location.href = '/login'; 
    }
    if (error.response) { 

      switch (error.response.status) {
        case 401:
          {
            localStorage.removeItem(smbToken);
            localStorage.removeItem(smbStorage);
            const { message } = error.response.data
            alert(`${error.response.status}: ${message || '資料錯誤'}。`)
          }
          break;
        case 404:
          alert(`${error.response.status}: 資料來源不存在`)
          break
        case 500:
          alert(`${error.response.status}: 內部系統發生錯誤`)
          break;
        default:
          // alert(`${error.response.status}: 系統維護中，造成您的不便，敬請見諒。`)
          break
      }
      // return window.location.href = '/login';
      
    };
        
    if(error.message.includes('timeout')){   // 判斷請求異常資訊中是否含有超時timeout字串
      console.log("server error", error);
      alert("網路連線逾時");
      Promise.reject(error);

       //return window.location.href = '/login';     // reject這個錯誤資訊
    }
    return Promise.reject(error);
  }
)
export default axiosApi;