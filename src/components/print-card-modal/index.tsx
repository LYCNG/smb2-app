import {
  Button,
  IconButton, Paper, useMediaQuery,
  useTheme
} from '@mui/material';
import { styled } from '@mui/material/styles';
import { useSnackbar } from 'notistack';
import React from 'react';

import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableRow from '@mui/material/TableRow';
import moment from "moment";

import CloseIcon from '@mui/icons-material/Close';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import { makeStyles } from '@mui/styles';
import { useTranslation } from 'react-i18next';
import api from '../../api';

const bgColor = "#393939";

const useTableStyles = makeStyles({
  table: {
    "& .MuiTableCell-root": {
      borderLeft: "1px solid rgba(224, 224, 224, 1)"
    }
  }
});

const CardTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.body}`]: {
    fontSize: 24,
    fontWeight: 'bold',
  
  },
}));

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="down" ref={ref} {...props} />;
});
export interface OrderType { 
   date:string,
  location:string,
  flag_number:string,
  product_number:string
  product_name:string,
  quantity: number,
  type:String
};

const PrintDialog = (
  props: {
    open: boolean, order:OrderType,handleClose: () => void,
  }) => { 
  const classes = useTableStyles();
  const { t } = useTranslation();
  const {enqueueSnackbar } = useSnackbar();
  const { open,order, handleClose } = props;
    const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

  const handlePrint = async () => { 
    try {
      const {
        date, location, product_name, product_number, quantity, flag_number
      } = order;
      const data = {
        "date":date,
        "location":location,
        "flag_number":flag_number,
        "product_number":product_number,
        "product_name":product_name,
        "quantity":quantity
      };
      await api.post(`api/print/${order.type}`, data);
      enqueueSnackbar("已送出",{variant:'success'})
      
    } catch (err: any) {
      console.log(err);
       enqueueSnackbar("送出失敗",{variant:'error'})
    }
  };

  // useEffect(() => { 
  //   getFlgNumberAsync()
  
  // }, [open]);

  return (
      <Dialog
      open={open}
      maxWidth={"md"}
      fullScreen={fullScreen}
      fullWidth={true}
        TransitionComponent={Transition}
        keepMounted
        onClose={handleClose}
        aria-describedby="alert-dialog-slide-description"
      >
      <DialogTitle sx={{display:'flex',justifyContent:'space-between',bgcolor:bgColor,fontSize:25,fontWeight:'bold',color:'white'}}>
        {"Product Card"}
        <IconButton onClick={handleClose}>
          <CloseIcon />
        </IconButton>
      </DialogTitle>
        <DialogContent sx={{bgcolor:'#8A8A8A'}}>
        <TableContainer component={Paper} sx={{mt:2}}> 
          <Table sx={{ minWidth: 500,border:1 }} aria-label="spanning table" size={fullScreen?"small":"medium"} className={classes.table}>
              <TableBody>
                <TableRow >
                  <CardTableCell align="center"  sx={{width:"60%"}} colSpan={2}>{"物料卡"}</CardTableCell>
                  <CardTableCell align="center" sx={{width:"25%"}} >{"入貨日期"}</CardTableCell>
                  <CardTableCell align="center" sx={{width:"15%"}} >{moment(order.date).format("YYYY/MM/DD")}</CardTableCell>
                </TableRow>
                <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}}>{"架區"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"35%"}} >{"辦桌旁"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"45%"}} colSpan={2}>{"*辦桌旁*"}</CardTableCell>
              </TableRow>
              <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}}>{"旗號"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"35%"}}>{order.flag_number}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"45%"}} colSpan={2}>{order.flag_number}</CardTableCell>
              </TableRow>
              <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}}>{"品號"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"35%"}}>{order.product_number}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"45%"}} colSpan={2}>{order.product_number}</CardTableCell>
              </TableRow>
              <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}}>{"品名"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"80%"}} colSpan={3}>{order.product_name}</CardTableCell>
              </TableRow>
              <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}}>{"數量"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"35%"}}>{order.quantity}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"45%"}} colSpan={2}>{order.quantity}</CardTableCell>
              </TableRow>
              <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}}>{"補"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"35%"}}>{"0"}</CardTableCell>
                    <CardTableCell align="center" sx={{width:"45%"}} colSpan={2}>{"*0*"}</CardTableCell>
              </TableRow>
              <TableRow >
                    <CardTableCell align="center" sx={{width:"20%"}} rowSpan={2}>{"繳庫"}</CardTableCell>
                <CardTableCell align="center" sx={{ width: "15%" }} rowSpan={2}>{"數量調整"}</CardTableCell>
                <CardTableCell align="center" sx={{width:"15%"}}>2</CardTableCell>
                    <CardTableCell align="center" sx={{ width: "30%" }} >2</CardTableCell>
                    <CardTableCell align="center" sx={{width:"30%"}} >3</CardTableCell>
              </TableRow>
              <TableRow >
                <CardTableCell align="center" sx={{width:"20%"}}>2</CardTableCell>
                  <CardTableCell align="center" sx={{ width: "40%" }} >1</CardTableCell>
                  <CardTableCell align="center" sx={{width:"40%"}} >2</CardTableCell>
              </TableRow>
            </TableBody>
           </Table>
         </TableContainer>
        </DialogContent>
        <DialogActions sx={{bgcolor:bgColor}}>
         
              <Button onClick={handlePrint} variant={"contained"} color="success" startIcon={<LocalPrintshopOutlinedIcon />}>
                  {t("warehouse.print")}
              </Button>
        </DialogActions>
      </Dialog>
  );
};

export default PrintDialog;