import CloseIcon from '@mui/icons-material/Close';
import DownloadSharpIcon from '@mui/icons-material/DownloadSharp';
import ForwardSharpIcon from '@mui/icons-material/ForwardSharp';
import { Box, Button, Divider, Grow, IconButton, List, Stack, useMediaQuery, useTheme } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { TransitionProps } from '@mui/material/transitions';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { useContext, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { DatasetType } from '..';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';
import AreaLocationContent from '../../area-display';
const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Grow  ref={ref} {...props} />;
});

const bgColor = "#283747";

function ChangeModal(
     props: {
        open: boolean, handleClose: () => void,handleFinish:()=>void,data:DatasetType
    }
) {
    const { open, handleClose, handleFinish, data } = props;
    const { t } = useTranslation();
    const {auth} = useContext(AuthContext);
    const { employee_id } = auth; 
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    //state---------------------------------------------------------
    const [selectArea, setSelectArea] = useState<string>("");

    //function--------------------------------------------------------
    const handleSelectArea = (area:string) => {
        setSelectArea(area);
     };
    //api-----------------------------------------------------------
    const submitDataAsync = async () => {
        if (!selectArea) {
            return enqueueSnackbar("請選擇儲位", { variant: "warning" });
        };
        const [position, area] = selectArea.split("-");
        const body = {
            order_id: "",
            date:moment(Date.now()).format("YYYY-MM-DD"),
            employee_id:employee_id,
            machinecode: "",
            orderitems: [
                {
                    item_number: 1,
                    product_name: data.product_name,
                    quantity: data.remain,
                    position: data.position,
                    area: data.area,
                    action: "out",
                    flag_number: data.flag_number
                },{
                    item_number: 2,
                    product_name:data.product_name,
                    quantity:data.remain,
                    position:position,
                    area:area,
                    action:"in",
                    flag_number: ""
                }
            ]
        };
        try {
            await api.post("api/management/orders/pending/change", body);
            enqueueSnackbar("送出成功", { variant: "success" });
            handleFinish(); 
            handleClose();
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar("送出失敗", { variant: "error" });
        }
    };
    return (
        <Dialog
            open={open}
            maxWidth={"md"}
            fullScreen={fullScreen}
            fullWidth={true}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle sx={{display:'flex',justifyContent:'space-between',bgcolor: bgColor,fontSize:25,fontWeight:'bold',color:'white'}}>
                {t("warehouse.manage.emptyAndChange")}
                <IconButton onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent sx={{ bgcolor: "#BA4A00" }}>
                <Box sx={{mt:2}}>
                    <AreaLocationContent handleSelectArea={handleSelectArea}/>
                </Box>
                <Stack sx={{p:1,bgcolor:'#EAECEE',height:{xs:400,sm:250,md:250}}} direction={{xs:'column',sm:'row',md:'row'}} alignItems="center" justifyContent={"space-around"}>
                    {/* 異動架位: {data.location} 異動至 儲位:{selectArea} */}
                    <List sx={{ p:2,width: 300,border:1,borderColor:"rgb(0,0,0,0.5)",bgcolor:"white" }}>
                        <h3>{t("warehouse.manage.change")}</h3>
                        <ListItem disablePadding>
                            <ListItemText primary={t("warehouse.manage.rack")} />
                            <ListItemText primary={data.location} />
                        </ListItem>
                        <Divider />
                        <ListItem disablePadding>
                            <ListItemText primary={t("warehouse.manage.product")}  />
                            <ListItemText primary={data.product_name} />
                        </ListItem>
                        <Divider />
                        <ListItem disablePadding>
                            <ListItemText primary={t("warehouse.manage.remain")}  />
                            <ListItemText primary={data.remain} />
                        </ListItem>
                         <Divider />
                    </List>
                    {fullScreen? <DownloadSharpIcon fontSize='large' color="success"/>:<ForwardSharpIcon fontSize='large' color="success"/>}
                    <List sx={{ p:2,width: 300,border:1,borderColor:"rgb(0,0,0,0.5)",bgcolor:"white"}}>
                        <h3>目標</h3>
                        <ListItem  disablePadding>
                            <ListItemText primary={t("warehouse.manage.rack")} />
                            <ListItemText primary={selectArea} />
                        </ListItem>
                             <Divider />
                        </List>
                </Stack>
            </DialogContent>
            <DialogActions sx={{ bgcolor: bgColor }}>
                <Button variant="contained" color='success' sx={{ m: 'auto' }} onClick={submitDataAsync}>
                    {t("warehouse.sure")}
                </Button>
            </DialogActions>
        </Dialog>
    );
};

export default ChangeModal