import OpenInFullIcon from '@mui/icons-material/OpenInFull';
import { Box, Button, Divider, FormControl, Stack, TextField, Toolbar, Typography } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { useEffect, useMemo, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
import OnPendingTable, { OnPendingType } from '../onPending-table';
import ChangeModal from './changeMolal';

const FormContainer = (props: {
    title: string
    value: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}) => {
    const { t } = useTranslation();
    return (
        <FormControl sx={{ maxWidth: 300, display: 'flex', flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
            <h3 style={{ marginRight: 10 }}>{props.title}</h3>
            <TextField
                label={t("warehouse.keyword")}
                value={props.value}
                size="small"
                sx={{ bgcolor: 'white', width: { xs: 120, sm: 150, md: 180 }}}
                onChange={props.onChange}
            />
        </FormControl>
    );
};
    
export interface DatasetType { 
    id: number | string;
    area: string
    expect_remain: number
    flag_number: string
    location: string
    position: string
    product_name: string
    remain: number
    remark: ""
    update_time: string
};

const containsText = (text:string, searchText:string) =>
    text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

function WareChange() {
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    //state-----------------------------------------------------------
    const [locationText, setLocationText] = useState<string>("");
    const [productText, setProductText] = useState<string>("");
    const [dataset, setDataset] = useState<DatasetType[]>([]);
    const [loading, setLoading] = useState<boolean>(false);
    const [targetData, setTargetData] = useState<DatasetType | null>(null);
    const [onPendingData, setOnPendingData] = useState<OnPendingType[]>([]);


    //function-----------------------------------------------------------
    /**架位輸入 */
     const onLocationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setLocationText(event.target.value);
    };
    /**產品名稱輸入 */
    const onProductChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        setProductText(event.target.value);
    };

    const handleClickRow = (rowData:DatasetType) => { 
        setTargetData(rowData);
    };

    //api-----------------------------------------------------------------
    const getStorageNoEmpty = async () => { 
        setLoading(true)
        try {
            const res = await api.get("api/storage/nonempty");
            const results: DatasetType[] = res.data.map((data:any, index:number) => {
                return {
                    ...data,
                    id: index
                };
            });
            
            setDataset(results);
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar("讀取資料失敗",{variant:"error"})
        } finally {
             setLoading(false)
        }
    };
     const getMergeData = async () => { 
        try {
            const res = await api.get('/api/management/orders/pending/in');
            const changedList = res.data.filter((data: any) => data.type === "change");
             const dataset =changedList.map((merge: any, index: number) => {
                return {
                    ...merge,
                    id:index
                };
            })
            setOnPendingData(dataset);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("讀取資料失敗",{variant:'error'})
        };
    };
    const displayedDataset = useMemo(
        () => dataset.filter((option) =>
            containsText(option.location, locationText) && containsText(option.product_name, productText)),
        [locationText,productText,dataset]
    );
    
    //useEffect-----------------------------------------------------------
    useEffect(() => {
        setDataset([]);
        getStorageNoEmpty();
        getMergeData();
    }, []);
    
    return (
         <Box sx={{ bgcolor: '#FBFCFC', width: '100%', minHeight: 1000, mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            {targetData&&<ChangeModal
                data={targetData}
                open={Boolean(targetData)}
                handleClose={() => setTargetData(null)}
                handleFinish={() => getStorageNoEmpty()}
            />}
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                   <Typography variant={"h4"} sx={{fontWeight:'bold',fontSize:"3.5vh"}} align="center">
                    {t("warehouse.manage.title")} : {t("warehouse.manage.storageChangeList")}
                </Typography>
            </Toolbar>
            <Box sx={{  display: 'flex', flexDirection: { xs: "column", sm: "column", md: "row" }, justifyContent: 'space-between' }}>
                 <Stack
                    direction={{ xs: "column", sm: "column", md: 'row' }}
                    justifyContent={"space-around"}
                    alignItems={{ xs: "flex-start", sm: "flex-start", md: "center" }}
                    sx={{m:'auto',width:'100%'}}
                >
                    <FormContainer
                        title={t("warehouse.manage.rack")}
                        value={locationText}
                        onChange={onLocationChange} 
                    />
                    <FormContainer
                        title={t("warehouse.manage.product")}
                        value={productText}
                        onChange={onProductChange} 
                    />
                </Stack>
            </Box>
            <Box
                sx={{
                    width:{xs:'100%',sm:"100%",md:'75%'},m:'auto',
                    pt: 2, height:450,
                    '& .MuiDataGrid-columnHeaders': { bgcolor:"#BA4A00", color: 'white' }}}
            >
                <DataGrid
                    sx={{
                    fontSize: 16,bgcolor:'white',
                    '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                        outline: 'none',
                    },
                    }}
                    loading={loading}
                    density={'compact'}
                    rows={displayedDataset}
                    columns={ changeColumns(t,handleClickRow)}
                    showCellRightBorder
                    pageSize={15}
                    rowsPerPageOptions={[5]}
                    experimentalFeatures={{ newEditingApi: true }}
                 
                />
            </Box>
            <Divider sx={{ mt: 2}} />
            <OnPendingTable
                sx={{
                    m:'auto',
                    width:{ xs: "100%", sm: "100%", md: "90%" },
                    pt: 2, height:250,
                    '& .MuiDataGrid-columnHeaders': { bgcolor:"#BA4A00", color: 'white' }
                }}
                title={'異動單據'}
                rows={onPendingData}
                reload={getMergeData}
                type={'change'}
            /> 
        </Box>
    );
};

export default WareChange;

const changeColumns = (
    t: TFunction,
    handleClick: (data: DatasetType) => void,
): GridColDef[] => [
        { field: 'id', headerName: "ID", width: 90, align: 'center', headerAlign: 'center',hide:true },
        {
        field: 'checked',
        headerName: t("warehouse.manage.change"),
        align: 'center',
        headerAlign:'center',
        width: 120,
        renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        size="small"
                      
                        endIcon={<OpenInFullIcon />}
                        color="warning" onClick={() => handleClick(params.row)}>選擇</Button>
                );
            }
        },
        {
        field: 'location',
        headerName: t("warehouse.manage.rack"),
        align: 'right',
        headerAlign:'right',
        width: 120,
    },
{
        field: 'update_time',
        headerName: t("warehouse.main.date"),
        align: 'right',
        headerAlign:'right',
    width: 180,
        valueGetter:(params:GridValueGetterParams)=>`${moment(params.row.update_time).format("YYYY-MM-DD")}`
    },
{
        field: 'flag_number',
        headerName: t("warehouse.manage.materialNum"),
        align: 'right',
        headerAlign:'right',
        width: 150,
        },
    {
        field: 'product_name',
        headerName: t("warehouse.manage.product"),
        align: 'right',
        headerAlign:'right',
        width: 250,
        },
    {
        field: 'remain',
        headerName: t("warehouse.manage.count"),
        align: 'right',
        headerAlign:'right',
        width: 120,
        },
     {
        field: 'remark',
        headerName:t("warehouse.manage.mark"),
        align: 'right',
        headerAlign:'right',
        width: 240,
    },

];