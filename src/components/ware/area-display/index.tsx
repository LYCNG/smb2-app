import { Box, Button, Checkbox, Divider, IconButton, Paper, Stack, useMediaQuery, useTheme } from '@mui/material';
import Avatar from '@mui/material/Avatar';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemAvatar from '@mui/material/ListItemAvatar';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import React, { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import CloseIcon from '@mui/icons-material/Close';
import OpenInNewRoundedIcon from '@mui/icons-material/OpenInNewRounded';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Slide from '@mui/material/Slide';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { TransitionProps } from '@mui/material/transitions';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import api from '../../../api';

/**計算顏色 */
const getColor = (count: number) => {
    if (count > 3) return "#27AE60";
    if (count > 0) return "#3498DB ";
    if (!count) return "#85929E";
};
  
interface StorageType { 
    name: string;
    remark: string;
};
/**架位接口 */
interface LocationType{
  num: string;
  place: { name: string, remark: string }[];
};
/**架區接口 */
interface AreaType { 
  area: string;
  location: LocationType[];
};
interface StockType{
    id:number
    location: string,
    flag_number: string,
    status: string,
    product_name: string,
    remain: number
};
    
const initTempStorage: AreaType = {
    area: "",
    location:[]
};
function AreaLocationContent(props: {
    handleSelectArea: (area: string) => void,
    product?: string;
}) {
    const { t } = useTranslation();
    const { handleSelectArea,product } = props;
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('md'));
    //state--------------------------------------------
    const [checked, setChecked] = useState<number>(0);
    const [locationData, setLocationData] = useState<AreaType[]>([]);

    const [storage,setStorage ] = useState<StorageType[]>([]); 
    const [showModal, setShowModal] = useState<boolean>(false);
    const [stockData, setStockData] = useState<StockType[]>([]);
    const [tempStorage, setTempStorage]
        = useState<AreaType>(initTempStorage);
    
    const columns: GridColDef[] = [
        { field: 'id', headerName: 'No', width: 80 ,align:'center',headerAlign:'center'},
        {
            field: 'location',
            headerName: t("warehouse.purchase.location"),
            width: 150,
             align: 'center',
            headerAlign:'center'
        },
        {
            field: 'flag_number',
            headerName: t("warehouse.purchase.card"),
            width: 200,
             align: 'center',
            headerAlign:'center'
        },
        {
            field: 'product_name',
            headerName: t("warehouse.purchase.product"),
            sortable: false,
            width: 250,
        },
        {
            field: 'remain',
            headerName: t("warehouse.purchase.remainCount"),
            sortable: false,
            width: 120,
            align: 'right',
            headerAlign:'right'
        },
    ];
    //function------------------------------------------
    /**選擇架區 */
    const handleToggle = (value: number) => () => {
        
        if (checked === value) {
            setChecked(0);
              setTempStorage(initTempStorage)
            return
        };
        if (value > 0) {
            setChecked(value);
            const targetData = locationData[value - 1];
            setTempStorage(targetData);
            return;
        };
    };

    const handleGetStorage = (data:StorageType[]) => { 
        setStorage(data);
        setShowModal(true);
    };


    const handleSelectAndClose = (area: string) => { 
        handleSelectArea(area);
        setShowModal(false);
        setTempStorage(initTempStorage);
    };
      //api------------------------------------
    const getStorage = async (position: string) => { 
         try {
             const res = await api.get(`api/storage/product/${product}/${tempStorage.area + position}`);
             const data = res.data.map((d:any, index:number) => {
                 return {
                     id: index,
                     ...d
                 };
             });
            setStockData(data);
        } catch (err: any) {
            enqueueSnackbar(err.message, { variant: 'error' });
            return;
        };
    };
    const getEmptyAsync = async () => { 
        try { 
            const res = await api.get("api/storage/empty");
            setLocationData(res.data);
        } catch (err: any) { 
            console.log(err.message);
        };
        
    };
    //useEffect-----------------------------------------------
    useEffect(() => { 
        getEmptyAsync();
    }, []);
    return (
        <Box>
             <AreaModal
                open={showModal}
                handleClose={() => setShowModal(false)}
                handleSelect={ handleSelectAndClose}
                storages={storage}
                area={tempStorage.area} />
            <Stack
                sx={{m:'auto'}}
                direction={{ xs: 'column', sm: 'column',md:'row' }}
                spacing={{ xs: 1, sm: 2, md: 4 }}
                justifyContent={tempStorage.area ?"space-around": "center"}
                alignItems={'center'}
            >
            <List sx={{ p:2, maxWidth: 350, bgcolor: '#34495E',height:250,border:2,borderColor:"#2C3E50",color:'white' }}>
                {locationData.map((located, index) =>
                    <div  key={index+1}>
                    <ListItem
                        secondaryAction={
                            <Checkbox
                                edge="end"
                                onChange={handleToggle(index+1)}
                                checked={index+1===checked}
                            />}
                    >
                        <ListItemButton  onClick={() => console.log(located.location)}> 
                            <ListItemAvatar>
                                <Avatar  sx={{ bgcolor: "#DC7633",width: 30, height: 30 }} >
                                    {located.area}
                                </Avatar>
                            </ListItemAvatar>
                            <ListItemText
                                primary={`${t("warehouse.purchase.area")} : ${located.area}`}
                                primaryTypographyProps={{
                                    fontSize: 18,
                                    fontWeight: 'bold',
                                    letterSpacing: 0,
                                }}
                            />
                             <ListItemText primary={`架位數量 : ${located.location.length}`}  />
                        </ListItemButton>    
                    </ListItem>
                      <Divider />
                    </div>
                )}
                </List>
             
                <List dense
                    sx={{
                        display:tempStorage.location.length>0?"":"none",
                        overflow: 'auto', p: 2, width: '90%', maxWidth: 400,
                        bgcolor: '#34495E', height: 250, border: 2, borderColor: "#2C3E50",color:'white'
                    }}
                >
                {tempStorage.location.map((ra,index) =>
                    <div key={index}>
                        <ListItem  secondaryAction={
                            <IconButton onClick={()=>handleGetStorage(ra.place)} >
                                <OpenInNewRoundedIcon color="success"  />
                            </IconButton>    
                        }>
                            <ListItemButton  onClick={() =>product&& getStorage(ra.num)}> 
                                <ListItemAvatar>
                                    <Avatar  sx={{ bgcolor: "#2E86C1",width: 30, height: 30 }} >
                                        {ra.num}
                                    </Avatar>
                                </ListItemAvatar>
                                <ListItemText
                                    primary={`架位 : ${ra.num}`}
                                    primaryTypographyProps={{
                                        fontSize: 18,
                                        fontWeight: 'bold',
                                        letterSpacing: 0,
                                    }}
                                />
                                <ListItemText
                                    primaryTypographyProps={{
                                         width:100,height:30,textAlign:'center',pt:1,
                                        fontSize: 18,
                                        fontWeight: 'bold',
                                            letterSpacing: 0,
                                        bgcolor:getColor(ra.place.length)
                                    }}
                                    primary={`${t("warehouse.purchase.remain")} : ${ra.place.length}`}
                                />
                            </ListItemButton>    
                        </ListItem>
                        <Divider />
                    </div> 
                )}
            </List>
            <AreaModal
                open={showModal}
                handleClose={() => setShowModal(false)}
                handleSelect={ handleSelectAndClose}
                storages={storage}
                area={tempStorage.area} />
            </Stack>
            <Divider sx={{mt:3}} />
            <Box
                sx={{
                    display:product?"":"none",
                    pt: 2, m: 'auto', height: 450, width: { xs: '100%', sm: '100%', md: '95%', lg: '95%' },
                    '& .MuiDataGrid-columnHeaders': { bgcolor: "#5D6D7E",color:'white' }
                }}>
                  <h2 style={{fontWeight:'bold'}}>{ t("warehouse.purchase.inventory")}</h2>
                <DataGrid
                    sx={{ 
                        fontSize:18,
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                        maxHeight:380
                    }}
                    density={small?'compact':"comfortable"}
                    rows={stockData}
                    columns={columns}
                    pageSize={10}
                    rowsPerPageOptions={[5]}
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
        </Box>
    );
};

export default AreaLocationContent;

// id:number
//     location: string,
//     flag_number: string,
//     status: string,
//     product_name: string,
//     remain: number

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="down" ref={ref} {...props} />;
});
const AreaModal = (props: {
    open: boolean,
    handleClose: () => void,
    area: string,
    storages: StorageType[],
    handleSelect: (area: string) => void,
}) => {

    const { open, handleClose, storages, area, handleSelect } = props;
    const { t } = useTranslation();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    
    return (
        <Dialog
            onClose={handleClose} open={open} fullScreen={fullScreen} TransitionComponent={Transition}>
            <DialogTitle id="dialog-title"
                sx={{
                    fontWeight: "bold",
                    height: 20, display: 'flex', flexDirection: 'row', justifyContent: 'space-between', alignItems: 'center', bgcolor: '#DC7633'
                }}
            >
                {` ${t('warehouse.purchase.area')}`}: {`${area}`}
                <IconButton onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent sx={{ minHeight: 200 ,bgcolor:"#B2BABB"}}>
                <TableContainer component={Paper} sx={{mt: {xs:10,sm:6,md:2}, maxHeight: {xs:750,sm:600,md:400 }}}>
                    <Table sx={{ minWidth: 400, }} aria-label="simple table" stickyHeader  >
                        <TableHead >
                            <TableRow>
                                <TableCell sx={{ width: "10%", color: 'white', bgcolor: '#34495E' }}>{t("warehouse.manage.choice")}</TableCell>
                                <TableCell align="center" sx={{ width: "40%", color: 'white', bgcolor: '#34495E' }}>{t("warehouse.purchase.remainRack")}</TableCell>
                                <TableCell align="center" sx={{ width: "50%", color: 'white', bgcolor: '#34495E' }}>{t("warehouse.manage.mark")}</TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {storages.map(p =>
                                <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} key={p.name}>
                                    <TableCell align="center" >
                                        <Button size="small" variant="contained" color="success" onClick={() => handleSelect(p.name)}>
                                            {t("warehouse.manage.choice")}
                                        </Button>
                                    </TableCell>
                                    <TableCell align="center">{p.name}</TableCell>
                                    <TableCell align="center">{p.remark}</TableCell>
                                </TableRow>)}
                        </TableBody>
                    </Table>
                </TableContainer>
            </DialogContent>
            {/* <DialogActions sx={{ alignItems:'center'}}>
          <Button onClick={handleFinish}>{t("warehouse.ok") }</Button>
        </DialogActions> */}
        </Dialog>
    );
};

