import { useSnackbar } from 'notistack';
import { useContext, useEffect, useState } from 'react';


import LaunchOutlinedIcon from '@mui/icons-material/LaunchOutlined';
import { Box, Button, Divider, useMediaQuery, useTheme } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import moment from 'moment';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';
import { DataType } from '../index';
import LocationModal from '../location-modal';
import { updateDataType } from '../location-modal/index';


export const GridBox = (props: {
    rows: any[],
    columns: GridColDef[];
    height:number
    bgColor?: string;
    density?:string
}) => {
  const theme = useTheme();
  const small = useMediaQuery(theme.breakpoints.down('sm'));
  return (
    <Box
      sx={{
        pt: 2, height: props.height,
        '& .MuiDataGrid-columnHeaders': { bgcolor:props.bgColor?props.bgColor:"#5D6D7E", color: 'white' }
      }}
    >
      <DataGrid
        sx={{
          fontSize: 16,bgcolor:'white',
          '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
            outline: 'none',
          },
              }}
              hideFooter
              headerHeight={40}
              rowHeight={40}
            density={small||props.density ? 'compact':"comfortable" }
            rows={props.rows}
            columns={props.columns}
            // pageSize={15}
            // rowsPerPageOptions={[5]}
            // showCellRightBorder
            experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  );
};

export interface OutDataType { 
    id: number | string;
    number: number;
    machinecode: string;
    product_name: string;
    product_number: string;
    quantity: number;
    totalCount:number
    locations:{location:string,count:number}[]|null
};

function Picking(
     props: {
        data: DataType
    }
) {

    const { t } = useTranslation();
    const { data } = props;
    const {auth}= useContext(AuthContext);
    const { employee_id } = auth;
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    //status------------------------------------------------------------
    const [finish, setFinish] = useState<boolean>(false);

    const [dataset, setDataset] = useState<OutDataType[]>([]);
    const [mainData, setMainData] = useState <OutDataType | null>(null);


    //function-----------------------------------------------------------
     const getItemData = (data: OutDataType[]) => {
        const res = data.map(item => {
            if (item.locations) {
                return item.locations.map(location => {
                    const [position, area] = location.location.split("-")
                    return {
                        "item_number": item.number,
                        "product_name": item.product_name,
                        "quantity": location.count,
                        "position": position,
                        "area": area,
                        "action": "out"
                    }
                })
            }
        });
       
        return res[0];
    };

    /**
     * * 選擇儲位和數量後更新至列表
     */
    const handleFinish = (updateData: updateDataType) => {
        let total = 0;
        const locations = updateData.locations.map(ele => {
            total += ele.count;
            return {
                location: ele.location,
                count: ele.count
            }
        });
        if (total < mainData!.quantity) {
            const ok = window.confirm("未達需求數量，請問要繼續嗎?");
            if (!ok) return;
        };
        const updated = { ...mainData!, locations: locations, totalCount: total };
        const updateDataset = [...dataset];
        const targetIndex = updateDataset.findIndex(ele => ele.number === updated.number);
        updateDataset.splice(targetIndex, 1, updated);
        setDataset(updateDataset);
        return setMainData(null);
    };
     const handleGetStorage = (data: OutDataType) => {
        setMainData(data);
        // setOpen(true);
    };
    //api----------------------------------------------------------------
     const getDatasetAsync = async () => {
        try {
            const res = await api.get(`api/operate/orders/wait/${data.orderid}`);
            const result:OutDataType[] = res.data.map((ele:any,index:number) => {
                return {
                    ...ele,
                    id:index,
                    totalCount:0,
                    locations: ""
                }
            });
            setDataset(result);
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar("讀取資料失敗，該資料已處理過或不存在。", { variant: 'error' });
         };
     };
    const handleFinishAsync = async() => { 
        try {
            getItemData(dataset);
            const body = {
                "order_id": data.orderid,
                "date": moment(Date.now()).format("YYYY-MM-DD"),
                "employee_id": employee_id,
                "machinecode": dataset.map(ele => ele.machinecode)[0],
                "orderitems": getItemData(dataset)
            };
     
            await api.post("api/management/orders/pending/out", body);
            enqueueSnackbar("送出成功。", { variant: 'success' });
            getDatasetAsync()
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar("送出失敗。", { variant: 'error' });
        }
    };
    //useEffect------------------------------------------------------------
      useEffect(() => {
        getDatasetAsync();
      }, [data]);
    
    
    useEffect(() => { 
        for (let data of dataset) {
            const { locations } = data;
            if (!locations) return setFinish(false);
        }
        return setFinish(true)
    }, [dataset]);
    
    return (
        <Box sx={{ p:{xs:0,sm:1,md:4} }}>
            {mainData &&
                <LocationModal
                    open={Boolean(mainData)}
                    data={mainData}
                    handleClose={() => setMainData(null)}
                    handleFinish={handleFinish} />}
          
            <Box sx={{ width: { xs: '100%', sm: '80%', md: '55%', lg: '50%' } }}>
                <GridBox rows={[data]} columns={titleColumns(t)} height={120} bgColor={"#C0392B"} />
            </Box>
            <Divider sx={{mt:4}} />
            <Box sx={{mt:2}}>
                <GridBox rows={dataset} columns={getColumns(t, handleGetStorage)} height={350} bgColor={"#C0392B"} />
            </Box>
            <Box >
               <Button
                    disabled={!finish}
                    color="success"
                    variant='contained'
                    sx={{ml:"45%",height:50,mt:4}}
                    onClick={handleFinishAsync}>
                    {t("warehouse.stockIn.finish")}
                </Button>   
            </Box>
        </Box>
    );
}

export default Picking;

const titleColumns = (
    t: TFunction,
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
    { field: 'number', headerName:t("warehouse.order.order"), width: 80, align: 'center', headerAlign: 'center' },
        {
            field: 'status', headerName: t("warehouse.order.status"), width: 120, align: 'center', headerAlign: 'center',
            valueGetter: (params: GridValueGetterParams) => t(`status.${params.row.status}`)
        },
    { field: 'datetime', headerName:t("warehouse.order.date"), width: 120, align: 'center', headerAlign: 'center' },
    { field: 'applicant', headerName:  t("warehouse.order.filler"), width: 200, align: 'center', headerAlign: 'center' },

    ];


const getColumns = (
    t: TFunction,
    handleGetStorage:(data:OutDataType)=>void
): GridColDef[] => [
        { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
        { field: 'number', headerName:  t("warehouse.order.no"), width: 80, align: 'center', headerAlign: 'center' },
        { field: 'product_name', headerName: t("warehouse.order.product"), width: 300, align: 'center', headerAlign: 'center' },
        { field: 'quantity', headerName: t("warehouse.order.count"), width: 120, align: 'center', headerAlign: 'center' },
        { field: 'totalCount', headerName: t("warehouse.order.realCount"), width: 120, align: 'center', headerAlign: 'center' },
        { field: 'machinecode', headerName: t("warehouse.order.mcode"), width: 150, align: 'center', headerAlign: 'center' },
        {
            field: 'locations', headerName: t("warehouse.order.locations"), width: 300, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { locations } = params.row;
                return (
                    <Button
                        sx={{fontSize:18}}
                        endIcon={<LaunchOutlinedIcon />}
                        onClick={() => handleGetStorage(params.row)} color="warning"
                    >
                        {locations ? locations?.map((ele:any) => `${ele.location}(${ele.count}) `):"選擇儲位"}
                    </Button>
                );
        }},
];