import React,{useEffect,useState} from 'react'
import { TreDatasetType } from '../treasury'
import { Dialog, DialogActions, DialogContent, DialogTitle, IconButton, styled, useTheme,useMediaQuery, Paper, Button, Stack, Box, Divider } from '@mui/material';
import { TFunction, useTranslation } from 'react-i18next';
import CloseIcon from '@mui/icons-material/Close';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import api from '../../../../api';
import Checkbox from '@mui/material/Checkbox';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';


const mainColor = "#DC7633";
const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor:  '#F1948A',
    color: "black",
    
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="down" ref={ref} {...props} />;
});

interface EmptyAreaType { 
    id: number|string;
    checked: boolean,
    name: string,
    remark:string
};
interface InventoryType{
    id:number|string,
    checked:boolean
    expect_remain: number
    flag_number: string
    location: string
    product_name:string
    remain: number
    update_time:string
};


function StorageModal(
    props: {
        open: boolean;
        data: TreDatasetType;
        handleClose: () => void;
        handleFinish: (area:string) => void;
}) {
    const { open,data, handleClose,handleFinish } = props;
    const { t } = useTranslation();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    //useState----------------------------------------------------------
    const [checkData, setCheckData] = useState<InventoryType[]>([]);
    const [emptyData, setEmptyData] = useState<EmptyAreaType[]>([]);
    const [target, setTarget] = useState<string>('');
    //function-----------------------------------------------------------
    const mapKey = (data: any[]) => {
        return data.map((d, i) => {
            return {
                id: i,
                ...d
            }
        });
    }
    const handleAreaChange = (event: React.ChangeEvent<HTMLInputElement>) => { 
        const name = event.target.name;
        setTarget(name);
    };
    //api-----------------------------------------------------------------
    const getStorageAsync = async () => { 
        try {
            const res1 = await api.get(`api/storage/product/${data.product_number}`);
            const res2 = await api.get("api/storage/empty");
            const inData = res1.data.map((ele: any,index:number) => { return { ...ele, checked: false,id:index } });
            let results:EmptyAreaType[]=[];
            res2.data.forEach((ele: any) => {
                ele.location.forEach((locat: any) => {
                    locat.place.forEach((plc: any) => {
                        results = [...results, { ...plc,checked:false}];
                    });
                });
            });
            setEmptyData(mapKey(results));
            setCheckData(inData);
        } catch (err: any) {
            console.log(err.message)
        }
    };
    //useEffect---------------------------------------------------------
    useEffect(() => { 
        getStorageAsync();
    }, [data]);

    return (
        <Dialog
            open={open}
            maxWidth={"md"}
            fullScreen={fullScreen}
            fullWidth={true}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-storage"
        >
            <DialogTitle sx={{ color:'white',display: 'flex', justifyContent: 'space-between', bgcolor: mainColor , fontSize: 25, fontWeight: 'bold' }}>
                {"選擇入庫位置"}
                <IconButton onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent sx={{ bgcolor: '#F9E79F',minHeight:500 }}>
                <h3>{t("warehouse.order.stockInNew")}</h3>
                <TableContainer component={Paper} sx={{m:{xs:'auto',sm:"auto",md:0},mt:2,width: 350}}> 
                    <Table aria-label="spanning table" size="small" >
                         <TableHead >
                                <TableRow>
                                    <StyledTableCell align="center">{t("warehouse.order.product")}</StyledTableCell>
                                    <StyledTableCell align="center" >{t("warehouse.order.count")}</StyledTableCell>
                                </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <StyledTableCell align="center" >{data.product_name}</StyledTableCell>
                                <StyledTableCell align="center" >{data.quantity}</StyledTableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
                <Divider sx={{mt:2}} />
                <Stack
                    direction={{ xs: "column", sm: "column", md: "row" }}
                    justifyContent={{ xs: "center", sm: "center", md: "space-around" }}
                    alignItems={"center"}
                    spacing={{ xs: 8, sm: 8, md: 1 }}
                    sx={{mt:2,mb:4,border:0,minHeight:300}}
                >
                    <Box id="location-information"
                        sx={{
                            height: 250, width:400,
             
                            '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                        }}>
                          <h3>{t("warehouse.order.inventoryNew")}</h3>
                        <DataGrid
                            sx={{ 
                                    fontSize:18,bgcolor:'white',
                                    '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                        outline: 'none',
                                    },
                                
                                }}
                                hideFooter
                                density={'compact'}
                                rows={checkData}
                                columns={checkColumns(t,target,handleAreaChange)}
                                // pageSize={10}
                                // rowsPerPageOptions={[5]}
                                experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
                    <Box id="empty-location-information"
                        sx={{
                            height: 250, width:400,
                            '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                        }}>
                        <h3>{t("warehouse.order.emptyNew")}</h3>
                        <DataGrid
                            sx={{ 
                                    fontSize:18,bgcolor:'white',
                                    '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                        outline: 'none',
                                    },
                                }}
                                hideFooter
                                density={'compact'}
                                rows={emptyData}
                                columns={emptyColumns(t,target,handleAreaChange)}
                                // pageSize={10}
                                // rowsPerPageOptions={[5]}
                                experimentalFeatures={{ newEditingApi: true }}
                        />
                    </Box>
                </Stack>
                <Divider sx={{mt:10}} />
            </DialogContent>
            <DialogActions sx={{ bgcolor: mainColor  }}>
                <Button
                    sx={{m:'auto'}}
                    onClick={() => handleFinish(target)}
                    variant={"contained"} color="success">{t("warehouse.sure")}</Button>
            </DialogActions>
        </Dialog>
    );
};

export default StorageModal;


const checkColumns = (
    t: TFunction,
    checkEqual:string,
    handleAreaChange:(event: React.ChangeEvent<HTMLInputElement>)=>void
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
        {
            field: 'checked', headerName: t("warehouse.order.choice"), width: 80, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { checked,flag_number,location} = params.row;
                return (
                    <Checkbox checked={location===checkEqual} size="small" name={location} onChange={handleAreaChange} />
                )
            }
        },
    { field: 'location', headerName: t("warehouse.order.rack"), width: 80, align: 'right', headerAlign: 'center' },
    { field: 'flag_number', headerName: t("warehouse.order.mcard"), width: 150, align: 'right', headerAlign: 'center' },
    // { field: 'status', headerName: t("warehouse.order.status"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'remain', headerName: t("warehouse.order.count"), width: 120, align: 'center', headerAlign: 'center' }
    
      
    ];

const emptyColumns = (
    t: TFunction,
    checkEqual:string,
    handleAreaChange:(event: React.ChangeEvent<HTMLInputElement>)=>void
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
        {
            field: 'checked', headerName: t("warehouse.order.choice"), width: 80, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { name} = params.row;
                return (
                    <Checkbox checked={name===checkEqual} size="small" name={name} onChange={handleAreaChange} />
                )
            }
        },
    { field: 'name', headerName: t("warehouse.order.rack"), width: 150, align: 'center', headerAlign: 'center' },
    { field: 'remark', headerName: t("warehouse.order.other"), width: 120, align: 'center', headerAlign: 'center' }
    
      
];