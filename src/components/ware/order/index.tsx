import { useSnackbar } from 'notistack';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';

import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import { Box, IconButton, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useContext } from 'react';
import { TFunction } from 'react-i18next';
import api from '../../../api';
import { WareContext } from '../../../provider/wareProvider/index';
import Picking from './picking/index';
import Treasury from './treasury';

export const enum StatusType { 
    in= "in",
    out= "out",
    none="none"
};


export interface DataType {
    id: number;
    applicant: string;
    datetime: string;
    number: number;
    orderid: string
    status: string;
    type: string;
};

function WareOrder() {


    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const { dataPack,onSignFor} = useContext(WareContext);

    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('md'));
    //state------------------------------------------------------------
    const [dataset, setDataset] = useState<DataType[]>([]);
    const [selectData, setSelectData] = useState<DataType>();
    const [pageStatus, setPageStatus] = useState<StatusType>(StatusType.none);
    const [showModal, setShowModal] = useState<boolean>(false);
    //function----------------------------------------------------------
    /**選擇資料類別 */
    const handleSelect = (data: DataType) => { 
        const { type } = data;
        setShowModal(true);
        setSelectData(data);
        switch (type) {
            case 'in':
                setPageStatus(StatusType.in);
                break;
            case "out":
                setPageStatus(StatusType.out);
                break;
            default:
                setPageStatus(StatusType.none);
                break;
        };
    };
    const handleCloseModal = () => { 
        setShowModal(false);
        setPageStatus(StatusType.none);
    };
    //api--------------------------------------------------------------
    const getOrderDatasetAsync = async () => { 
        try {
            const res = await api.get("api/operate/orders/wait");
            if (res.data.length === 0) {
                enqueueSnackbar("沒有任何資料。", { variant: 'warning' });
            };
            const result = res.data.map((item: any, index: number) => {
                return {
                    id: index,
                    ...item
                };
            });
            if (dataPack.orderId) { 
                const target = result.filter((ele:any)=> ele.orderid === dataPack.orderId)[0];
                if (target) {
                    handleSelect(target);
                    onSignFor()
                }
            }; 
            setDataset(result);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("讀取資料失敗", { variant: 'error' });
        }
    };
    //useEffect--------------------------------------------------------
     useEffect(() => {
        getOrderDatasetAsync();
    }, [dataPack]);
    return (
        <Box sx={{ bgcolor: '#FBFCFC', width: '100%', minHeight: '100vh', mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                    { t("warehouse.order.orderList")}
                </Typography >
            </Toolbar>
            <Box
                sx={{
                    pt: 1, m: 'auto', height: 600, width: { xs: '100%', sm: '100%', md: '75%', lg: '75%' },
                    '& .MuiDataGrid-columnHeaders': { bgcolor: "#5D6D7E", color: 'white' }
                }}>
                <h2 style={{ fontWeight: 'bold' }}>{ t("warehouse.order.orderList")}</h2>
                 <DataGrid
                    sx={{ 
                        fontSize:18,
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                        maxHeight:600
                    }}
                    hideFooter
                    density={'compact'}
                    rows={dataset}
                    columns={columns(handleSelect,t,small)}
                    // pageSize={10}
                    // rowsPerPageOptions={[5]}
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
            {/* <Divider sx={{mt:2 }} />
            <Box>
                {selectData && (
                    pageStatus === "in" ?<Treasury data={selectData} />:<Picking  data={selectData} />
                        // <TreasuryPage data={selectData} />:<PickingPage data={selectData} />      
                )}
            </Box> */}
              <Dialog
                fullScreen={small}
                open={showModal}
                onClose={handleCloseModal}
                fullWidth={true}
                maxWidth={'lg'}
                >
                <DialogTitle id="select-data-title" sx={{ height:50,bgcolor: "#CA6F1E", color: 'white', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{width:350,display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                        <p>單號: {selectData?.orderid}</p>
                        <p>類別: {selectData?.type}</p>
                    </div>
                  
                    <IconButton onClick={handleCloseModal}>
                        <CloseRoundedIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent >
                    {selectData && (
                        pageStatus === "in" ?<Treasury data={selectData} />:<Picking  data={selectData} />
                    )}
                </DialogContent>
             
            </Dialog>
        </Box>
    );
};

export default WareOrder;



const columns = (
    checkStatus: (data:DataType) => void,
    t: TFunction,
    small: boolean,
    
): GridColDef[] => [
        {
            field: 'check1', headerName: t("warehouse.order.check"), width: 150, align: 'center', headerAlign: 'center',hide:!small,
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <IconButton onClick={() => checkStatus(params.row)}>
                        <ArrowCircleRightOutlinedIcon color="success" />
                    </IconButton>
                );
            }
        },
        { field: 'id', headerName: 'ID', width: 90, align: 'center', headerAlign: 'center', hide: true },
        { field: 'number', headerName: t("warehouse.order.item"), width: 90, align: 'center', headerAlign: 'center',hide:small },
        { field: 'datetime', headerName: t("warehouse.order.date"), width: 200, align: 'left', headerAlign: 'left', hide: small },
        {
            field: 'type', headerName: t("warehouse.order.type"), width: 120, align: 'center', headerAlign: 'center', hide: small,
          valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)},
        {
            field: 'status', headerName: t("warehouse.order.status"), width: 120, align: 'center', headerAlign: 'center',
      valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)    },
        { field: 'orderid', headerName: t("warehouse.order.order"), width: 200, align: 'right', headerAlign: 'right' },
        { field: 'applicant', headerName: t("warehouse.order.filler"), width: 150, align: 'center', headerAlign: 'center' },  
        {
            field: 'check', headerName: t("warehouse.order.check"), width: 100, align: 'center', headerAlign: 'center',hide:small,
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <IconButton onClick={() => checkStatus(params.row)}>
                        <ArrowCircleRightOutlinedIcon color="success" />
                    </IconButton>
                );
            }
        }
];