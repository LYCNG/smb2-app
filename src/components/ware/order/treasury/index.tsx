import LaunchOutlinedIcon from '@mui/icons-material/LaunchOutlined';
import { Box, Button, Divider, FormControl, MenuItem, Select, SelectChangeEvent, useMediaQuery, useTheme } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import { useContext, useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';
import { DataType } from '../index';
import StorageModal from '../storage-modal';


export const GridBox = (props: {
    rows: any[],
    columns: GridColDef[];
    height:number
    bgColor?: string;
    density?:string
}) => {
  const theme = useTheme();
  const small = useMediaQuery(theme.breakpoints.down('sm'));
  return (
    <Box
      sx={{
        pt: 2, height: props.height,
        '& .MuiDataGrid-columnHeaders': { bgcolor:props.bgColor?props.bgColor:"#5D6D7E", color: 'white' }
      }}
    >
      <DataGrid
        sx={{
          fontSize: 16,bgcolor:'white',
          '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
            outline: 'none',
          },
              }}
              hideFooter
              headerHeight={40}
              rowHeight={40}
            density={small||props.density ? 'compact':"comfortable" }
            rows={props.rows}
            columns={props.columns}
            // pageSize={15}
            // rowsPerPageOptions={[5]}
            // showCellRightBorder
            experimentalFeatures={{ newEditingApi: true }}
      />
    </Box>
  );
};

export interface TreDatasetType {
    id: number;
    number: number;
    machinecode: string;
    product_name: string;
    product_number: string;
    quantity: number;
    reason: string;
    status: string;
    location: string;
};  

function Treasury(
    props: {
        data: DataType
}) {

    const { t } = useTranslation();
    const { data } = props;
    const {auth} =useContext(AuthContext);
    const { employee_id } = auth; 
    const { enqueueSnackbar} = useSnackbar();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    //state--------------------------------------------------------------------
    const [finish, setFinish] = useState<boolean>(false);
    const [reasons, setReasons] = useState<string[]>([]);
    const [status, setStatus] = useState<string[]>([]);
    const [dataset, setDataset] = useState<TreDatasetType[]>([]);
    const [targetData, setTargetData] = useState<TreDatasetType | null>(null);

    //function------------------------------------------
     const handleReasonChange = (event: SelectChangeEvent, index: number) => {
        const value = event.target.value as string;
        const target = dataset[index];
        const updateDataset = [...dataset];
        updateDataset.splice(index, 1, { ...target, reason: value });
        return setDataset(updateDataset);
    };
    const handleStatusChange = (event: SelectChangeEvent, index: number) => {
        const value = event.target.value as string;
        const target = dataset[index];
        const location = (value === '良品') ? target.location : '報廢庫';
        const updateDataset = [...dataset];
        updateDataset.splice(index, 1, { ...target, status: value,location :location  });
        return setDataset(updateDataset);
    };
    const handleSelectStorage = (data:TreDatasetType) => { 
        setTargetData(data);
    };
    const handleSelectAreaFinish = (area: string) => {
        if (!area) return;
        if (targetData) {
            const updatedDataset = [...dataset];
            const targetIndex = updatedDataset.findIndex(ele => ele.id === targetData.id);    
            updatedDataset.splice(targetIndex, 1, { ...targetData, location: area });
            setDataset(updatedDataset);
            return setTargetData(null);
        };
    };
    //api-----------------------------------------------
     const getSelectType = async () => { 
        const reason_res = await api.get("api/dropdown/items/inreason");
        const status_res = await api.get("api/dropdown/items/status");
        setReasons(reason_res.data);
        setStatus(status_res.data)
    };
    const getDatasetAsync = async () => {
        try {
            const res = await api.get(`api/operate/orders/wait/${data.orderid}`);
    
            const result: TreDatasetType[] = res.data.map((ele: any,index:number) => {
                return {
                    id:index,
                    reason: "",
                    status: '',
                    location: ele.status==='良品'?'':'D01-1',
                    ...ele,
                }
            });
            setDataset(result);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("讀取資料失敗。", { variant: "error" })
        }
    };
    const handleFinishAsync = async () => { 
           //只取良品 過濾掉不良
        const postData = dataset
  
        for (let data of postData) {
            if (!data.status || !data.location || !data.reason) {
                enqueueSnackbar("未填完整，請重新檢查。", { variant: "warning" });
                return;
            };
         };
       
        const body = {
            "order_id": data.orderid,
            "date": moment(Date.now()).format("YYYY-MM-DD"),
            "employee_id": employee_id,
            "machinecode": postData.map(ele=>ele.machinecode)[0],
            "orderitems": postData.map(data => {
                const [position,area] = data.location.split("-");
                return {
                    "item_number": data.number,
                    "product_name":data.product_name,
                    "quantity": data.quantity,
                    "position": position,
                    "area": area,
                    "action": "in",
                    "flag_number": "",
                    "reason": data.reason,
                    "status": data.status
                }
            })   
        };

        try {
            await api.post("api/management/orders/pending/in", body);
            enqueueSnackbar("送出成功", { variant: "success" })
            getDatasetAsync();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("送出失敗",{variant:"error"})
        }
    };
    //useEffect-----------------------------------------
    useEffect(() => {
        for (let data of dataset) {
            const { reason, status, location } = data;
            if (!reason || !status || !location) return setFinish(false);
        };
        return setFinish(true);
    }, [dataset]);
    
     useEffect(() => { 
        getDatasetAsync();
        getSelectType();
     }, [data]);
    
    return (
        <Box sx={{ p:{xs:0,sm:1,md:4}}}>
            {targetData && <StorageModal open={Boolean(targetData)} data={targetData} handleClose={() => setTargetData(null)} handleFinish={handleSelectAreaFinish} />}
            
            <Box  sx={{ width: { xs: '100%', sm: '100%', md: '80%', lg: '70%' } }}>
                <GridBox
                rows={[data]}
                columns={titleColumns(t)}
                height={120}
                bgColor={ "#CA6F1E"}
            />
            </Box>
             <Divider sx={{mt:4}} />
            <Box sx={{mt:2}}>
                <GridBox
                    rows={dataset}
                    columns={getColumns(reasons, status, handleReasonChange, handleStatusChange, handleSelectStorage, t)}
                    height={300}
                    bgColor={"#CA6F1E"}
                />
            </Box>
            <Box >
               <Button
                    // disabled={finish}
                    color="success"
                    variant='contained'
                    sx={{ml:"45%",height:50,mt:4}}
                    onClick={handleFinishAsync}>
                    {t("warehouse.stockIn.finish")}
                </Button>   
            </Box>
        </Box>
    );
}

export default Treasury;


const titleColumns = (
    t: TFunction,
): GridColDef[] => [
        { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
        { field: 'number', headerName: t("warehouse.order.no"), width: 80, align: 'center', headerAlign: 'center' },
        { field: 'datetime', headerName: t("warehouse.order.date"), width: 150, align: 'center', headerAlign: 'center' },
        {
            field: 'status', headerName: t("warehouse.order.status"), width: 120, align: 'center', headerAlign: 'center',
      valueGetter:(params:GridValueGetterParams)=>t(`status.${params.row.status}`)    },
        { field: 'orderid', headerName: t("warehouse.order.order"), width: 200, align: 'center', headerAlign: 'center' },
        { field: 'applicant', headerName: t("warehouse.order.filler"), width: 150, align: 'center', headerAlign: 'center' },
    ];

const getColumns = (
    reasons: string[],
    statuses:string[],
    handleReasonChange: (event: SelectChangeEvent,id: number) => void,
    handleStatusChange: (event: SelectChangeEvent, id: number) => void,
    handleSelectStorage:(data:TreDatasetType)=>void,
    t: TFunction,

    // small: boolean
): GridColDef[] => [
        { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
        { field: 'number', headerName: t("warehouse.order.no"), width: 80, align: 'center', headerAlign: 'center' },
        { field: 'product_name', headerName: t("warehouse.order.product"), width: 200, align: 'center', headerAlign: 'center' },
        { field: 'quantity', headerName: t("warehouse.order.count"), width: 120, align: 'center', headerAlign: 'center' },
        { field: 'machinecode', headerName: t("warehouse.order.mcode"), width: 120, align: 'center', headerAlign: 'center' },
        {
            field: 'reason', headerName: t("warehouse.order.reason"), width: 150, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { id,reason } = params.row;
                return (
                    <FormControl fullWidth >
                        <Select
                        labelId="reason-select-label"
                        id="select-reason-menu"
                        size="small"
                        value={reason}
                        onChange={(event)=>handleReasonChange(event,id)}
                    >
                        {reasons.map(r =>
                            <MenuItem value={r} key={r}>{r}</MenuItem>)
                        }
                        </Select>
                    </FormControl>
                )
            }
        },
        {
            field: 'status',
            headerName: t("warehouse.order.status"),
            width: 150, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { id, status } = params.row;
                return (
                    <FormControl fullWidth>
                        <Select
                            labelId="status-select-label"
                            id="select-status-menu"
                            value={status}
                            size="small"
                            onChange={(event) => handleStatusChange(event, id)}
                        >
                            {statuses.map(s =>
                                <MenuItem value={s} key={s}>{t(`${s}`)}</MenuItem>)
                            }
                        </Select>
                    </FormControl>
                );
            }
        },
        
        {
            field: 'location',
            headerName: t("warehouse.order.information"),
            width: 250, align: 'center', headerAlign: 'center', sortable: false, filterable: false, disableColumnMenu: true,
            renderCell: (params: GridValueGetterParams) => {
                const { location,status } = params.row;
                return (
                    <Button
                        disabled={status!=='良品'}
                        sx={{ fontSize: 20 }}
                        color={location ? "success" : "warning"}
                        endIcon={<LaunchOutlinedIcon />}
                        onClick={() => handleSelectStorage(params.row)}>
                        {location ? location : '選擇儲位'}
                    </Button>
                );
            }
        },
];