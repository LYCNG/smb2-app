import CloseIcon from '@mui/icons-material/Close';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, Divider, IconButton, Paper, styled, TextField, useMediaQuery, useTheme } from '@mui/material';
import Slide from '@mui/material/Slide';
import { TransitionProps } from '@mui/material/transitions';
import React, { useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';

import Checkbox from '@mui/material/Checkbox';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import moment from "moment";
import { useSnackbar } from 'notistack';
import api from '../../../../api';
import { GridBox, OutDataType } from '../picking';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor:  '#BB8FCE',
    color: "black"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Slide direction="down" ref={ref} {...props} />;
});

interface LocationDataType { 
    id: number | string;
    expect_remain: number;
    flag_number: string;
    location: string;
    product_name: string;
    remain: number;
    update_time: string;
    checked: boolean;
    count:number
};
export interface updateDataType { 
    locations: any[];
    total: number;
};

function LocationModal(
    props: {
        open: boolean;
        data: OutDataType;
        handleClose: () => void;
        handleFinish: (data: updateDataType) => void    
    }
) {
    const { open, data, handleClose, handleFinish } = props;
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    //useState----------------------------------------------------------
    const [checkData, setCheckData]
        = useState<LocationDataType[]>([]);
    const [updateData, setUpdateData]
        = useState<updateDataType>({ locations: [], total: 0 });
    
    //function-----------------------------------------------------------
     const handleEditCount = (event: React.ChangeEvent<HTMLInputElement>, flag: string) => {
        const { total } = updateData;
        let value = parseInt(event.target.value);
        if (parseInt(event.target.value) < 0||!value) {
            value = 1;
        };
        if ((total + value) > data.quantity) {
            return enqueueSnackbar("拿取數量不可大於需求數量。", { variant: "warning" });
        }
        const updated = [...checkData];
        const targetIndex = updated.findIndex(ele => ele.flag_number === flag);
        const target = updated[targetIndex];
        if (value > target.remain) {
            return enqueueSnackbar("拿取數量不可多於剩餘數量。", { variant: "warning" });
        }
        updated.splice(targetIndex, 1, { ...target, count: value });
        return setCheckData(updated);
    };
    /**
     * * 儲位選擇 
     * @param event 
     */
    const handleLocationChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        const targetIndex = checkData.findIndex(ele => ele.flag_number === event.target.name); 
        const currentTotal = checkData.reduce((a, b) => a + b.count,0);
        if (currentTotal>=data.quantity) { 
            enqueueSnackbar("已達需求", { variant: 'warning' })
            return;
        };
        const updateData = [...checkData];
        const target = updateData[targetIndex];
        const count = (target.remain > data.quantity) ? data.quantity : target.remain;
        updateData.splice(targetIndex, 1, { ...target, count: count-currentTotal, checked: event.target.checked });
        setCheckData(updateData);
    };

    const handleFinishSet = () => { 
        const result = checkData.filter(ele => ele.checked);
        handleFinish({ ...updateData, locations: result });
    };
    //api-----------------------------------------------------------------
     const getStorageAsync = async () => { 
        try {
            const res = await api.get(`api/storage/productAvailable/${data.product_number}`);
            const result:LocationDataType[] = res.data.map((ele:any,index:number) => { return { ...ele, checked: false,count:0,id:index } });
            setCheckData(result);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("讀取資料失敗。",{variant:"error"})
        };
    };
    //useEffect-----------------------------------------------------------
    useEffect(() => { 
        getStorageAsync()
    }, [data]);


    return (
        <Dialog
            open={open}
            maxWidth={"md"}
            fullScreen={fullScreen}
            fullWidth={true}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-storage"
        >
            <DialogTitle sx={{ color:'white',display: 'flex', justifyContent: 'space-between', bgcolor: '#DC7633', fontSize: 25, fontWeight: 'bold' }}>
                {t("warehouse.order.spaceDistribute")}
                <IconButton onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent sx={{ bgcolor: "#F9E79F" }}>
                 <h3>{t("warehouse.order.pickUpNews")}</h3>
                <TableContainer component={Paper} sx={{mt:2,maxWidth: 400,border:0}}> 
                    <Table  aria-label="spanning table" size={'small'}>
                         <TableHead>
                                <TableRow>
                                    <StyledTableCell align="center" sx={{width:"50%"}}>{ t("warehouse.order.product")}</StyledTableCell>
                                    <StyledTableCell align="center" sx={{width:"10%"}}>{ t("warehouse.order.count")}</StyledTableCell>
                                </TableRow>
                        </TableHead>
                        <TableBody>
                            <TableRow>
                                <StyledTableCell align="center" sx={{ width: "50%" }}>{data.product_name}</StyledTableCell>
                                <StyledTableCell align="center" sx={{width:"30%"}}>{data.quantity}</StyledTableCell>
                            </TableRow>
                        </TableBody>
                    </Table>
                </TableContainer>
                <Divider sx={{mt:2}} />
                <Box sx={{ m: 'auto' }}>
                    <h3>{t("warehouse.order.spaceNews") }</h3>
                    <GridBox rows={checkData} columns={getColumns(t,fullScreen , handleLocationChange,handleEditCount)} height={300} density={"compact"} />
                </Box>
                
            </DialogContent>
            <DialogActions sx={{bgcolor:'#DC7633'}}>
                <Button
                    sx={{m:'auto'}}
                    onClick={handleFinishSet} variant={"contained"} color="success">{t("warehouse.sure")}</Button>
        </DialogActions>
        </Dialog>
    );
}

export default LocationModal;


const getColumns = (
    t: TFunction,
    small:boolean,
    handleLocationChange:(event: React.ChangeEvent<HTMLInputElement>)=>void,
    handleEditCount: (event: React.ChangeEvent<HTMLInputElement>, flag: string) => void
    
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
        {
            field: 'checked', headerName: t("warehouse.order.choice"), width: 80, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { checked, flag_number } = params.row;
                
                return (
                    <Checkbox checked={checked} size="small" name={flag_number} onChange={handleLocationChange}/>
                )
            }
        },
        {
            field: 'update_time', headerName: t("warehouse.order.date"), width: 230, align: 'center', headerAlign: 'center',hide:small,
            valueGetter: (params: GridValueGetterParams) =>
                `${moment(params.row.update_time).format("YYYY-MM-DDTHH:mm:ss")}`
        },
    { field: 'flag_number', headerName: t("warehouse.order.order"), width: 150, align: 'center', headerAlign: 'center' ,hide:small},
    { field: 'location', headerName:t("warehouse.order.rack"), width: 120, align: 'center', headerAlign: 'center' },
    { field: 'remain', headerName: t("warehouse.order.realCount"), width: 120, align: 'center', headerAlign: 'center' },
        {
            field: 'count', headerName: t("warehouse.order.take"), width: 120, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const {checked,count ,flag_number} = params.row;
                return (
                     <TextField
                        disabled={!checked}
                        size="small"
                        type="number"     
                        value={count}
                        onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleEditCount(event, flag_number)}
                        inputProps={{ style: { maxWidth: 50, textAlign: 'center' } }}
                        />
                );
            }
        },
        {
            field: 'flag_number1', headerName: t("warehouse.order.order"), width: 150, align: 'center', headerAlign: 'center', hide: !small,
            valueGetter: (params: GridValueGetterParams) =>
                `${params.row.flag_number}`
        },
        
];