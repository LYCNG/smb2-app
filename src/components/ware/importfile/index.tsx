import React, { useState,useRef,useEffect  } from 'react';
import { Box, Button, Divider,  Paper, Stack, Typography, Toolbar } from '@mui/material';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import UploadFileOutlinedIcon from '@mui/icons-material/UploadFileOutlined';
import { makeStyles } from '@mui/styles';
import RestartAltOutlinedIcon from '@mui/icons-material/RestartAltOutlined';
import { useSnackbar } from 'notistack';
import * as XLSX from 'xlsx';
import api from '../../../api';
import SendIcon from '@mui/icons-material/Send';
import LinearProgress from '@mui/material/LinearProgress';
import Modal from '@mui/material/Modal';
import LoadingButton from '@mui/lab/LoadingButton';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
    "& .MuiTableCell-root": {
      borderLeft: "1px solid rgba(224, 224, 224, 1)"
    }
  }
});
const style = {
  position: 'absolute' as 'absolute',
  top: '50%',
  left: '50%',
  transform: 'translate(-50%, -50%)',
  width: 400,
  bgcolor: 'background.paper',
  border: '2px solid #000',
  boxShadow: 24,
  p: 4,
};
interface FileType { 
    name: string;
    data: any[];
    col: any[];
};

const make_cols = (refstr:any) => {
  let o = [],
    C = XLSX.utils.decode_range(refstr).e.c + 1;
  for (var i = 0; i < C; ++i) o[i] = { name: XLSX.utils.encode_col(i), key: i };
  return o;
};

function WareImportFile() {
    const classes = useStyles();
     const { enqueueSnackbar } = useSnackbar();
    const [file, setFile] = useState<File|null>(null);
    const [array, setArray] = useState<{ [index: string]: string }[]>([]);
    const [fileName, setFileName] = useState<string>("");

    const [dataFile, setDataFile] = useState<FileType | null>(null);
    const [converting, setConverting] = useState<boolean>(false);
     const [submitting, setSubmitting] = useState<boolean>(true);

    const csvInputRef = useRef<HTMLInputElement | null>(null);

    const handleOnChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        if (e.target.files && e.target.files[0]) {
            const files = e.target.files[0];
            const type = files.type;
            const reader = new FileReader();
            const rabs = !!reader.readAsBinaryString;
            reader.onload = (e) => { 
                 setConverting(true)
                try {
                    const result = e.target!.result;
                    if (type === "text/csv") {
                        setFileName(files.name)
                        setFile(files);
                    };
                    if (type === "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" && result) {
                        const wb = XLSX.read(result, { type: rabs ? "binary" : "array" });
                        const wsname = wb.SheetNames[0];
                        const ws = wb.Sheets[wsname];
                        const json = XLSX.utils.sheet_to_json(ws, { header: 1 });
                        const cols = json[0] as any[];
                        json.splice(0, 1)//移除[品名 品號]
                        setDataFile({
                            name: wsname,
                            data: json,
                            col:cols
                        });
                    };
                } catch (err: any) { 
                    enqueueSnackbar("無法解析檔案", { variant: 'error' });
                    return;
                } finally {
                    setConverting(false);
                 };
               
            };
            if (rabs) reader.readAsBinaryString(files);
            else reader.readAsArrayBuffer(files);
         };
    };

    const handleInputClick = () => { 
        if (csvInputRef && csvInputRef.current) {
            csvInputRef.current.click();
        };
    };

    const csvFileToArray = async(string: string) => {
        try {
            const csvHeader = string.slice(0, string.indexOf("\n")).split(",");
            const csvRows = string.slice(string.indexOf("\n") + 1).split("\n");
            const array = csvRows.map(i => {
                const values = i.split(",");
                const obj = csvHeader.reduce((object:{[index: string]:string}, header, index) => {
                    object[header] = values[index].replaceAll(`"`,'');
                    return object;
                }, {});
                return obj;
            });
            setArray(array);
        } catch (err: any) { 
            enqueueSnackbar("無法解析檔案",{variant:'error'})
        };
        
    };

    // const handleOnSubmit = () => {
    //     if (file) {
    //         fileReader.onload = function (event) {
    //             const csvOutput = event?.target?.result;
    //             csvFileToArray(csvOutput as string);
    //         };
    //         fileReader.readAsText(file);
    //     };
    // };

    const headerKeys = Object.keys(Object.assign({}, ...array));

    const handleSubmitAsync = async () => {
        if (dataFile) {
             setSubmitting(true)
            const body = dataFile.data.map(data => {
                return {
                    "product_name": data[0],
                    "product_number": data[1]
                };
            });
            try { 
                await api.post('api/product/upload', body);
                enqueueSnackbar("檔案上傳成功。", { variant: 'success' });
                
            } catch (err: any) { 
                console.log(err)
                enqueueSnackbar("檔案上傳失敗。", { variant: 'error' });
            } finally {
                setSubmitting(false);
                setDataFile(null);
            };
        }
        
    };
    
    const handleFileClear = () => { 
        setFile(null);
        setFileName('');
        setArray([]);
        setDataFile(null);
        return;
    };

    return (
        <Box sx={{ bgcolor: '#FBFCFC', width: '100%', minHeight: 850, mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{ pt: 2, pb: 2, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                    匯入CSV
                </Typography >
            </Toolbar>
            <Stack
               direction="row" alignItems="center" justifyContent={"space-between"}
               sx={{p:2,width:'80%',m:'auto'}}
            >
                <input
                    ref={csvInputRef}
                    style={{ display: 'none' }}
                    type={"file"}
                    id={"csvFileInput"}
                    accept={".xlsx, .xls, .csv"}
                    onChange={handleOnChange}
                />
                <Button
                    variant="contained"
                    color="success"
                    sx={{width:160}}
                    startIcon={<UploadFileOutlinedIcon />}
                    onClick={handleInputClick}
                >
                    匯入CSV
                </Button>
                <Button
                    variant="contained"
                    color="error"
                    sx={{width:100}}
                    startIcon={<RestartAltOutlinedIcon  />}
                    onClick={handleFileClear}
                >
                    清除
                </Button>
            </Stack>
            <Modal
                keepMounted
                open={converting}
            >
                <Box sx={style}>
                    <LinearProgress color="success" />
                </Box> 
            </Modal>
            <Divider sx={{width:"98%",m:'auto',mt:2}} />
            {!dataFile  && 
                <Box
                    onClick={handleInputClick}
                    sx={{
                        border: 1, borderColor: '#9E9E9E', fontSize: 24, fontWeight: 'bold',
                        color: '#747373', width: '90%', height: 300, textAlign: 'center', m: 'auto', pt: 20,
                        mt:10
                }}>
                    請先上傳csv檔案。
               </Box>}
           {dataFile&&!converting ? (
            <>
                <TableContainer component={Paper} sx={{ m:'auto',maxWidth:1200,maxHeight: 440, overflow: 'auto', mt: 5,mb:5 }}>
                  <Table size="small" aria-label="csv-demo-table" className={classes.table} stickyHeader>
                       <TableHead>
                        <TableRow>
                            {dataFile.col.map((key,index) => (
                                <TableCell key={`header-` + index} sx={{ color:'white',width:200,bgcolor:'green'}}>
                                    {key}
                                </TableCell>
                            ))}
                        </TableRow>
                        </TableHead>
                        <TableBody>
                            {dataFile.data.map((item, index) =>
                            <TableRow key={index}>
                                {item.map((val:any,index:number) => (
                                    <TableCell key={`row-`+index+`-c`+index}>{val}</TableCell>
                                ))}
                            </TableRow>
                        )}
                      </TableBody>
                   </Table>
                </TableContainer>
                <Divider />
                    <LoadingButton
                        onClick={handleSubmitAsync}
                         loading={submitting}
                        startIcon={<SendIcon />}
                        variant="contained"
                        color="success"
                        sx={{ mt: 5, ml: "70vw" }}
                    >
                        submit
                </LoadingButton>
            </>
          ) : null}
     </Box>
  )
}

export default WareImportFile