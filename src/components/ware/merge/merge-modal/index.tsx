import CloseIcon from '@mui/icons-material/Close';
import { Button, IconButton, Paper, styled, useMediaQuery, useTheme } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import Grow from '@mui/material/Grow';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import { TransitionProps } from '@mui/material/transitions';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';


import moment from 'moment';
import { SelectMergeType } from '..';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
        backgroundColor: '#BA4A00',
        fontSize: 16,
       fontWeight:'bold',
        color: "white"
  },
  [`&.${tableCellClasses.body}`]: {
      fontSize: 16,
    
  },
}));

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Grow  ref={ref} {...props} />;
});

interface CombineData {
     expect_remain: number;
    flag_number: string;
    location: string;
    product_name: string;
    remain: number;
    update_time: string;
    in: boolean;
    out:boolean
};

const bgColor = "#34495E";

function MergeModal(

    props: {
        data: SelectMergeType;
        open: boolean,
        handleClose: () => void,
        handleFinish: () => void,
    }
) {
    const { data,open, handleClose,handleFinish } = props;
    const { t } = useTranslation();
    const { auth } = useContext(AuthContext);
    const { employee_id } = auth;
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const [dataset, setDataset] = useState<CombineData[]>([]);
    
    //function--------------------------------------------------------
    /**選擇入庫 */
    const onChangeIn = (event: React.ChangeEvent<HTMLInputElement>) => {
        const targetIndex = dataset.findIndex(ele => ele.flag_number === event.target.name);
        const update = [...dataset];
        if (event.target.checked) {
            update.splice(targetIndex, 1, { ...dataset[targetIndex], in: event.target.checked,out:false });
        } else {
            update.splice(targetIndex, 1, { ...dataset[targetIndex], in: event.target.checked });
        };
        setDataset(update);
    };
     /**選擇出庫 */
    const onChangeOut = (event: React.ChangeEvent<HTMLInputElement>) => {
        const targetIndex = dataset.findIndex(ele => ele.flag_number === event.target.name);
        const update = [...dataset];
        if (event.target.checked) {
            update.splice(targetIndex, 1, { ...dataset[targetIndex], out: event.target.checked,in:false });
        } else {
            update.splice(targetIndex, 1, { ...dataset[targetIndex], out: event.target.checked });
        };
        setDataset(update);
    };

    useEffect(() => { 
        console.log(dataset)
    }, [dataset]);

    const getBodyItem = (data: CombineData[]) => { 
        console.log(data)
        const stockin = data.filter(data => data.in);
        const stockout = data.filter(data => data.out);
        const { remain,product_name:out_name ,location:out_location} = stockout[0];
        const { product_name: in_name, location: in_location } = stockin[0];

        const [out_p, out_a,out_remain] = out_location.split("-");
        const [in_p, in_a,in_remain] = in_location.split("-");
        
        return [
            {
                "item_number": 0,
                "product_name": in_name,
                "quantity": remain,
                "position": in_p,
                "area": in_a,
                "action": "in"
            }, {
                "item_number": 1,
                "product_name": out_name,
                "quantity": remain,
                "position": out_p,
                "area": out_a,
                "action": "out"
            }
        ];        
    };
    const handleSure = () => { 
        const stockin = dataset.filter(data => data.in);
        const stockout = dataset.filter(data => data.out);
        if (stockin.length === 1 && stockout.length === 1) { 
            handleSubmitMerge();
            return;
        };
        if (stockin.length === 0 || stockout.length === 0) {
            enqueueSnackbar("請選擇出庫和入庫。", { variant: 'warning' });
            return;
        }
        enqueueSnackbar("不能兩個同時出入庫。", { variant: 'warning' });
        return;
    };
    //api-------------------------------------------------------------
    const handleSubmitMerge = async () => {
        const body = {
            "order_id": "",
            "date": moment(Date.now()).format("YYYY-MM-DD"),
            "employee_id": employee_id,
            "machinecode": "",
            "orderitems": getBodyItem(dataset)
        };
        try {
            const res = await api.post("/api/management/orders/pending/merge", body);
            if (res.data) {
                enqueueSnackbar("合併成功", { variant: 'success' });
                handleFinish()
                return;
            };
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("合併失敗", { variant: 'error' });
        };
    };
    //useEffect-------------------------------------------------------
    useEffect(() => {
        const { merge1, merge2 } = data;
        if (merge1 && merge2) { 
            const merge1Data = { ...merge1, in: false, out: false };
            const merge2Data = { ...merge2, in: false, out: false };
            setDataset([merge1Data, merge2Data]);
        };
    }, [open]);
    
    return (
        <Dialog
            open={open}
            maxWidth={"md"}
            fullScreen={fullScreen}
            fullWidth={true}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
        >
            <DialogTitle sx={{ color:'white',bgcolor:bgColor,display: 'flex', justifyContent: 'space-between', fontSize: 25, fontWeight: 'bold' }}>
                {t("warehouse.manage.combineChoice")}
                <IconButton onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent
                sx={{ bgcolor: "#F9E79F"}}
            >
                 <TableContainer component={Paper} sx={{ m:'auto',mt:5 }}>
                    <Table aria-label="spanning table" stickyHeader size={fullScreen?"small":"medium"}  sx={{ minWidth: 700 }}>
                        <TableHead>
                            <TableRow>
                                <StyledTableCell>{t("warehouse.manage.rack")}</StyledTableCell>
                                <StyledTableCell >{t("warehouse.manage.enterDate")}</StyledTableCell>
                                <StyledTableCell>{t("warehouse.manage.card")}</StyledTableCell>
                                <StyledTableCell>{t("warehouse.manage.product")}</StyledTableCell>
                                {/* <StyledTableCell>{t("warehouse.manage.contain")}</StyledTableCell> */}
                                <StyledTableCell>{t("warehouse.manage.remain")}</StyledTableCell>
                              
                                <StyledTableCell>{t("warehouse.manage.in")}</StyledTableCell>
                                <StyledTableCell>{t("warehouse.manage.out")}</StyledTableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {dataset.map(ele =>
                                <TableRow key={ele.flag_number}>
                                    <StyledTableCell>{ ele.location}</StyledTableCell>
                                    <StyledTableCell >{ moment(ele.update_time).format("YYYY-MM-DD")}</StyledTableCell>
                                    <StyledTableCell>{ ele.flag_number}</StyledTableCell>
                                    <StyledTableCell>{ele.product_name}</StyledTableCell>
                                    {/* <StyledTableCell>{ele.expect_remain}</StyledTableCell> */}
                                    <StyledTableCell>{ele.remain}</StyledTableCell>
                                    <StyledTableCell>
                                        <Checkbox name={ele.flag_number} checked={ele.in} onChange={onChangeIn} />
                                    </StyledTableCell>
                                    <StyledTableCell>
                                        <Checkbox name={ele.flag_number} checked={ele.out} onChange={onChangeOut} />
                                    </StyledTableCell>
                                </TableRow>
                            )}
                            
                        </TableBody>
                    </Table>
                </TableContainer>
    
            </DialogContent>
            <DialogActions sx={{ bgcolor:bgColor }}>
                <Button color="success" variant="contained" sx={{ m: 'auto' }} onClick={handleSure}>{t("warehouse.sure")}</Button>
            </DialogActions>
        </Dialog>
    );
};

export default MergeModal;