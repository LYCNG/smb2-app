import SearchIcon from '@mui/icons-material/Search';
import { Box, Button, Divider, IconButton, Stack, TextField, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import Checkbox from '@mui/material/Checkbox';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
import { CheckListType } from '../../check-data-modal';
import OnPendingTable from '../onPending-table';
import MergeModal from './merge-modal';

interface OnMergeType {
    row_number: number,
    date: string,
    type: string,
    order_id: string,
    employee_id: string,
    machinecode: string,
    id: number|string
};
export interface SearchDatasetType { 
    id: string | number;
    expect_remain: number;
    flag_number: string;
    location: string;
    product_name: string;
    remain: number;
    update_time: string;
};

export interface SelectMergeType { 
    "merge1": null | SearchDatasetType;
    "merge2": null | SearchDatasetType;
};

function WareMerge() {
     const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();

    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('md'));

    //state-----------------------------------------
    const [showModal, setShowModal] = useState<boolean>(false);
    const [searchValue, setSearchValue] = useState<string>("");
    const [selectData, setSelectData] = useState<SelectMergeType>({"merge1":null,"merge2":null});//選擇的資料，不可多於2
    const [searchDataset, setSearchDataset] = useState<SearchDatasetType[]>([]);
    const [onMergeData, setOnMergeData] = useState<OnMergeType[]>([]);
    //查看列表設定
    const [checkData, setCheckData]
        = useState<CheckListType | null>(null);
    //function---------------------------------------
    /**處理選擇合併對象的checkbox */
    const handleSelectMerge = (event: React.ChangeEvent<HTMLInputElement>) => { 
        const flag = event.target.name;
        const {merge1,merge2 } = selectData;
        const flagData = searchDataset.filter(data => data.flag_number === flag)[0];

        if (flagData.flag_number === merge1?.flag_number) {
            setSelectData({ ...selectData, merge1: null });
            return
        };
        if (flagData.flag_number === merge2?.flag_number) {
            setSelectData({ ...selectData, merge2: null });
            return
        };
        if (selectData.merge1) {
            setSelectData({ ...selectData, merge2: flagData });
            return;
        };
        setSelectData({ ...selectData, merge1: flagData });
        return;
    };
    /**搜尋*/
    const onChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = event.target.value;
        setSearchValue(value);
    };
    /**Enter 執行搜尋 */
     const handleKeyPress = (event: React.KeyboardEvent) => { 
        if (event.key === "Enter") {
            handleSearch();
        };
    };
    const handleMergeData = () => { 
        const { merge1, merge2 } = selectData;
        if (!merge1 || !merge2) {
            return enqueueSnackbar("需有選擇兩筆。",{variant:"warning"});
        };
        if (merge1.product_name!==merge2.product_name) {
            return enqueueSnackbar("品名一樣才能進行合併。",{variant:"warning"});
        }
        setShowModal(true);
        return;
    };
    /**確認使否刪除 */
    const handleCheckDelete = (orderId: string) => { 
        const sure = window.confirm(`你確定要刪除 ${orderId} 嗎?`)
        if (sure) {
            handleDelete(orderId);
            return 
        }
    };
    //api-----------------------------------------------
    /**刪除指定資料 */
    const handleDelete = async (order_id:string) => {
        try {
            await api.delete(`/api/management/pending/${order_id}`);
            enqueueSnackbar("刪除成功", { variant: "success" });
            getMergeData();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("刪除失敗", { variant: "error" });
        };
    };
    /**開始搜尋 */
    const handleSearch = async () => {
        try {
            const res = await api.post("/api/storage/product/keyword", { "keyword": searchValue });
            const dataset: SearchDatasetType[]
                = res.data.map((data: any, index: number) => {
                    return { ...data, id: index };
                });
            setSearchDataset(dataset);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("查詢失敗", { variant: 'error' });
        }
    };
    /** 取德合併列表*/
    const getMergeData = async () => { 
        try {
            const res = await api.get('/api/management/orders/pending/in');
            const merges = res.data.filter((data: any) => data.type === "merge");
            const dataset = merges.map((merge: any, index: number) => {
                return {
                    ...merge,
                    id:index
                };
            })
            setOnMergeData( dataset);
        } catch (err: any) {
            console.log(err.message);
        };
    };

   
    const handleFinish = () => { 
        handleSearch();
        getMergeData();
    };

    //useEffect------------------------------------------------
    useEffect(() => { 
        setSelectData({
            "merge1": null, "merge2": null
        });
    }, [searchDataset]);

    useEffect(() => { 
        handleSearch();
        getMergeData();
    }, []);

    return (
        <Box sx={{ bgcolor: '#FBFCFC', width: '100%', minHeight: 1000, mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <MergeModal
                open={showModal}
                data={selectData}
                handleClose={() => setShowModal(false)}
                handleFinish={handleFinish}
            />
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant={"h4"} sx={{fontWeight:'bold',fontSize:"3.5vh"}} align="center">
                    {t("warehouse.manage.title")} : {t("warehouse.manage.combineSelect")}
                </Typography>
            </Toolbar>
            <Box sx={{m: 'auto',width: { xs: "100%", sm: "100%", md: "90%" }}}>
                <Stack direction="row" justifyContent={"space-between"} alignItems="center" sx={{ mt: 3, maxWidth: 800 }}>
                    <Box sx={{display:'flex',alignItems:'center'}}>
                        <h2>{t("warehouse.manage.product")} : </h2>
                        <TextField
                            label={t("warehouse.keyword")}
                            value={searchValue}
                            size="small" sx={{ bgcolor: 'white',width:{xs:120,sm:150,md:180},ml:1 }}
                            onChange={onChange}
                            onKeyDown={handleKeyPress}
                        />
                        <IconButton onClick={handleSearch}>
                            <SearchIcon fontSize="large" />
                        </IconButton>
                    </Box>
                    <Button
                        variant="outlined" color="success"
                        sx={{ m: 'auto', mt: 2 }}
                        onClick={handleMergeData}
                        size="large">
                        {t("warehouse.manage.combine")}
                    </Button>
                </Stack>
                <Box
                    sx={{
                        width:{ xs: "100%", sm: "100%", md: "75%" },
                        pt: 2, height:400,
                        '& .MuiDataGrid-columnHeaders': { bgcolor:"#2980B9", color: 'white' }}}
                >
                    <DataGrid
                        sx={{
                        fontSize: 16,bgcolor:'white',
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                            }}
                        density={'compact'}
                        rows={searchDataset}
                        columns={SearchDataColumns(t,selectData,handleSelectMerge)}
                        showCellRightBorder
                        pageSize={15}
                        rowsPerPageOptions={[5]}
                        experimentalFeatures={{ newEditingApi: true }}
                    />
                </Box>
            </Box>
            <Divider sx={{ mt: 5 }} />
            <OnPendingTable
                sx={{
                    width:{ xs: "100%", sm: "100%", md: "90%" },
                    pt: 2, height:250,
                    '& .MuiDataGrid-columnHeaders': { bgcolor: "#1A5276", color: 'white' }
                }}
                title={'合併單據'}
                rows={onMergeData}
                reload={getMergeData}
                type={'merge'}
            />
   
        </Box>
    );
};

export default WareMerge;


const SearchDataColumns = (
    t: TFunction,
    onSelect: SelectMergeType,
    handleSelectMerge:(event: React.ChangeEvent<HTMLInputElement>)=>void,
): GridColDef[] => [
        { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
        {
            field: 'checked', headerName: t("warehouse.manage.choice"), width: 80, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { flag_number } = params.row;
                const { merge1,merge2} = onSelect;
                const checked = flag_number === merge1?.flag_number || flag_number === merge2?.flag_number 
                return (
                    <Checkbox checked={checked} size="small" name={flag_number} onChange={handleSelectMerge} />
                );
            }
        },
        { field: 'location', headerName: t("warehouse.manage.rack"), width: 120, align: 'center', headerAlign: 'center' },
        {
            field: 'update_time', headerName: t("warehouse.manage.date"), width: 180, align: 'center', headerAlign: 'center',
            valueGetter:(params:GridValueGetterParams)=>`${ moment(params.row.update_time).format("YYYY-MM-DD")}`
        },
        { field: 'flag_number', headerName: t("warehouse.manage.card"), width: 180, align: 'center', headerAlign: 'center' },
        { field: 'product_name', headerName: t("warehouse.manage.product"), width: 180, align: 'center', headerAlign: 'center'},
        { field: 'remain', headerName: t("warehouse.manage.remain"), width: 120, align: 'center', headerAlign: 'center'},
             
    ];

