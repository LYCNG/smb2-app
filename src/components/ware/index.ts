import { LazyLoad } from "../../lazy-load";


export const StockoutPage = LazyLoad(import("./stockout"));
export const PurchasePage = LazyLoad(import("./purchase")); //lazy(async () => import("./purchase/index").then(module => ({ default: module.default })));
export const StockinPage = LazyLoad(import("./stockin"));
export const OrderPage = LazyLoad(import("./order/index"));
export const ImportFilePage = LazyLoad(import("./importfile"));
export const MergePage = LazyLoad(import("./merge"));
export const ChangePage = LazyLoad(import("./change"));
export const InventoryPage = LazyLoad(import("./inventory"));
export const StorageOverview = LazyLoad(import("./storage-overview"));