import CloseIcon from '@mui/icons-material/Close';
import CompareArrowsRoundedIcon from '@mui/icons-material/CompareArrowsRounded';
import DoneRoundedIcon from '@mui/icons-material/DoneRounded';
import ErrorOutlineIcon from '@mui/icons-material/ErrorOutline';
import QrCodeScannerOutlinedIcon from '@mui/icons-material/QrCodeScannerOutlined';
import {
    Box, Button, Checkbox, Dialog, DialogActions,
    DialogContent, DialogTitle,
    Divider, FormControlLabel, Grow, IconButton,
    List,
    ListItem,
    ListItemIcon,
    Stack, styled, TextField,
    Typography, useMediaQuery, useTheme
} from '@mui/material';
import ListItemText from '@mui/material/ListItemText';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import { TransitionProps } from '@mui/material/transitions';
import { useSnackbar } from 'notistack';
import React, { ReactNode, useContext, useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { InventoryDataType } from '..';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';
import BarcodeCamera from '../../../barcode-scanner';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor:  '#74B9FF',
    color: "black"
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 16,
  },
}));


const bgColor = "#2E4053 ";

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Grow  ref={ref} {...props} />;
});

interface CheckInventoryType { 
    check_time: string;
    flag_number: string;
    location: string;
    product_name: string;
    remain: number;
    update_time: string;

    check_location: boolean;
    check_flag: boolean;
    check_remain: boolean;
};

const ListTextStyle = (props: {
    children: ReactNode
}) =>
    <Typography
        sx={{ display: 'inline', fontWeight: 'bold', fontSize: 18 }}
        component="span"
        variant="body2"
        color="text.primary"
    >
        {props.children}
    </Typography>;

type CheckType = 'flag' | 'location';

function InventorModal(props: {
    data: InventoryDataType,
    open: boolean,
    handleClose: () => void,
    handleFinish: (id:number|string) => void
}) {

    const { t } = useTranslation();
    const {auth} =  useContext(AuthContext);
    const { employee_id } = auth;
    const { enqueueSnackbar} = useSnackbar();
    const { data,open, handleClose, handleFinish } = props;
     const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    //state-----------------------------------------------------------
    const [showCamera, setShowCamera] = useState<boolean>(false);
    const [targetType, setTargetType] = useState<CheckType | null>(null);//辨識列別
    const [checkData, setCheckData] = useState<CheckInventoryType>();
    const [inputValue, setInputValue] = useState<number>(0);//現場數量資料
    const [isEmpty, setIsEmpty] = useState<boolean>(false);
    const [matchValue, setMatchValue] = useState<boolean>(false);

    //function--------------------------------------------------------
    /**處理送出前檢查比對是否有未完成 */
     const handleFinishBeforeCheck =  () => {
        const { check_flag, check_location, check_remain } = checkData!;
        if (!check_flag || !check_location || !check_remain) {
            return enqueueSnackbar("尚未比對完成", { variant: "warning" });
        };
        return handleSubmitCheckFinish();
    };
    /**檢查數量是否比對正確 */
    const handleChangeRemain = (event: React.ChangeEvent<HTMLInputElement>) => { 
        const value = parseInt(event.target.value);
        setInputValue(value);
        // if (value === checkData?.remain &&checkData) {
        //     setCheckData({
        //         ...checkData,
        //         check_remain: true
        //     });
        //     return;
        // };
        // if (value !== checkData?.remain &&checkData) { 
        //     setCheckData({
        //         ...checkData,
        //         check_remain: false
        //     });
        //     return;
        // };
    };
     /**設定類別及顯示辨別條碼 */
    const handleMatchCode = (target: CheckType) => {
        setTargetType(target);
        setShowCamera(true);
        return;
    };
    /**關閉條碼辨識 */
     const handleCloseCamera = () => {
        setTargetType(null);
         setShowCamera(false);
         return;
    };
    /**處理辨識回傳結果*/
    const onResult = (text: string) => { 
        if (checkData) {
            if (targetType === 'location') {
                if (checkData.location === text) {
                    setCheckData({ ...checkData, check_location: true });
                    handleCloseCamera();
                    return 
                };
            };
            if (targetType === 'flag') {
                if (checkData.flag_number === text) {
                    setCheckData({ ...checkData, check_flag: true });
                    handleCloseCamera();
                    return;
                    
                };
            };
        }
    };
    const handleThisEmpty = (event: React.ChangeEvent<HTMLInputElement>) => { 
        let checked = event.target.checked
        setIsEmpty(checked);
        if (checkData) { 
            setCheckData({
                ...checkData, 
                check_flag: checked,
                check_remain: checked
            });
        };
    };

    const handleCheckRemain = () => { 
        let value = inputValue;
        let remain = checkData?.remain;
        if (value !== remain) {
            const replaced = window.confirm("現場數量與系統數量不同，是否要更新系統數量?");
            if (replaced) {
                updateRemain(value);
                return;
            };
        };
        if (value === remain) {
            setCheckData({ ...checkData!, check_remain: true });
            return;
        }

    };
    //api---------------------------------------------------------------
    /**送出盤點完成 */
    const handleSubmitCheckFinish = async () => {
        try {
            const body = {
                location: checkData?.location,
                checker: employee_id
            };
            await api.put("api/storage/inventory", body);
            enqueueSnackbar("更新成功。", { variant: "success" });
            handleFinish(data.id);
            return;
        } catch (err: any) {
            console.log(err.message);
            return enqueueSnackbar("更新失敗", { variant: "warning" });
        };
    };

    const updateRemain = async (value:number) => {
        try { 
            const body = {
                "location":data.location,
                "flag_number":data.flag_number,
                "remain":value
            };
            await api.put("/api/storage/inventory/remain", body);
            enqueueSnackbar("更新成功。", { variant: "success" });
            setCheckData({ ...checkData!, remain: value });
        } catch (err: any) { 
            console.log(err.message);
            enqueueSnackbar("更新失敗。"+err.message, { variant: "error" });
        };
    };
    //useEffect---------------------------------------------------------
    useEffect(() => { 
        console.log(data)
        setCheckData({
            ...data,
            check_location: false,
            check_flag: false,
            check_remain: false
        });
        setIsEmpty(false);
    }, [data]);

    return (
        <Dialog
            open={open}
            maxWidth={"sm"}
            fullScreen={fullScreen}
            fullWidth={true}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleClose}
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle sx={{color:'white',display:'flex',justifyContent:'space-between',bgcolor:bgColor,fontSize:25,fontWeight:'bold'}}>
                {"儲位盤點"}
                <IconButton onClick={handleClose}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent
                sx={{
                    bgcolor:  "#C0392B", display: 'flex', flexDirection: 'column', justifyContent: 'center',
                    alignItems: 'flex-start'
                }}>
                <BarcodeCamera
                    showCamera={showCamera}
                    onResult={onResult}
                    handleClose={handleCloseCamera}
                />
                <Box >
                    <h2>1. {t("warehouse.manage.rackCompare")}</h2>
                    <Stack direction="row" justifyContent={"space-between"} sx={{width:{xs:450,sm:500}}}>
                         <ListItem
                        sx={{width:300,bgcolor:'white',borderRadius:2,fontWeight:'bold'}}
                        secondaryAction={
                            <IconButton edge="end" id="location-check-button" onClick={()=>handleMatchCode("location")}>
                                <QrCodeScannerOutlinedIcon color="primary" fontSize='large'/>
                            </IconButton>
                        }
                    >
                        <ListItemIcon>
                            {checkData?.check_location ?
                                    <DoneRoundedIcon fontSize="large" color="success"/> :
                                <ErrorOutlineIcon color="warning" fontSize="large"  /> 
                            }
                        </ListItemIcon>
                        <ListItemText
                            secondary={
                                <ListTextStyle>
                                     {`${t("warehouse.manage.rack")}: ${checkData?.location}`}
                                </ListTextStyle>
                            }
                        />
                        </ListItem>
                        <FormControlLabel control={<Checkbox checked={isEmpty} onChange={handleThisEmpty}/>} label="此為空儲位" />
                    </Stack>
                   
                </Box>
                <Box>
                    <h2>2.{t("warehouse.manage.materialCardCompared")}</h2>
                    <ListItem
                        sx={{width:430,bgcolor:'white',borderRadius:2,fontWeight:'bold',height:120}}
                        secondaryAction={
                            <IconButton edge="end" id="card-number-check-button" onClick={()=>handleMatchCode("flag")}>
                                <QrCodeScannerOutlinedIcon color="primary" fontSize='large'/>
                            </IconButton>
                        }
                    >
                        <ListItemIcon>
                            {checkData?.check_flag?
                                <DoneRoundedIcon fontSize="large" color="success"/> :
                                <ErrorOutlineIcon color="warning" fontSize="large"  /> 
                            }
                        </ListItemIcon>
                        <ListItemText
                            // primary={`${t("warehouse.manage.rack")}: ${checkData?.location}`}
                            secondary={
                                <ListTextStyle>
                                    <Box sx={{display:'flex',alignItems:'center'}}>
                                        <p style={{width:80}}>{t("warehouse.manage.card")}:</p>
                                        <p style={{fontSize:16}}>{checkData?.flag_number}</p>
                                    </Box>
                                    <Divider sx={{width:300}} />
                                    <Box sx={{display:'flex',alignItems:'center'}}>
                                        <p style={{width:80}}>{t("warehouse.manage.product")}:</p>
                                        <p style={{fontSize:16}}>{checkData?.product_name}</p>
                                    </Box>
                               </ListTextStyle>
                            }
                        />
                    </ListItem>
                </Box>
                <Box >
                    <h2>3.{t("warehouse.manage.remainCompared")}</h2>
                    <Stack
                        direction={"row"}
                        justifyContent="space-between"
                        alignItems={"center"}
                        sx={{ bgcolor: 'white',p:2 ,borderRadius: 2,  width: {xs:400,sm:450,md:500}}}>
                         {checkData?.check_remain?
                                <DoneRoundedIcon fontSize="large" color="success"/> :
                                <ErrorOutlineIcon color="warning" fontSize="large"  /> 
                            }
                        <List>
                            <ListItem sx={{width:100}} disablePadding>
                                <ListTextStyle>
                                    現場數量
                                </ListTextStyle>
                            </ListItem>
                            <ListItem sx={{width:100}} disablePadding>
                                <TextField
                                    value={inputValue}
                                    size="small" type="number"
                                    InputProps={{ inputProps: { min: 0, max: 100000 } }}
                                    onChange={handleChangeRemain}
                                    inputProps={{ style: { textAlign: 'center' } }}
                                />
                            </ListItem>
                        </List>
                        <Divider orientation="vertical" flexItem />
                        <IconButton onClick={()=>handleCheckRemain()}>
                            <CompareArrowsRoundedIcon fontSize='large' color="secondary" />
                        </IconButton>
                      
                            <Divider orientation="vertical" flexItem/>
                         <List sx={{width:150,fontWeight:'bold'}}>
                            <ListItem sx={{width:100}} disablePadding>
                                <ListTextStyle>
                                    系統數量
                                </ListTextStyle>
                            </ListItem>
                            <ListItem sx={{ width: 100}} disablePadding>
                                <ListTextStyle >
                                    <p style={{margin:'auto',width:60,textAlign:'center'}}>
                                        {/* {false && checkData?.remain} */}
                                    </p>
                                </ListTextStyle>
                            </ListItem>
                        </List>

                    </Stack>
                </Box>
            </DialogContent>
            <DialogActions sx={{ bgcolor:bgColor}}>
                <Button
                    variant="contained"
                    sx={{ m: 'auto' }}
                    color="success"
                    onClick={handleFinishBeforeCheck}>
                    {t("warehouse.sure")}
                </Button>
            </DialogActions>

        </Dialog>
    );
};

export default InventorModal;