import CheckCircleOutlinedIcon from '@mui/icons-material/CheckCircleOutlined';
import CloseIcon from '@mui/icons-material/Close';
import CloseOutlinedIcon from '@mui/icons-material/CloseOutlined';
import CompareArrowsRoundedIcon from '@mui/icons-material/CompareArrowsRounded';
import DeveloperModeIcon from '@mui/icons-material/DeveloperMode';
import DoneOutlinedIcon from '@mui/icons-material/DoneOutlined';
import DoneOutlineOutlinedIcon from '@mui/icons-material/DoneOutlineOutlined';
import DriveFileRenameOutlineOutlinedIcon from '@mui/icons-material/DriveFileRenameOutlineOutlined';
import RedoOutlinedIcon from '@mui/icons-material/RedoOutlined';
import { Autocomplete, Box, Checkbox, Divider, FormControlLabel, IconButton, Stack, TextField, Typography, useMediaQuery, useTheme } from '@mui/material';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { useSnackbar } from 'notistack';
import { useContext, useRef, useState } from 'react';
import { useTranslation } from 'react-i18next';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';
import BarcodeCamera from '../../../barcode-scanner';

interface ProductType{
    product_name: string;
    product_number: string;
};

interface CheckInventoryType { 
    check_time: string;
    flag_number: string;
    location: string;
    product_name: string;
    status: string;
    remain: number;
    update_time: string;
    check_flag: boolean;
    check_remain: boolean;
};

function ScanningLocationModal(
    props: {
        open: boolean;
        handleClose: () => void;

    }
) {
    const { open, handleClose } = props;
    const { enqueueSnackbar } = useSnackbar();
    const {auth} =  useContext(AuthContext);
    const { employee_id } = auth;
    const { t } = useTranslation();
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));

    const [showCamera, setShowCamera] = useState<boolean>(false);
    const [location, setLocation] = useState<string>('');
    const [matchType, setMatchType] = useState<string | null>(null);
    const [checkData, setCheckData] = useState<CheckInventoryType|null>(null);
    const [onEditName, setOnEditName] = useState<boolean>(false);
    const [products, setProducts] = useState<ProductType[]>([]);
    const [selected, setSelected] = useState<string>('');
    const [isEmpty, setIsEmpty] = useState<boolean>(false);
    const [choice, setChoice] = useState<boolean>(false);

    const remainRef = useRef<HTMLInputElement | null>(null);


    const handleCancelMatch = () => { 
        setCheckData({ ...checkData!, check_flag: false, check_remain: false });
        setIsEmpty(false);
    };

    const handleSelect = (
        value:ProductType|null,
    ) => { 
        setSelected(value ? value.product_name : selected);
    };

    const handleFinishEdit = () => { 
        const value = selected;
        if (!value.trim()) return setOnEditName(false);
        setCheckData({
            ...checkData!,
            product_name: value
        });
        return;
    };

    const handleMatchRemain = () => {
        const refValue = remainRef.current?.value;
        if (refValue && checkData) { 
            const value = parseInt(refValue);
            if (typeof value !== 'number') {
                enqueueSnackbar('輸入錯誤', { variant: 'error' });
                return;
            }
            if (value !== checkData?.remain) {
                const checked = window.confirm("剩餘數量不同，是否要更新?");
                if (checked) {
                    const newData = {
                        ...checkData,
                        remain: value,
                        check_remain: true
                    };
                    updateStorage(newData);
                };
            } else {
                setCheckData({
                    ...checkData,
                    check_remain:true
                })
            }
            return;
        };
        enqueueSnackbar('輸入錯誤', { variant: 'warning' });
    };
    
    const getResultType = (text: string) => { 
        if (matchType === 'flag'&&checkData) {
            if (text !== checkData.flag_number) { 
                const checked = window.confirm("物料卡不同，是否要更新?");
                if (checked) {
                    const newData = {
                        ...checkData,
                        flag_number: text,
                        check_flag: true
                    };
                    updateStorage(newData);
                    setCheckData(newData);  
                };
                return;
            };
            setCheckData({ ...checkData, check_flag: true });
            return;
         };
        if (matchType === 'location') { 
            setLocation(text);
            return;
        };
    };

    const handleThisEmpty = (event: React.ChangeEvent<HTMLInputElement>) => { 
        let checked = event.target.checked;
        setIsEmpty(checked);
        if (checkData) { 
            setCheckData({
                ...checkData, 
                check_flag: checked,
                check_remain: checked
            });
        };
    };

    /**
     * * 更新架位內容
     * @param body <CheckInventoryType>
     */
    const updateStorage = async (body:CheckInventoryType) => { 
        try { 
            await api.put('api/storage/inventory/location', {
                product_name: body.product_name,
                flag_number: body.flag_number,
                remain: body.remain,
                location: body.location,
                status:body.status
            });
            setCheckData({ ...checkData!, check_remain: true });
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar('更新失敗', { variant: 'error' });
        };
    };

    /**
     * * 取得價位資訊
     * @returns 
     */
    const getStorageByLocation = async () => { 
        if (!location) {
            return enqueueSnackbar('請輸入架位碼', { variant: 'error' });
        };
        try {
            const res = await api.get('api/storage/inventory/get/' + location);
            const data = await res.data[0];
            if (!data.flag_number && data.remain === 0 && !data.product_name) {
                setIsEmpty(true);
                setCheckData({
                    ...data,
                    check_flag: true,
                    check_remain: true,
                });
            }else{
                 setCheckData({
                    ...data,
                    check_flag: false,
                    check_remain: false,
                });  
            };
           
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar(`無法找到該架位:${location} 資訊。`, { variant: 'error' });
        };
    };
    const handleEditName = async () => {
        try {
            const product_res = await api.get("/api/product/infos");
            const productData = await product_res.data;
            const productName = Object.keys(productData);
            const result = productName.map(ele => {
                return {
                    product_name: ele,
                    product_number: productData[ele]
                };
            });
            setProducts(result);
            setOnEditName(true)
        } catch (err: any) { 
            console.log(err.message);
         };
     };
    const handleMatch = (match: string) => {
        setMatchType(match);
        setShowCamera(true);
    };

    /**
     * * 盤點完成 
     * @returns 
     */
    const handleFinish = async () => { 
        if (checkData) {
            try {
                const { check_flag, check_remain } = checkData;
                if (!check_flag || !check_remain) {
                    return enqueueSnackbar(`尚未比對完成。`, { variant: 'error' });
                };
                const body = {
                    location: checkData?.location,
                    checker: employee_id
                };
                await api.put("api/storage/inventory", body);
                enqueueSnackbar("架位盤點成功。", { variant: "success" });
                setChoice(true)
            } catch (err: any) {
                console.log(err.message);
                enqueueSnackbar("更新失敗", { variant: "warning" });
            };    
        };
    };

    /**
     * * 變更為空儲位 
     * @returns 
     */
    const handleUpdateToEmpty = async () => { 
        const update = window.confirm('確定更改此儲位為空儲位?');
        if (!update) return;
        try {
            const { location } = checkData!;
            await api.put('api/storage/inventory/location/empty', { "location": location });
            enqueueSnackbar("更改為空儲位成功。", { variant: "success" });
            getStorageByLocation();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("更新失敗", { variant: "warning" });
        };
    };

    const ChoicePage = () => (
        <Stack alignItems={'center'} justifyContent='space-around' direction={'row'} sx={{width:400,m:'auto',mt:10}}>
            <Button
                onClick={() => {
                    setCheckData(null);
                    setChoice(false);
                }}
                autoFocus color="warning" variant='contained' size='large'
                startIcon={<RedoOutlinedIcon />}
            >
                比對下一個
            </Button>
            <Button
                onClick={() => {
                    handleClose();
                    setChoice(false);
                    handleClose();
                }}
                autoFocus color="success" variant='contained' size='large'
                 startIcon={<DoneOutlinedIcon />}
            >
                盤點完成
            </Button>
        </Stack>
    );

    return (
        <Dialog
            fullScreen={fullScreen}
            open={open}
            fullWidth={true}
            maxWidth={'sm'}
            onClose={handleClose}
            aria-labelledby="alert-dialog-title"
            aria-describedby="alert-dialog-description"
        >
            <DialogTitle id="alert-dialog-title" sx={{ bgcolor: '#C0392B', color: 'white', fontWeight: 'bold', justifyContent: 'space-between', display: 'flex' }}>
                {"盤點掃描"}
                <IconButton onClick={handleClose} >
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent sx={{ minHeight: 350 }}>
                <Box sx={{ mt: 5 }}>
                    {!choice && checkData ? (
                        <Stack alignItems={'center'} justifyContent='space-around' sx={{ color: 'black', height: 320 }}>
                            <Typography variant="h5">
                                架位: {location}
                                <FormControlLabel
                                    sx={{ pl: 5 }}
                                    control={
                                        <Checkbox
                                            checked={isEmpty} onChange={handleThisEmpty}
                                            disabled={Boolean(checkData.product_name && checkData.flag_number)} />
                                    }
                                    label="此為空儲位"
                                />
                            </Typography>
                            <Divider sx={{ width: 300 }} />
                            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                {checkData?.check_flag ? <CheckCircleOutlinedIcon color='success' /> : null}
                                <p>{t("warehouse.manage.card")}:</p>
                                <p style={{ fontSize: 16, paddingLeft: 10 }}>
                                    {checkData?.flag_number}
                                </p>
                                <IconButton color='primary' onClick={() => handleMatch('flag')}>
                                    <DeveloperModeIcon />
                                </IconButton>
                            </Box>
                            <Divider sx={{ width: 300 }} />
                            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                {onEditName ? (
                                    <>
                                        <Autocomplete
                                            size='small'
                                            disablePortal
                                            onChange={(e, value) => handleSelect(value)}
                                            id="combo-box-demo"
                                            options={products}
                                            getOptionLabel={(option) => option.product_name}
                                            sx={{ width: 300 }}
                                            renderInput={(params) => <TextField {...params} label="Product Name" />}
                                        />
                                        <IconButton color='success' onClick={() => handleFinishEdit()}>
                                            <DoneOutlineOutlinedIcon />
                                        </IconButton>
                                        <IconButton color='error' onClick={() => setOnEditName(false)}>
                                            <CloseOutlinedIcon />
                                        </IconButton>
                                    </>) :
                                    <>
                                        <p>{t("warehouse.manage.product")}:</p>
                                        <p style={{ fontSize: 16, paddingLeft: 10 }}>
                                            {checkData?.product_name}
                                        </p>
                                        <IconButton color='primary' onClick={() => handleEditName()}>
                                            <DriveFileRenameOutlineOutlinedIcon />
                                        </IconButton>
                                    </>}
                            </Box>
                            <Divider sx={{ width: 300 }} />
                            <Box sx={{ display: 'flex', alignItems: 'center' }}>
                                {checkData?.check_remain ? <CheckCircleOutlinedIcon color='success' /> : null}
                                <p>{t("warehouse.manage.remainCompared")}:</p>
                                <TextField size='small' sx={{ width: 150, ml: 2 }} type={'number'} inputRef={remainRef} />
                                <IconButton color='secondary' onClick={handleMatchRemain}>
                                    <CompareArrowsRoundedIcon />
                                </IconButton>
                            </Box>
                            <Divider sx={{ width: 300 }} />
                            <Box sx={{mt:2,width:400,display:'flex',justifyContent:'space-around'}}>
                                <Button onClick={handleUpdateToEmpty} variant='outlined' color='warning'>
                                    清空為空儲位
                                </Button>
                                <Button onClick={() => setCheckData(null)} variant='outlined' color='error'>
                                    取消
                                </Button>
                            </Box>
                        </Stack>
                    ) : (
                        choice ?
                            <ChoicePage /> :
                            <div>
                                <Stack sx={{ width: '60%', m: 'auto' }}>
                                    <TextField id='input-location' size='small' onClick={() => handleMatch('location')} value={location} />
                                    <Button onClick={() => getStorageByLocation()} color='warning'>
                                        盤點架位
                                    </Button>
                                </Stack>
                            </div>
                    )}
                </Box>
                <BarcodeCamera
                    showCamera={showCamera}
                    onResult={getResultType}
                    handleClose={() => setShowCamera(false)}
                />
            </DialogContent>
            <DialogActions sx={{ bgcolor: '#C0392B' }}>
                {!choice &&　checkData ?
                    <>
                        <Button onClick={handleCancelMatch} autoFocus color="warning" variant='contained'>
                            重新比對
                        </Button>
                        <Button onClick={handleFinish} autoFocus color="success" variant='contained'>
                            完成
                        </Button>
                    </>
                    : null
                }
            </DialogActions>
        </Dialog>
    );
};

export default ScanningLocationModal