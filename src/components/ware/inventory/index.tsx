import SearchIcon from '@mui/icons-material/Search';
import {
    Box, Button, ButtonGroup, Divider, FormControl, IconButton, Stack, TextField, Toolbar,
    Typography, useMediaQuery, useTheme
} from '@mui/material';
import { DataGrid, GridColDef } from '@mui/x-data-grid';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
import { AuthContext } from '../../../provider/authProvider';
import ScanningLocationModal from './get-location-modal';

const SearchForm = (props: {
    title: string,
    value: string,
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void,
    onSearch: () => void
}) => {
    const { t } = useTranslation();
    return (
        <FormControl sx={{ width: 400, display: 'flex', flexDirection: 'row', alignItems: 'center' }}>
            <h3 style={{ width: '25%', marginRight: 10 }}>{props.title}:</h3>
            <TextField
                label={t("warehouse.keyword")}
                value={props.value}
                size="small" sx={{ bgcolor: 'white', width: 180 }}
                onChange={props.onChange}
            />
            <IconButton onClick={props.onSearch}>
                <SearchIcon fontSize="large" />
            </IconButton>
        </FormControl>
    );
};
   
interface FinishDataType{
    id: number | string;
    location: string;
    check_time: string;
    update_time: string;
    flag_number: string;
    product_name: string;
    remain: number;
};

export interface InventoryDataType { 
    id: number | string;
    check_time: string;
    flag_number: string;
    location: string;
    product_name: string;
    remain: number;
    update_time: string;
};

/**
 * * convert date string to milliseconds
 * @param timeString string
 * @returns number 
 */
const formatTime = (timeString: string) => moment(timeString).valueOf();

function WareInventory() {
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const {auth} = useContext(AuthContext);
    const { username } = auth; 
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    const today = moment(Date.now()).format("YYYY-MM-DD");
    //state--------------------------------------------------------------------------------
    const [storages, setStorages] = useState<any[]>([]);
    const [show, setShow] = useState<boolean>(false);
    const [condition, setCondition] = useState<React.Key>(1);
    const [onCheckTime, setOnCheckTime] = useState<string>("");
    const [history, setHistory] = useState<any>([]);
    const [defaultHour, setDefaultHour] = useState<number>(4);


    //function----------------------------------------------------------------------------
    const matchColumnData = (data:any[]) => { 
        const dataset = data.map((data: any, index: number) => {
            return {
                ...data,
                id: index
            }
        });
        setStorages(dataset);
    };
    
    //api---------------------------------------------------------------------------------
    const getAllStorage = async () => { 
        try {
            setCondition(1);
            const res = await api.get('api/storage/all');
            matchColumnData(res.data);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar('伺服器錯誤', { variant: 'error' });
        }
    };
    const getUnCheck = async () => { 
        try { 
            setCondition(3);
            const res = await api.post("api/storage/inventory/get/unchecked",{});
            matchColumnData(res.data);
        } catch (err: any) { 
            console.log(err.message);
            enqueueSnackbar('伺服器錯誤', { variant: 'error' });
        };
    };
    const getChecked = async () => { 
        try {
            setCondition(2);
            const res = await api.post("api/storage/inventory/get/checked",{});
            matchColumnData(res.data);
        } catch (err: any) { 
            console.log(err.message);
            enqueueSnackbar('伺服器錯誤', { variant: 'error' });
         };
    };
    const goAutoCheck = async () => {
        try { 
            const unCheckRes = await api.post("api/storage/inventory/get/unchecked",{});
            const dataset = await unCheckRes.data;
            for (let data of dataset) {
                if (data.product_name||data.flag_number) {
                    enqueueSnackbar('尚有物料在儲位內，請檢查並盤點或清空該儲位。', { variant: 'warning' });
                    return;
                };
            };
            await api.put('api/storage/inventory/autocheck',{checker:username});
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar('', { variant: 'error' });
        };
    };
    const getHistory = async () => { 
        try {
             setCondition(4);
            const res = await api.get("api/storage/inventory/history");
            const data = res.data.map((h: any, index: number) => {
                return {
                    ...h,
                    id: index
                };
            });
            setHistory(data);
        } catch (err: any) {
            console.log(err.message);
        }
    };
    const getLatestInventoryTime = async () => { 
        try {
            const res = await api.get("api/storage/inventory/last");
            const { end_time, start_time } = await res.data;
            const current = Date.now();
            if (formatTime(start_time) < current && current < formatTime(end_time)) { 
                setOnCheckTime("盤點中: "+start_time + "~" + end_time);
            } else {
                setOnCheckTime('');
            };
        } catch (err: any) {
            console.log(err.message);
        };
    };

    const handleStartInventory = async () => {
        try {
            const current = Date.now();
            const body = {
                "checker":username,
                "start_time":moment(current).format('YYYY-MM-DD HH:mm:ss'),
                "end_time":moment(current).add(defaultHour,'hours').format('YYYY-MM-DD HH:mm:ss')
            };
            const res = await api.post("api/storage/inventory", body);
            const data = res.data;
            if (data.success) {
                enqueueSnackbar("盤點開始", { variant: 'success' })
                getLatestInventoryTime();
            } else {
                throw new Error(data.msg)
            };
        } catch (err: any) {
            console.log(err.message);
        };
    };
    const handleChangeHours = (
        event: React.ChangeEvent<HTMLTextAreaElement | HTMLInputElement>
    ) => {
        const value = parseInt(event.target.value);
        if (value <= 0) return setDefaultHour(1);
        if (value > 10) return setDefaultHour(10);
        return setDefaultHour(value);
    };
    //useEffect----------------------------------------------------------------------------
    useEffect(() => { 
        getAllStorage();
        getHistory();
        getLatestInventoryTime();
     }, []);
    
    return (
        <Box sx={{ bgcolor: '#FBFCFC', width: '100%', minHeight:'100vh',mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant={"h4"} sx={{fontWeight:'bold',fontSize:"3.5vh"}} align="center">
                    {t("warehouse.manage.title")} : {t("warehouse.manage.inventoryList")}
                </Typography>
                <p style={{ color: 'red' }}>
                    {onCheckTime ? onCheckTime : '非盤點時間'}
                </p>
            </Toolbar>
            <Divider />
            <Stack
                direction={fullScreen ?'column-reverse':"row"}
                justifyContent={"center"}
                alignItems={"center"}  
            >
                <Box
                    sx={{
                        height:500,width:{xs:'90%',sm:'90%',md:'40%'},m:'auto',mt:5,
                        '& .MuiDataGrid-columnHeaders': { bgcolor: "#C0392B", color: 'white' }
                    }}>
                    <Box sx={{ display:'flex',justifyContent: 'center' }}>
                        <ButtonGroup variant="outlined" >
                            <Button onClick={()=> getAllStorage()} color={condition===1?'success':'primary'}>所有儲位</Button>
                            <Button onClick={()=>getChecked()} color={condition===2?'success':'primary'}>已盤點儲位</Button>
                            <Button onClick={() => getUnCheck()} color={condition === 3 ? 'success' : 'primary'}>未盤點儲位</Button>
                            <Button onClick={()=>getHistory()} color={condition===4?'success':'primary'}>過去盤點</Button>
                        </ButtonGroup>
                    </Box>
                    
                    <DataGrid
                        sx={{ 
                            fontSize:18,
                            '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                outline: 'none',
                            },
                            maxHeight: { xs: 500, sm: 500, md: 600 },
                            mt:2
                        }}
                        hideFooter
                        density={'compact'}
                        rows={condition===4?history:storages}
                        columns={condition===4?HistoryColumn(t):StorageColumn(t)}
                        showCellRightBorder
                        // pageSize={10}
                        // rowsPerPageOptions={[5]}
                        experimentalFeatures={{ newEditingApi: true }}
                    />
                </Box>
                <Divider orientation={fullScreen?'horizontal':"vertical"} flexItem sx={{mt:5}} />
                <Box sx={{ width: { xs: '90%', sm: '90%', md: '50%' } }}>
                    <h2 style={{ textAlign: 'center', fontWeight: 'bold' }}>
                        當前盤點人: {username}
                    </h2>
                    <Stack
                        sx={{
                            m: 'auto', mt: 2,
                            width: { xs: '80%', sm: '80%', md: '50%' },
                            height: { xs: 200, sm: 200, md: 300 }
                        }}
                        justifyContent='space-around'
                        direction={'column'}
                    >
                        <Button color='success' variant='outlined' onClick={() => handleStartInventory()} disabled={Boolean(onCheckTime)}>
                            開始盤點
                        </Button>
                        <Box sx={{display:'flex',justifyContent:'space-around',alignItems:'center'}}>
                            <p>盤點時長(預設4):</p>
                            <TextField value={defaultHour} size="small" type="number" onChange={handleChangeHours} sx={{width:150}} />
                        </Box>
                        <Button
                            disabled={!Boolean(onCheckTime)}
                            onClick={()=>setShow(true)}
                            color='warning' variant='outlined'
                        >
                            架位盤點
                        </Button>
                        <Button   disabled={!Boolean(onCheckTime)} color='warning' variant='outlined' onClick={goAutoCheck}>
                            自動盤點空儲位
                        </Button>
                    </Stack>
                </Box>
            </Stack>
            
            <ScanningLocationModal open={show} handleClose={()=>setShow(false)} />
        </Box>
    );
};

export default WareInventory;


const StorageColumn = (
    t: TFunction
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
    { field: 'location', headerName: t("warehouse.manage.rack"), width: 120, align: 'center', headerAlign: 'center',  },
    { field: 'product_name', headerName: t("warehouse.manage.product"), width: 200, align: 'center', headerAlign: 'center',  },
    { field: 'flag_number', headerName: t("warehouse.manage.card"), width: 150, align: 'center', headerAlign: 'center',  },
    { field: 'remain', headerName: t("warehouse.manage.remain"), width: 150, align: 'center', headerAlign: 'center', },  
    ];

const HistoryColumn = (
    t: TFunction
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
    { field: 'checker', headerName: "盤點人", width: 150, align: 'center', headerAlign: 'center',  },
    { field: 'start_time', headerName: "開始時間", width: 240, align: 'center', headerAlign: 'center',  },
    { field: 'end_time', headerName: "結束時間", width: 240, align: 'center', headerAlign: 'center',  },
    // { field: 'insert_time', headerName: t("warehouse.manage.remain"), width: 150, align: 'center', headerAlign: 'center', },  
];
