import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import CheckCircleOutlinedIcon from '@mui/icons-material/CheckCircleOutlined';
import CheckOutlinedIcon from '@mui/icons-material/CheckOutlined';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import QrCode2SharpIcon from '@mui/icons-material/QrCode2Sharp';
import SendRoundedIcon from '@mui/icons-material/SendRounded';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Box, Button, Divider, IconButton, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import Tab from '@mui/material/Tab';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
import { AuthContext } from '../../../provider/authProvider';
import { WareContext } from '../../../provider/wareProvider/index';
import BarcodeCamera from '../../barcode-scanner';

type CheckType = 'flag' | 'location' | 'order' | "m";

interface OutDataType { 
    id: string | number;
    date: string;
    employee_id: string;
    machinecode: string;
    order_id: string;
    row_number: number;
    type: string;
    check_code: boolean;
};

interface StockDataType {
    id: string | number;
    action: string;
    item_number: number;
    location: string;
    flag_number: string;
    product_name: string;
    quantity: number;
    checkLocation: boolean;
    checkOrder: boolean,
};

 export const GridBox = (props: {
    rows: any[],
    columns: GridColDef[];
    height:number
    bgColor?: string;
     density?: string;
     border?: boolean;
     headerHeight?: number;
}) => {
  const theme = useTheme();
  const small = useMediaQuery(theme.breakpoints.down('sm'));
  return (
        <Box
            sx={{
                pt: 2, height: props.height,
                '& .MuiDataGrid-columnHeaders': { bgcolor:props.bgColor?props.bgColor:"#5D6D7E", color: 'white' }
            }}
        >
            <DataGrid
                    sx={{
                    fontSize: 16,bgcolor:'white',
                    '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                        outline: 'none',
                    },
                        }}
                    hideFooter
                    headerHeight={ props.headerHeight? props.headerHeight:52}
                    density={small||props.density ? 'compact':"comfortable" }
                    rows={props.rows}
                    columns={props.columns}
                    showCellRightBorder={props.border}
                    // pageSize={15}
                    // rowsPerPageOptions={[5]}
                    // showCellRightBorder
                    experimentalFeatures={{ newEditingApi: true }}
            />
        </Box>
  );
};

function WareStockout() {
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const { auth } = useContext(AuthContext);
    const { employee_id } = auth;
    const { dataPack, onSignFor } = useContext(WareContext);
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));


    //state-----------------------------------
    const [tabValue, setTabValue] = useState('1');//tab index
     const [showCamera, setShowCamera] = useState<boolean>(false);
    const [dataset, setDataset] = useState<OutDataType[]>([]); //top 列表

    const [stockData, setStockData] = useState<StockDataType[]>([]);//出庫資訊

    const [selectData, setSelectData] = useState<OutDataType | null>(null);//選擇出貨選項
    
    const [machineCode, setMachineCode] = useState<string>('');//比對機台碼設定

    const [tempData, setTempData]
        = useState<{ data: StockDataType, target: CheckType } | null>(null);//temp data;
    
    //function--------------------------------
    const handleClose=()=>setShowCamera(false);
    const handleMainData = (data:OutDataType) => { 
        setSelectData(data);
    };
    const handleTabChange = (event: React.SyntheticEvent, newValue: string) => {
        setTabValue(newValue);
    };
    
    const handleMatch = (
        data: StockDataType, targetType: CheckType
    ) => {
        setTempData({
            data: data,
            target: targetType
        });
        setShowCamera(true);
        return
    };

    const updatedDataset = (data: StockDataType) => { 
        console.log(data)
        const updated = [...stockData];
        const targetIndex = updated.findIndex(row => row.item_number === data.item_number);
        updated.splice(targetIndex, 1, data);
        setStockData(updated);
        setTempData(null);
        return;
    };
    const onResult = (text: string) => { 
        if (tempData) {
         
            const { data, target } = tempData;
            if (target === 'location') {
                if (data.location === text) {
                    data.checkLocation = true;
                    updatedDataset(data);
                    handleClose();
                };
            };
            if (target === 'flag') {
      
                if (data.flag_number === text) {
                    data.checkOrder = true;
                    updatedDataset(data);
                    handleClose();
                };
            };
        };
        if (machineCode&&selectData) {
            if (text === machineCode) {
                setSelectData({
                    ...selectData,
                    check_code:true
                });
                handleClose();
            };
        };
    };
    /**比對機台碼設定 */
    const handleMatchCode = (code:string) => { 
        setMachineCode(code);
        setShowCamera(true);
    };

    //api-------------------------------------
    /**取得出貨列表 */
    const getOutDataAsync = async () => { 
        try { 
            const res = await api.get("api/management/orders/pending/out");
            const result = res.data.map((data: any, index: number) => {
                return {
                    id: index,
                    check_code: false,
                    ...data
                }
            });
            if (dataPack.orderId) { 
                const target = result.filter((ele:any) => ele.order_id === dataPack.orderId)[0];
                if (target) {
                    handleMainData(target);
                    onSignFor();
                };
            };
            setDataset(result);
        } catch (err: any) { 
            console.log(err.message);
            enqueueSnackbar("讀取資料失敗", { variant: "error" });
        };
    };
    /** 取得出庫資訊*/
     const getOutOrderData = async(data:OutDataType) => { 
        try {
            const res = await api.get(`api/management/orders/${data.order_id}`);
            const result = res.data.map((item: any,index:number) => {
                return {
                    ...item,
                    id:index,
                    checkLocation: false,
                    checkOrder: false,
                };
            });
            setStockData(result);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message, { variant: "error" });
        }
    };
    /**送出 */
    const handleFinish = async () => { 
        if (selectData) {
            for (let data of stockData) {
                const { checkLocation, checkOrder } = data;
                if (!checkLocation || !checkOrder) {
                    enqueueSnackbar("尚未比對完成", { variant: "warning" });
                    return;
                }
            };
            if (!selectData.check_code) {
                enqueueSnackbar("尚未比對完成", { variant: "warning" });
                return;
            };
            try {
                await api.put(`api/management/orders/finish/${selectData.order_id}/${employee_id}`);
                enqueueSnackbar("更新成功", { variant: "success" });
            } catch (err: any) {
                console.log(err.message);
                enqueueSnackbar("更新失敗", { variant: "error" });
            }  
        };
    };

    //useEffect--------------------------------
    useEffect(() => {
        getOutDataAsync();
    }, []);

   
    useEffect(() => { 
        if (selectData) {
            getOutOrderData(selectData);
        };
    }, [selectData]);

    return (
        <Box sx={{ bgcolor: '#FBFCFC',  minHeight: '100vh', mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <BarcodeCamera
                showCamera={showCamera}
                handleClose={handleClose}
                onResult={onResult}
            />
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                   {t("warehouse.stockOut.title")}
                </Typography >
            </Toolbar>
            <Box sx={{ m: 'auto',width: { xs: '100%', sm: '100%', md: '65%', lg: '65%' }}}>
                <h2 style={{ fontWeight: 'bold' }}> {t("warehouse.stockOut.stockoutNew")}</h2>
                <GridBox
                    height={300}
                    rows={dataset}
                    density={"compact"}
                    columns={columns(handleMainData, t, fullScreen)}
                    border={true}
                />
            </Box>
            <Divider sx={{ mt: 2 }} />
            {selectData &&
                <Box sx={{ p: 2 }}>
                    <Button
                        sx={{ float: 'right', mr: 5,display:fullScreen?"none":"" }}
                        startIcon={<SendRoundedIcon />}
                        variant="contained" onClick={handleFinish} color="success">
                        送出
                    </Button>
                <TabContext value={tabValue}>
                    <Box sx={{ m:"auto",mt:3,borderBottom: 1, borderColor: 'divider' ,width:"90%",color:'white'}}>
                        <TabList onChange={handleTabChange} aria-label="tab-information-list" centered={fullScreen}>
                            <Tab label={t("warehouse.stockOut.shipmentInformation")} value="1" sx={{fontWeight:'bold'}} />
                            <Tab label={t("warehouse.stockOut.deliverMc")} value="2"  sx={{fontWeight:'bold'}} />
                        </TabList>
                    </Box>
                    <TabPanel value="1">
                        <Box>
                             <GridBox
                                height={250}
                                rows={stockData}
                                headerHeight={40}
                                columns={outStockColumns(handleMatch,t,fullScreen)}
                            />
                        </Box>
                    </TabPanel>
                    <TabPanel value="2">
                        <Box sx={{width:{xs:'100%',sm:"80%",md:'40%'}}}>
                             <GridBox
                                height={250}
                                rows={[selectData]}
                                 headerHeight={40}
                                columns={deliverColumn(handleMatchCode,t,fullScreen)}
                            />
                        </Box>
                    </TabPanel>
                    </TabContext>
                     <Button
                        sx={{ ml:'35vw',display:fullScreen?"":"none" }}
                        startIcon={<SendRoundedIcon />}
                        variant="contained" onClick={handleFinish} color="success">
                        送出
                    </Button>
                </Box>}
            
        </Box>
    );
};

export default WareStockout;


//出庫列表columns
const columns = (
    checkStatus: (data:OutDataType) => void,
    t: TFunction,
    small:boolean
): GridColDef[] => [
    {
        field: 'check1', headerName: t("warehouse.stockIn.check"), width: 100, align: 'center', headerAlign: 'center',hide:!small,
        renderCell: (params: GridValueGetterParams) => { 
            
            return (
                <IconButton onClick={() => checkStatus(params.row)}>
                    <ArrowCircleRightOutlinedIcon color="success" />
                </IconButton>
            );
        }
    },  
        { field: 'id', headerName: "ID", width: 90, align: 'center', headerAlign: 'center', hide: true },
    { field: 'row_number', headerName: t("warehouse.stockOut.item"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'date', headerName: t("warehouse.stockIn.date"), width: 200, align: 'left', headerAlign: 'left'},
    { field: 'order_id', headerName: t("warehouse.stockIn.order"), width: 150, align: 'right', headerAlign: 'right' },
    { field: 'machinecode', headerName: t("warehouse.stockOut.mcode"), width: 150, align: 'right', headerAlign: 'right' },
    { field: 'employee_id', headerName: t("warehouse.stockIn.filler"), width: 150, align: 'center', headerAlign: 'center' },  
        {
            field: 'check', headerName: t("warehouse.stockIn.check"), width: 120, align: 'center', headerAlign: 'center',hide:small,
            renderCell: (params: GridValueGetterParams) => { 
                
                return (
                    <IconButton onClick={() => checkStatus(params.row)}>
                        <ArrowCircleRightOutlinedIcon color="success" />
                    </IconButton>
                );
            }
        },  
    ];

//出庫資訊columns
const outStockColumns = (
    handleMatch:(data: StockDataType, targetType: CheckType)=>void,
    t: TFunction,
    small: boolean
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
    { field: 'item_number', headerName: t("warehouse.stockOut.no"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'product_name', headerName: t("warehouse.stockOut.product"), width: small?200:350, align: 'center', headerAlign: 'center' },  
    { field: 'quantity', headerName: t("warehouse.stockOut.count"), width: 100, align: 'center', headerAlign: 'center', },
    {
        field: 'location', headerName: t("warehouse.stockOut.information"), width: small?150:250, align: 'center', headerAlign: 'center',
        renderCell: (params: GridValueGetterParams) => {
            return (
                <Button
                    color="warning"
                        size={small ? 'small' : "medium"}
                    variant='outlined'
                    startIcon={<QrCode2SharpIcon />}
                    onClick={() => handleMatch(params.row,'location')}
                >
                    {params.row.location}
                </Button>
            )
        }
    },
        {
            field: 'checkLocation', headerName: t("warehouse.stockOut.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>
                        {params.row.checkLocation ? <CheckOutlinedIcon color="success" /> : <ErrorOutlineOutlinedIcon color="error" />}    
                    </div>
                )
            }
        },
        {
            field: 'order_id', headerName: t("warehouse.stockIn.flagNumber"), width: small?150:250, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        color="warning"
                            size={small ? 'small' : "medium"}
                        variant='outlined'
                        startIcon={<QrCode2SharpIcon />}
                        onClick={() =>  handleMatch(params.row,'flag')}
                    >
                        {params.row.flag_number}
                    </Button>    
                )
            }
        },
        {
            field: 'checkOrder', headerName: t("warehouse.stockOut.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>
                        {params.row.checkOrder?<CheckOutlinedIcon color="success"/>:<ErrorOutlineOutlinedIcon color="error"/>}
                    </div>
                )
            }
        },
    ];

const deliverColumn = (
    handleMatch:(code:string)=>void,
    t: TFunction,
    small: boolean
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
    { field: 'machinecode', headerName: t("warehouse.stockOut.mcode"), width: 150, align: 'center', headerAlign: 'center' },
        {
            field: 'barcode', headerName: t("warehouse.stockOut.barcodeCheck"), width: 150, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        startIcon={<QrCode2SharpIcon />}
                        size="small" onClick={()=>handleMatch(params.row.machinecode)}
                    >
                        {t("warehouse.stockOut.barcodeCheck")}
                    </Button>
                );
            }
        },
        {
            field: 'checked', headerName: t("warehouse.stockOut.check"), width: 80, align: 'center', headerAlign: 'center',
            renderCell: (params) => {
                const { check_code } = params.row;
                return check_code ?
                    <CheckCircleOutlinedIcon color="success" /> : <ErrorOutlineOutlinedIcon color="error" />
            }
        },
    
];
