
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useMemo, useState } from 'react';
import { useTranslation } from 'react-i18next';


import SearchIcon from "@mui/icons-material/Search";
import SendRoundedIcon from '@mui/icons-material/SendRounded';
import { Box, Button, Divider, FormControl, Grid, InputAdornment, ListSubheader, MenuItem, Select, SelectChangeEvent, Stack, TextField, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import moment from 'moment';
import api from '../../../api';
import { AuthContext } from '../../../provider/authProvider';
import AreaLocationContent from '../area-display';


const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: 300
    },
  },
};
const fromStyle = {
    display: 'flex',
    flexDirection: { xs: 'row', sm: 'row' },
    justifyContent: { xs: 'center', sm: "start" },
    width: "90%",
    alignItems: 'center',
    fontSize: { xs: 10, sm: 12, md: 16 }
};
const FromTitleStyle = {
    fontSize: { xs: 16, sm: 18, md: 20 },
    width:160,
    textAlign: 'start',
    fontWeight: 'bold',
    justifyContent:'flex-start'
}
/**form 表單 element */
const OrderForm = (props: { title: string, value: string,onClick?:()=>void }) =>
    <FormControl sx={fromStyle}>
        <Typography variant={"h6"} gutterBottom component="div"
            sx={FromTitleStyle}
        >
            {props.title}:
        </Typography>
        <TextField size="small" value={props.value} onClick={props.onClick} />
    </FormControl>;

//搜尋關鍵字function
const containsText = (text:string, searchText:string) =>
    text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

/**產品接口 */
interface ProductType{
  product_name: string,
  product_number:string
};
/**架位接口 */
interface LocationType{
  num: string;
  place: { name: string, remark: string }[];
};
/**架區接口 */
interface AreaType { 
  area: string;
  location: LocationType[];
};
/**api 資料類別接口 */
interface SubmitPurchaseBodyType { 
  order_id: string,
  date:string,
  employee_id: string
  machinecode: string
  orderitems: any[];
};
/**表單接口 */
interface OrderType { 
    name: string;
    date: string;
    location: string;
  product_name: string;
  product_number: string;
    count: number;
};

const initOrderState: OrderType = {
    name: "",
    date:"",
    location: "",
    product_name: '',
    product_number: '',
    count: 0
};

function WarePurchase() {
     const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const {auth} = useContext(AuthContext);
    const { username } = auth; 
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
    //state---------------------------------------------
    const [searchText, setSearchText] = useState<string>("");
    const [products, setProducts] = useState<ProductType[]>([]);//產品名稱state
    const [locationData, setLocationData] = useState<AreaType[]>([]);

    const [order, setOrder] = useState<OrderType>(initOrderState);
    
    //function-------------------------------------------------
    /**關鍵字查詢產品名稱*/
    const displayedOptions = useMemo(
        () => products.filter((option) => containsText(option.product_name, searchText)),
        [searchText, products]
    );
    /**選擇產品 */
    const handleSelectProduct = (event: SelectChangeEvent) => {
        let value_number = event.target.value;
        const index = products.findIndex(row => row.product_number === value_number);
        const target = products[index];
        setOrder({
            ...order,
            product_name: target.product_name,
            product_number: target.product_number
        });
        return; 
    };
    /**改變數量 */
     const handleChangeCount = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = parseInt(event.target.value);
        setOrder({ ...order, count: value });
    };
     /**綁定選擇的架區 */
    const handleSelectArea = (area: string) => { 
        setOrder({ ...order, location: area });
        };
    //api------------------------------------------------
    /**獲取產品名稱 */
    const getProductAsync = async () => { 
        const res = await api.get("api/product/infos");
        const productData = await res.data;
        const productName = Object.keys(productData);
        const result = productName.map(ele => {
        return {
            product_name: ele,
            product_number: productData[ele]
        };
        });
        setProducts(result);
    };
    /**檢查資料格式，再送出 */
    const handleSendData = () => { 
        const {name,date,location,product_name,count } = order;
        if (!name || !date || !location || !product_name || !count) {
        let msg = '';
        if (!location) msg = `請選擇 ${t("warehouse.purchase.location")}`;
        if (!product_name) msg = `請選擇 ${t("warehouse.purchase.product")}`;
        if(count===0) msg = `${t("warehouse.purchase.count")} 不可為0`;
            enqueueSnackbar(msg?msg:"something miss.", { variant: 'warning' });
        return;
        };
        const [position,area] = location.split('-');
        const body: SubmitPurchaseBodyType = {
            order_id: "",
            date: moment(date).format("YYYY-MM-DD"),
            employee_id: name,
            machinecode: "",
            orderitems: [
                {
                item_number: 1,
                product_name: product_name,
                quantity: count,
                position: position,
                area: area,
                action: "in",
                flag_number: ""
                }
            ]
        };
        return submitPurchase(body);
    };
    /**送出單號 */
    const submitPurchase = async (body:SubmitPurchaseBodyType) => {
        try {
            await api.post("api/management/orders/pending/purchase", body);
            enqueueSnackbar("送出成功", { variant: "success" });
        } catch (err: any) { 
            console.log(err.message)
            enqueueSnackbar("送出失敗。", { variant: 'error' });
        }
    };
     const getEmptyAsync = async () => { 
        const res = await api.get("api/storage/empty");
        setLocationData(res.data);
    };
    //useEffect----------------------------------------------
    useEffect(() => { 
        getProductAsync();
        getEmptyAsync();
        // getNoEmptyAsync()
        setOrder({
            ...order,
            name: username,
            date: moment(Date.now()).format("YYYY-MM-DD")
        });
    }, []);

    return (
        <Box sx={{ bgcolor: 'white',minHeight:'100vh', mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{ pt: 2, pb: 2, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                    {t("warehouse.purchase.title")}
                </Typography >
                
            </Toolbar>
            <Box sx={{ mt: 5, p: 2 }}>
                <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ m: 'auto' }}>
                    <Grid item xs={12} sm={12} md={4}>
                        <OrderForm title={t("warehouse.purchase.filler")} value={order.name} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <OrderForm title={t("operator.treasuryList.fillDate")} value={order.date} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <OrderForm title={t("warehouse.purchase.location")} value={order.location} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <FormControl sx={{ ...fromStyle }}>
                            <Typography variant={"h6"} gutterBottom component="div" sx={FromTitleStyle}>
                                {t("operator.pickList.name")}:
                            </Typography>
                            <Select
                                sx={{ width: 220 }}
                                size="small"
                                labelId="product-name-select"
                                id="product-name-select"
                                value={order.product_number}
                                onChange={handleSelectProduct}
                                onClose={() => setSearchText("")}
                                MenuProps={MenuProps}
                            >
                                <ListSubheader>
                                    <TextField
                                        size="small"
                                        sx={{ overflow: 'hidden' }}
                                        autoFocus
                                        placeholder="Search"
                                        fullWidth
                                        InputProps={{
                                            startAdornment: (
                                                <InputAdornment position="start">
                                                    <SearchIcon />
                                                </InputAdornment>
                                            )
                                        }}
                                        onChange={(e) => setSearchText(e.target.value)}
                                        onKeyDown={(e) => {
                                            if (e.key !== "Escape") {
                                                e.stopPropagation();
                                            }
                                        }}
                                    />
                                </ListSubheader>
                                {displayedOptions.map((row, index) =>
                                    <MenuItem
                                        key={index}
                                        value={row.product_number}
                                    >
                                        {row.product_name}
                                    </MenuItem>
                                )}
                            </Select>
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <FormControl sx={fromStyle}>
                            <Typography variant={"h6"} gutterBottom component="div" sx={FromTitleStyle}>
                                {t("warehouse.purchase.count")}:
                            </Typography>
                            <TextField sx={{ width: 220 }}
                                size="small"
                                type="number"
                                value={order.count}
                                InputProps={{ inputProps: { min: 0, max: 100000 } }}
                                onChange={handleChangeCount}
                                inputProps={{ style: { textAlign: 'center' } }} />
                        </FormControl>
                    </Grid>
                    <Grid item xs={12} sm={12} md={4}>
                        <Stack justifyContent="center">
                            <Button
                                startIcon={<SendRoundedIcon />}
                                variant="contained" sx={{ m: 'auto', width: 200 }} onClick={handleSendData} size="large">
                                {t("warehouse.purchase.send")}
                            </Button>
                        </Stack>
                    </Grid>
                </Grid>
            </Box>
            <Divider sx={{ mt: 2 }} />
            <Box sx={{ p: 2 }}>
                <h2 style={{ fontWeight: 'bold' }}>
                    {t("warehouse.purchase.areaType")}
                </h2>
                <Box sx={{ border: 2, p: 2, minHeight: 300, width: '90%', borderRadius: 3 }}>
                    <AreaLocationContent handleSelectArea={handleSelectArea} product={order.product_number} />
                </Box>
            </Box>
        </Box>
    );
};

export default WarePurchase;