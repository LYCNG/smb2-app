
import CheckOutlinedIcon from '@mui/icons-material/CheckOutlined';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import QrCode2SharpIcon from '@mui/icons-material/QrCode2Sharp';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Box, Button, Divider, useMediaQuery, useTheme } from '@mui/material';
import Tab from '@mui/material/Tab';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';
import BarcodeCamera from '../../../barcode-scanner';
import { DataType } from '../index';


type CheckType = 'flag' | 'location' | 'order';

interface EnterType { 
    id: number;
    item_number: number,
    product_name: string,
    quantity: number,
    location: string,
    flag_number: string,
    checkLocation: boolean;
    checkFlag: boolean,
    action:string|null
};

function OutPage(
    props: {
        data: DataType,handleReGetData:()=>void
}) {

    const { t } = useTranslation();
    const { auth } = useContext(AuthContext);
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('md'));
    const { data,handleReGetData } = props;
    const { enqueueSnackbar } = useSnackbar();
    //state--------------------------------------------------------------------
    const [finish, setFinish] = useState<boolean>(false);
    const [row, setRow] = useState<DataType[]>([]);
    const [inDataset, setInDataset] = useState<EnterType[]>([]);
    const [outDataset, setOutDataset] = useState<EnterType[]>([]);

    const [value, setValue] = React.useState('1');
    const [showCamera, setShowCamera] = useState<boolean>(false);
    const [tempData, setTempData]
        = useState<{ data: EnterType, target: CheckType } | null>(null);


    const mapKey = (data: any[]) => { 
        const newData = data.map((item, index) => { 
            return {
                ...item,
                id:index
            }
        });
        return newData;
    };
    
      const handleFinishCheck = async () => { 
        for (let data of inDataset) {
            const { checkLocation, checkFlag} = data;
            if (!checkLocation || !checkFlag) {
                return enqueueSnackbar("尚有資料未比對完成。", { variant: "warning" });
            };
          };
          for (let data of outDataset) {
            const { checkLocation, checkFlag} = data;
            if (!checkLocation || !checkFlag) {
                return enqueueSnackbar("尚有資料未比對完成。", { variant: "warning" });
            };
        };
        try {
            await api.put(`api/management/orders/finish/${data.order_id}/${auth.username}`);
            enqueueSnackbar("送出成功。", { variant: "success" });
            return handleReGetData()
        } catch (err: any) {
            console.log(err)
            enqueueSnackbar("送出失敗。", { variant: "error" });
        }
    };

    const updatedDataset = (data: EnterType) => { 
        const { action } = data;
        if (action === 'in') {
            const updated = [...inDataset];
            const targetIndex = updated.findIndex(row => row.item_number === data.item_number);
            updated.splice(targetIndex, 1, data);
            setInDataset(updated);
        };
        if (action === 'out') {
            const updated = [...outDataset];
            const targetIndex = updated.findIndex(row => row.item_number === data.item_number);
            updated.splice(targetIndex, 1, data);
            setOutDataset(updated);
        };
        setTempData(null);
        return;
    };

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };
    const handleClose = () => setShowCamera(false);
    const handleMatchTarget = (
        data: EnterType, targetType: CheckType
    ) => {
        setTempData({
            data: data,
            target: targetType
        });
        setShowCamera(true);
        return
    };
    const onResult = (text: string) => { 
        if (tempData) {
            const { data, target } = tempData;
            if (target === 'location') {
                if (data.location === text) {
                    data.checkLocation = true;
                    updatedDataset(data);
                    handleClose()
                };
            };
            if (target === 'flag') {
                if (data.flag_number === text) {
                    data.checkFlag= true;
                    updatedDataset(data);
                    handleClose()
                };
            };
        };
    };
     const getDatasetAsync = async() => { 
         try { 
            const res = await api.get("api/management/orders/" + data.order_id);
            const result:EnterType[] = res.data.map((ele:any) => {
                return {
                    ...ele,
                    flag_number:ele.flag_number,
                    checkLocation: false,
                    checkFlag: false
                }
            })
            const in_data = result.filter(ele => ele.action && ele.action === 'in');
            const out_data = result.filter(ele => ele.action && ele.action === 'out');
            setInDataset(mapKey(in_data));
            setOutDataset(mapKey(out_data));

        } catch (err: any) { 
            console.log(err.message);
        };
    };

    //useEffect-----------------------------------------
    useEffect(() => { 
        const total = [...inDataset, ...outDataset];
        for (let data of total) {
            const { checkLocation, checkFlag } = data;
            if (!checkLocation || !checkFlag) {
                return  setFinish(false);
            };
        }
        return setFinish(true);
    }, [inDataset,outDataset]);

    useEffect(() => {

        const update = { ...data, id: 0 };
        setRow([update]);
        getDatasetAsync()
    }, [data]);
    
    return (
        <Box sx={{ pt: 1 }}>
            <BarcodeCamera
                showCamera={showCamera}
                handleClose={handleClose}
                onResult={onResult}
            />
         
                 <Box
                    sx={{
                        height: 150, bgcolor:'white',
                        pt: 0,mt:4,  width: { xs: '100%', sm: '100%', md: '80%', lg: '65%' },
                        '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                }}>
                    <DataGrid
                        sx={{ 
                            fontSize:18,bgcolor:'white',
                            '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                outline: 'none',
                            },
                        }}
                        hideFooter
                        density={fullScreen?'compact':"comfortable"}
                        rows={row}
                        columns={titleColumns(t)}
                        pageSize={10}
                        rowsPerPageOptions={[5]}
                        experimentalFeatures={{ newEditingApi: true }}
                    />
                </Box>
      
            <Divider sx={{mt:4}} />
           <TabContext value={value}>
                <Box sx={{mt:3,borderBottom: 1, borderColor: 'divider' ,width:"100%",color:'white'}}>
                <TabList onChange={handleChange} aria-label="tab-out-information-list" centered={fullScreen}>
                    <Tab label={t("warehouse.stockIn.getInformation")} value="1" sx={{fontWeight:'bold'}} />
                    <Tab label={t("warehouse.stockIn.enterInformation")} value="2"  sx={{fontWeight:'bold'}} />
                </TabList>
                </Box>
                <TabPanel value="1">
                    <Box
                        sx={{
                            height: 300, width:"100%",
                            pt: 0,mt:2,
                            '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                    }}>
                        <DataGrid
                            sx={{ 
                                fontSize:18,bgcolor:'white',
                                '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                    outline: 'none',
                                },
                            
                            }}
                            hideFooter
                            // density={'compact'}
                            density={fullScreen?'compact':"comfortable"}
                            rows={outDataset}
                            columns={getColumns(handleMatchTarget,t,fullScreen)}
                            pageSize={10}
                            rowsPerPageOptions={[5]}
                            experimentalFeatures={{ newEditingApi: true }}
                    />
                    </Box>
                </TabPanel>
                <TabPanel value="2">
                     <Box
                        sx={{
                            height: 250, width:"100%",
                            pt: 0,mt:2,
                            '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                    }}>
                        <DataGrid
                            sx={{ 
                                fontSize:18,bgcolor:'white',
                                '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                    outline: 'none',
                                },
                            
                            }}
                            hideFooter
                            density={fullScreen?'compact':"comfortable"}
                            rows={inDataset}
                            columns={getColumns(handleMatchTarget,t,fullScreen)}
                            pageSize={10}
                            rowsPerPageOptions={[5]}
                            experimentalFeatures={{ newEditingApi: true }}
                    />
                    </Box>

                </TabPanel>
            </TabContext>
            <Box >
                <Button
                    disabled={!finish}
                    color="success"
                    variant='contained'
                    sx={{ml:"45%",height:50}}
                    onClick={handleFinishCheck}
                    >
                    {t("warehouse.stockIn.finish")}
                </Button>  
            </Box>
        </Box>
    );
}

export default OutPage;


const titleColumns = (
    t: TFunction,
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
    { field: 'row_number', headerName: t("warehouse.stockIn.order"), width: 80, align: 'center', headerAlign: 'center' },
        {
            field: 'type', headerName: t("warehouse.stockIn.status"), width: 120, align: 'center', headerAlign: 'center',
            valueGetter: (params: GridValueGetterParams) => t(`type.${params.row.type}`)
        },
    { field: 'date', headerName: t("warehouse.stockIn.date"), width: 300, align: 'center', headerAlign: 'center' },
    { field: 'employee_id', headerName: t("warehouse.stockIn.filler"), width: 200, align: 'center', headerAlign: 'center' },
];

const getColumns = (
        handleMatch:(data: EnterType, targetType: CheckType)=>void,
        t: TFunction,
      small:boolean
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
    { field: 'item_number', headerName: t("warehouse.stockIn.no"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'product_name', headerName: t("warehouse.stockIn.product"), width: 250, align: 'center', headerAlign: 'center' },  
    { field: 'quantity', headerName: t("warehouse.stockIn.count"), width: 100, align: 'center', headerAlign: 'center', },
        {
            field: 'location', headerName: t("warehouse.stockIn.carrierLocation"), width: 200, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        color="warning"
                            size={small ? 'small' : "medium"}
                        variant='outlined'
                        startIcon={<QrCode2SharpIcon />}
                        onClick={() => handleMatch(params.row,'location')}
                    >
                        {params.row.location}
                    </Button>
                )
            }
        },
        {
            field: 'checkLocation', headerName: t("warehouse.stockIn.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>
                        {
                            params.row.checkLocation ? <CheckOutlinedIcon color="success" /> : <ErrorOutlineOutlinedIcon color="error" />
                        }    
                    </div>
                )
            }
        },
        {
            field: 'flag_number', headerName: t("warehouse.stockIn.flagNumber"), width: 200, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        color="warning"
                            size={small ? 'small' : "medium"}
                        variant='outlined'
                        startIcon={<QrCode2SharpIcon />}
                        onClick={() =>  handleMatch(params.row,'flag')}
                    >
                        {params.row.flag_number}
                    </Button>    
                )
            }
        },
        {
            field: 'checkFlag', headerName: t("warehouse.stockIn.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>
                         {params.row.checkFlag?<CheckOutlinedIcon color="success"/>:<ErrorOutlineOutlinedIcon color="error"/>}
                    </div>
                )
            }
        },
    ];


const inColumns = (
        handleMatch:(data: EnterType, targetType: CheckType)=>void,
        t: TFunction,
      small:boolean,
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
    { field: 'item_number', headerName: t("warehouse.stockIn.no"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'product_name', headerName: t("warehouse.stockIn.product"), width: 350, align: 'center', headerAlign: 'center' },
    
    { field: 'quantity', headerName: t("warehouse.stockIn.count"), width: 100, align: 'center', headerAlign: 'center', },
        {
            field: 'location', headerName: t("warehouse.stockIn.carrierLocation"), width: 250, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        color="warning"
                         variant='outlined'
                        startIcon={<QrCode2SharpIcon />}
                        onClick={() => handleMatch(params.row, 'location')}
                        size={small?'small':"medium"}
                    >
                        {params.row.location}
                    </Button>
                )
            }
        },
        {
            field: 'checkLocation', headerName: t("warehouse.stockIn.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>
                        {
                            params.row.checkLocation ? <CheckOutlinedIcon color="success" /> : <ErrorOutlineOutlinedIcon color="error" />
                        }    
                    </div>
                )
            }
        },
        {
            field: 'flag_number', headerName: t("warehouse.stockIn.flagNumber"), width: 250, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <Button
                        color="warning"
                        variant='outlined'
                        startIcon={<QrCode2SharpIcon />}
                        onClick={() => handleMatch(params.row, 'flag')}
                        size={small ? 'small' : "medium"}
                        
                    >
                        {params.row.flag_number}
                    </Button>    
                )
            }
        },
        {
            field: 'checkFlag', headerName: t("warehouse.stockIn.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>
                         {params.row.checkFlag?<CheckOutlinedIcon color="success"/>:<ErrorOutlineOutlinedIcon color="error"/>}
                    </div>
                )
            }
        },
];

