import ArrowCircleRightOutlinedIcon from '@mui/icons-material/ArrowCircleRightOutlined';
import CloseRoundedIcon from '@mui/icons-material/CloseRounded';
import { Box, IconButton, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import { useContext, useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
import InPage from './inPage';
import OutPage from './outPage';

import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogTitle from '@mui/material/DialogTitle';
import { WareContext } from '../../../provider/wareProvider/index';

export const enum StatusType { 
    in = "in",
    out="out",
    none="none"
};

export interface DataType{
    id: number;
    row_number: number,
    date: string,
    type: string,
    order_id: string,
    employee_id: string;
    machinecode: string;
};

function WareStockin() {
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const { dataPack,onSignFor } = useContext(WareContext);
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('md'));

    //state---------------------------------------------------
    const [dataset, setDataset] = useState<DataType[]>([]);
    const [statusPage, setStatusPage] = useState<StatusType>(StatusType.none);
    const [selectData, setSelectData] = useState<DataType>();
    const [showModal, setShowModal] = useState<boolean>(false);
    //api-----------------------------------------------------



    const getPendingInOrderAsync = async () => { 
        try {
            const res = await api.get("api/management/orders/pending/in");
            const data = res.data.map((item: any, index: number) => {
                return {
                    id: index,
                    ...item
                };
            });
    
            if (dataPack.orderId) { 
                const target = data.filter((ele: any) => ele.order_id === dataPack.orderId)[0];
                if (target) {
                     setShowModal(true);
                    handleCheck(target);
                    onSignFor();
                };
            };
            setDataset(data);
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message, { variant: 'error' });
        }
    };

    const handleCheck = (data: DataType) => {
        console.log(data)
        setShowModal(true);
        const { type } = data;
        setSelectData(data);
        switch (type) {
            case "in":
            case "purchase":
                return setStatusPage(StatusType.in);
            case "merge":
            case "change":
                return setStatusPage(StatusType.out);
            default:
                return setStatusPage(StatusType.none);
        };
    };
    /**關閉顯示 */
    const handleCloseModal = () => {
        setShowModal(false);
        setStatusPage(StatusType.none);
    };
    //useEffect------------------------------------------------
    useEffect(() => { 
        getPendingInOrderAsync();
    }, [dataPack]);


    return (
        <Box sx={{ bgcolor: '#FBFCFC',  minHeight: '100vh', mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                    {t("warehouse.stockIn.title")}
                </Typography >
            </Toolbar>
             <Box
                sx={{
                    pt: 1, m: 'auto', height: 600, width: { xs: '100%', sm: '100%', md: '75%', lg: '68%' },
                    '& .MuiDataGrid-columnHeaders': { bgcolor: "#5D6D7E",color:'white' }
                }}>
                <h2 style={{ fontWeight: 'bold', paddingLeft: 20 }}> {t("warehouse.stockIn.information")}</h2>
                <DataGrid
                    sx={{ 
                        fontSize:18,
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                       
                    }}
                    hideFooter
                    density={'compact'}
                    rows={dataset}
                    columns={columns(handleCheck, t, small)}
                    showCellRightBorder
                    // pageSize={10}
                    // rowsPerPageOptions={[5]}
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
         
            <Dialog
                fullScreen={small}
                open={showModal}
                onClose={handleCloseModal}
                fullWidth={true}
                maxWidth={'lg'}
                >
                <DialogTitle id="select-data-title" sx={{ height:50,bgcolor: "#CA6F1E", color: 'white', display: 'flex', justifyContent: 'space-between' }}>
                    <div style={{width:350,display:'flex',justifyContent:'space-between',alignItems:'center'}}>
                        <p>單號: {selectData?.order_id}</p>
                        {/* <p>類別: {t(`type.${selectData?.type}`)}</p> */}
                    </div>
                    
                    <IconButton onClick={handleCloseModal}>
                        <CloseRoundedIcon />
                    </IconButton>
                </DialogTitle>
                <DialogContent >
                    {selectData && (statusPage === "in" ? 
                        <InPage data={selectData} handleReGetData={getPendingInOrderAsync} /> :
                        <OutPage data={selectData} handleReGetData={getPendingInOrderAsync} />
                    )}
                </DialogContent>
             
            </Dialog>
           
        </Box>
    );
};

export default WareStockin;


const columns = (
    checkStatus: (data:DataType) => void,
    t: TFunction,
    small:boolean
): GridColDef[] => [
    {
            field: 'check1', headerName: t("warehouse.stockIn.check"), width: 100, align: 'center', headerAlign: 'center',hide:!small,
            renderCell: (params: GridValueGetterParams) => { 
                
                return (
                    <IconButton onClick={() => checkStatus(params.row)}>
                        <ArrowCircleRightOutlinedIcon color="success" />
                    </IconButton>
                );
            }
        },  
    { field: 'id', headerName: t("warehouse.stockIn.item"), width: 90, align: 'center', headerAlign: 'center',hide:small },
    { field: 'date', headerName: t("warehouse.stockIn.date"), width: 250, align: 'left', headerAlign: 'left' ,hide:small},
        {
            field: 'type', headerName: t("warehouse.stockIn.status"), width: 120, align: 'center', headerAlign: 'center',
                 valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)
        },
    { field: 'order_id', headerName: t("warehouse.stockIn.order"), width: 200, align: 'right', headerAlign: 'right' },
    { field: 'employee_id', headerName: t("warehouse.stockIn.filler"), width: 150, align: 'center', headerAlign: 'center' },  
        {
            field: 'check', headerName: t("warehouse.stockIn.check"), width: 120, align: 'center', headerAlign: 'center',hide:small,
            renderCell: (params: GridValueGetterParams) => { 
                
                return (
                    <IconButton onClick={() => checkStatus(params.row)}>
                        <ArrowCircleRightOutlinedIcon color="success" />
                    </IconButton>
                );
            }
        },  
    ];


export type CheckType = 'flag' | 'location' | 'order';