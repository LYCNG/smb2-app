
import CheckOutlinedIcon from '@mui/icons-material/CheckOutlined';
import CloseIcon from '@mui/icons-material/Close';
import ErrorOutlineOutlinedIcon from '@mui/icons-material/ErrorOutlineOutlined';
import QrCode2SharpIcon from '@mui/icons-material/QrCode2Sharp';
import { Box, Button, Divider, useMediaQuery, useTheme } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import { useContext, useEffect, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../../api';
import { AuthContext } from '../../../../provider/authProvider';

import BarcodeCamera from '../../../barcode-scanner';
import { DataType } from '../index';


interface EnterType { 
    id: number;
    item_number: number,
    product_name: string,
    quantity: number,
    location: string,
    flag_number: string,
    checkLocation: boolean;
    checkFlag: boolean,
    action: string | null,
    status: string | null;
};

type CheckType = 'flag' | 'location' | 'order';

const InPage = (
    props: {
        data: DataType,
        handleReGetData:()=>void
    }
) => { 
    const { data,handleReGetData} = props;
    const { enqueueSnackbar } = useSnackbar();
    const { t } = useTranslation();
    const {auth} = useContext(AuthContext);
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('sm'));
    //state----------------------------------------------------------------------
    const [row, setRow] = useState<DataType[]>([]);
    const [mainData, setMainData] = useState<DataType | null>(null);
    const [match, setMatch] = useState<boolean>(false);//主單號項目比對
    const [dataset, setDataset] = useState<EnterType[]>([]);
    const [showCamera, setShowCamera] = useState<boolean>(false);
    const [tempData, setTempData] = useState<{ data: EnterType, target: CheckType } | null>(null);
  
    //function--------------------------------------------------------------------
    const getDatasetAsync = async() => { 
        try { 
            const res = await api.get("api/management/orders/" + data.order_id);
            const result = res.data.map((ele: any,index:number) => {
                return {
                    ...ele,
                    id:index,
                    flag_number: ele.flag_number,
                    checkLocation: false,
                    checkFlag: false
                }
            });
            setDataset(result);
        } catch (err: any) { 
            console.log(err.message);
        };
    };

    const handleClose = () => setShowCamera(false);

    const handleMatchOrder = (data:DataType) => { 
        setMainData(data);
        setShowCamera(true);
    };

    const handleMatchTarget = (
        data: EnterType, targetType: CheckType
    ) => {
        setTempData({
            data: data,
            target: targetType
        });
        setShowCamera(true);
        return
    };

    const updatedDataset = (data: EnterType) => { 
        const updated = [...dataset];
        const targetIndex = updated.findIndex(row => row.item_number === data.item_number);
        updated.splice(targetIndex, 1, data);
        setDataset(updated);
        setTempData(null);
        return;
    };
    const onResult = (text: string) => { 
        if (tempData) {
            const { data, target } = tempData;
            if (target === 'location') {
                if (data.location === text) {
                    data.checkLocation = true;
                    updatedDataset(data);
                    handleClose()
                };
            };
            if (target === 'flag') {
                if (data.flag_number === text) {
                    data.checkFlag = true;
                    updatedDataset(data);
                    handleClose()
                };
            };
        };
        if (mainData) {
            const { order_id, machinecode } = mainData;
            if (order_id === text||machinecode===text) {
                setMatch(true);
                setMainData(null);
                return;
            };
        };
    };
    //api----------------------------------------------------------------------------
    /**送出 */
    const handleFinishCheck = async () => { 
        for (let data of dataset) {
            const { status, checkLocation, checkFlag } = data;
            if (status==="報廢"||status==="不良品") {
                continue;
            }else if(!checkLocation || !checkFlag) {
                return enqueueSnackbar("尚有資料未比對完成。", { variant: "warning" });
            };
        };
        if (!match) {
            enqueueSnackbar("尚有資料未比對完成。",{ variant: "warning" });
            return;
        }
        try {
            await api.put(`api/management/orders/finish/${data.order_id}/${auth.username}`);
            enqueueSnackbar("送出成功。", { variant: "success" });
            return handleReGetData();
        } catch (err: any) {
            console.log(err)
        }
    };
    //useEffect----------------------------------------------------
    
    useEffect(() => {
        getDatasetAsync()
        if (data.type === 'purchase') { 
            setMatch(true);
        };
        const update = { ...data, id: 0 };
        setRow([update]);
    }, [data]);
    
    return (
        <Box sx={{  pt:2,pb: 2 }}>
            <BarcodeCamera
                showCamera={showCamera}
                handleClose={handleClose}
                onResult={onResult}
            />
                <Box
                    sx={{
                        height: 150, 
                        pt: 0, mt:4,  width: { xs: '100%', sm: '100%', md: '80%', lg: '90%' },
                        '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                }}>
                    <DataGrid
                        sx={{ 
                            fontSize:18,bgcolor:'white',
                            '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                outline: 'none',
                            },
                        }}
                        hideFooter
                        density={small?'compact':"comfortable"}
                        rows={row}
                        columns={titleColumns(match,handleMatchOrder,t)}
                        pageSize={10}
                        rowsPerPageOptions={[5]}
                        experimentalFeatures={{ newEditingApi: true }}
                    />
                </Box>
               
      
            <Divider sx={{mt:2}} />
            <Box
                sx={{
                    height: 350, width:"97%",
                    pt: 0,mt:4,mb:4,
                    '& .MuiDataGrid-columnHeaders': { bgcolor: "#34495E",color:'white' }
                }}
            >
                <DataGrid
                    sx={{ 
                        fontSize:18,bgcolor:'white',
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                    }}
                    hideFooter
                    density={small ? 'compact' : "comfortable"}
                    rows={dataset}
                    //rows={dataset.filter(data=> data.status==="良品" || !data.status )}
                    columns={contentColumns(handleMatchTarget,t,small)}
                    pageSize={10}
                    rowsPerPageOptions={[5]}
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
            {/* <Box>
                {dataset.filter(data => data.status === "不良品" || data.status === "報廢").map(ele =>
                     <ListItemText  primary={ele.status} />)}
            </Box> */}
            <Box >
                <Button
               
                    color="success"
                    variant='contained'
                    sx={{ml:"45%",height:50}}
                    onClick={handleFinishCheck}
                    >
                    {t("warehouse.stockIn.finish")}
                </Button>  
            </Box>
             
        </Box>
    )
};

export default InPage;


const titleColumns = (
    match: boolean,
    handleMatchOrder:(data:DataType)=>void,
    t: TFunction,
): GridColDef[] => [
        { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
        { field: 'row_number', headerName: t("warehouse.stockIn.order"), width: 80, align: 'center', headerAlign: 'center' },
        {
            field: 'type', headerName: t("warehouse.stockIn.status"), width: 120, align: 'center', headerAlign: 'center',
            valueGetter: (params: GridValueGetterParams) => t(`type.${params.row.type}`)
        },
        { field: 'date', headerName: t("warehouse.stockIn.date"), width: 250, align: 'center', headerAlign: 'center' },
        { field: 'employee_id', headerName: t("warehouse.stockIn.filler"), width: 150, align: 'center', headerAlign: 'center' },
        {
            field: 'order_id', headerName: t("warehouse.stockIn.getNews"), width: 280, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { machinecode, order_id, type } = params.row; 
          
                return (
                    <Button
                        color="warning"
                            startIcon={<QrCode2SharpIcon />}
                            onClick={() => handleMatchOrder(params.row)}
                        // size={small?'small':"medium"}
                        >
                            {type==="in"?machinecode:order_id}
                        </Button>     
                )
            },
        },
        {
            field: 'match', headerName: t("warehouse.stockIn.match"), width: 80, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                return (
                    <div>{
                        match ?
                            <CheckOutlinedIcon color="success" /> :
                            <ErrorOutlineOutlinedIcon color="error" />}
                    </div>
            )}
        }, 
    ];

const contentColumns = (
        handleMatch:(data: EnterType, targetType: CheckType)=>void,
        t: TFunction,
      small:boolean,
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center',hide:true },
    { field: 'item_number', headerName: t("warehouse.stockIn.no"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'product_name', headerName: t("warehouse.stockIn.product"), width: 250, align: 'center', headerAlign: 'center' },
    
    { field: 'quantity', headerName: t("warehouse.stockIn.count"), width: 100, align: 'center', headerAlign: 'center', },
        {
            field: 'location', headerName: t("warehouse.stockIn.carrierLocation"), width: 150, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { status } = params.row;
                const discard = status === "報廢" || status === "不良品";
                return (
                    <Button
                        disabled={discard}
                        color="warning"
                        startIcon={<QrCode2SharpIcon />}
                          variant='outlined'
                        onClick={() => handleMatch(params.row, 'location')}
                        size={small?'small':"medium"}
                    >
                        {params.row.location}
                    </Button>
                )
            }
        },
        {
            field: 'checkLocation', headerName: t("warehouse.stockIn.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { status } = params.row;
                const discard = status === "報廢" || status === "不良品";
                return (
                    //CloseIcon
                    <div>{
                         discard?<CloseIcon />:
                        (params.row.checkLocation ?
                            <CheckOutlinedIcon color="success" /> :
                            <ErrorOutlineOutlinedIcon color="error" />)
                    }
                    </div>
                )
            }
        },
        {
            field: 'flag_number', headerName: t("warehouse.stockIn.flagNumber"), width: 230, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                  const { status } = params.row;
               const discard = (status === "報廢") || (status === "不良品");
                return (
                    <Button
                           disabled={discard}
                        color="warning"
                        variant='outlined'
                        startIcon={<QrCode2SharpIcon />}
                        onClick={() => handleMatch(params.row, 'flag')}
                        size={small?'small':"medium"}
                    >
                        {params.row.flag_number}
                    </Button>    
                )
            }
        },
        {
            field: 'checkFlag', headerName: t("warehouse.stockIn.match"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { status } = params.row;
                const discard = (status === "報廢") || (status === "不良品");
                return (
                    <div>
                        {discard ? <CloseIcon /> :
                            (params.row.checkFlag ?
                                <CheckOutlinedIcon color="success" /> :
                                <ErrorOutlineOutlinedIcon color="error" />
                        )}
                    </div>
                )
            }
        },
        {
            field: 'status', headerName: t("warehouse.stockIn.status"), width: 100, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { status } = params.row;
                const discard = (status === "報廢") || (status === "不良品");
                return (
                    <div>
                        {discard ? status:null}
                    </div>
                )
            }
        },
];

