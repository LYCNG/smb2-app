
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import { Box, Button, FormControl, IconButton, Stack, TextField, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import React, { useEffect, useMemo, useState } from 'react';
import { CSVLink } from "react-csv";
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';

interface StorageDataType { 
    id: number | string;
    location: string;
    position: string;
    area: string,
    update_time: string,
    flag_number: string,
    product_name: string,
    remain: number,
    remark: string,
    status: string
};

const FormContainer = (props: {
    title: string
    value: string;
    onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
}) => {
    const { t } = useTranslation();
    return (
        <FormControl sx={{ maxWidth: 250, display: 'flex', flexDirection: 'row', justifyContent: "space-between", alignItems: 'center' }}>
            <h3 style={{ marginRight: 10 }}>{props.title}:</h3>
            <TextField
                label={t("warehouse.keyword")}
                value={props.value}
                size="small"
                sx={{ bgcolor: 'white', width: { xs: 120, sm: 150, md: 180 }}}
                onChange={props.onChange}
            />
        </FormControl>
    );
};

const containsText = (text:string, searchText:string) =>
    text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

function StorageOverview() {
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('md'));

    const [dataset, setDataset] = useState<StorageDataType[]>([]);
    const [searchText, setSearchText] = useState<string>("");
    const [nameText, setNameText] = useState<string>("");

    const onChangeSearchText = (event: React.ChangeEvent<HTMLInputElement>) => { 
        setSearchText(event.target.value);
    };
    const onChangeProductText = (event: React.ChangeEvent<HTMLInputElement>) => { 
        setNameText(event.target.value);
    };
    
    const getAllStorageAsync = async () => { 
        try { 
            const res = await api.get("api/storage/all");
            const result:StorageDataType[] = res.data.map((data:any,index:number) => {
                return {
                    ...data,
                    id:index
                }
            });
            setDataset(result);
        } catch (err: any) { 
            console.log(err.message);
        };
    };

    const handlePrint = async (data:any) => { 
        const { type } = data;
        try {
            const body = {
                "date":data.update_time,
                "location":data.location,
                "flag_number":data.flag_number,
                "product_number":data.product_number,
                "product_name":data.product_name,
                "quantity":data.remain
            };
            await api.post(`api/print/${type}`, body);
            enqueueSnackbar("已送出",{variant:'success'})
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar("送出失敗", { variant: 'error' });
        };
    };


    const displayStorageDataset = useMemo(() =>
        dataset.filter(data => containsText(data.location, searchText) && containsText(data.product_name,nameText))
    , [dataset, searchText,nameText]);

    
    
    useEffect(() => { 
        getAllStorageAsync();
    }, []);


    return (
        <Box sx={{ bgcolor: '#FBFCFC', width: '100%', minHeight: 1000, mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{ pt: 1, display: 'flex', justifyContent: 'space-between' }}>
                <Typography variant={"h4"} sx={{fontWeight:'bold',fontSize:"3.5vh"}} align="center">
                    {t("warehouse.manage.title")} : {t("warehouse.manage.storage")}
                </Typography>
            </Toolbar>
        
            <Box sx={{ m: 'auto', width: { xs: "100%", sm: "100%", md: "90%" } }}>
                <Stack sx={{width: { xs: "90%", sm: "90%", md: "100%" },m:'auto' }}>
                    <FormContainer
                        title={t("warehouse.manage.rack")}
                        value={searchText}
                        onChange={onChangeSearchText} 
                        />
                    <FormContainer
                        title={t("warehouse.manage.product")}
                        value={nameText}
                        onChange={onChangeProductText } 
                    />
                </Stack>
                <CSVLink data={dataset}>
                    <Button color='success' variant='contained'> 下載csv檔案</Button>
                </CSVLink>
                <Box
                    sx={{
                        width:{ xs: "100%", sm: "100%", md: "90%" },
                        pt: 2, height:680,
                        '& .MuiDataGrid-columnHeaders': { bgcolor:"#1E8449", color: 'white' }}}
                >
                    <DataGrid
                        sx={{
                        fontSize: 16,bgcolor:'white',
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                            }}
                        density={'compact'}
                        rows={displayStorageDataset}
                        columns={storageColumn(t,small, handlePrint)}
                        showCellRightBorder
                        pageSize={16}
                        rowsPerPageOptions={[5]}
                        experimentalFeatures={{ newEditingApi: true }}
                    />
                </Box>    
            </Box>
         </Box>
    );
};

export default StorageOverview;

const storageColumn = (
    t: TFunction,
    small: boolean,
    handleClick?: (data: any) => void,
): GridColDef[] => [
    { field: 'id', headerName: 'ID', width: 80, align: 'center', headerAlign: 'center', hide: true },
    { field: 'location', headerName: t("warehouse.manage.rack"), width: 80, align: 'center', headerAlign: 'center' },
    { field: 'area', headerName: t("warehouse.manage.area"), width:50, align: 'center', headerAlign: 'center',hide:small },  
    { field: 'remain', headerName: t("warehouse.manage.remain"), width: 80, align: 'center', headerAlign: 'center', },
        {
            field: 'update_time', headerName: t("warehouse.manage.enterDate"), width: 220, align: 'center', headerAlign: 'center', hide: small,
             valueGetter: (params: GridValueGetterParams) =>params.row.update_time//moment(params.row.update_time).format("YYYY/MM/DD HH:mm:ss")
        },
    { field: 'flag_number', headerName: t("warehouse.manage.card"), width: 150, align: 'center', headerAlign: 'center', },
    {
        field: 'status', headerName: t("warehouse.manage.status"), width: 100, align: 'center', headerAlign: 'center',hide:small,
        valueGetter: (params: GridValueGetterParams) =>params.row.status? t(`warehouse.main.${params.row.status}`):""
    },
    { field: 'product_name', headerName: t("warehouse.manage.product"), width: 240, align: 'center', headerAlign: 'center' }, 
    
    {
        field: 'remark', headerName: t("warehouse.manage.mark"), width: 280, align: 'center', headerAlign: 'center', hide: small,
        renderCell: (params: GridValueGetterParams) => {
            return(
                <p style={{ color: 'red' }}>
                    {params.row.remark}
                </p>
        )}
    }, 
    {
        field: 'action', headerName: t("warehouse.print"), width: 100, align: 'center', headerAlign: 'center',
        renderCell: (params: GridValueGetterParams) => {
            return <>
                <IconButton onClick={()=>handleClick && handleClick(params.row)} color='primary' disabled={!params.row.status}>
                    <LocalPrintshopOutlinedIcon />
                </IconButton>
            </>
        }
    },
];