
import DeleteOutlineOutlinedIcon from '@mui/icons-material/DeleteOutlineOutlined';
import LocalPrintshopOutlinedIcon from '@mui/icons-material/LocalPrintshopOutlined';
import OpenInNewOutlinedIcon from '@mui/icons-material/OpenInNewOutlined';
import { Box, IconButton, SxProps, Typography } from '@mui/material';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import { useSnackbar } from 'notistack';
import { useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
import CheckDataLog, { CheckListType } from '../../check-data-modal';

export interface OnPendingType {
    row_number: number,
    date: string,
    type: string,
    order_id: string,
    employee_id: string,
    machinecode: string,
    id: number|string
};

function OnPendingTable(props: {
    sx: SxProps;
    title: string;
    rows: OnPendingType[];
    reload: () => void;
    type?: string;
}) {
    const { sx,title, rows,reload ,type} = props;
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const [checkData, setCheckData]
        = useState<CheckListType | null>(null);
    
    /**
    * *  取得查詢資料類別 
    */
    const handleCheck = async (orderId: string) => { 
        try {   
            const res = await api.get("api/management/orders/" + orderId);
            const data = await res.data;//get data list
            setCheckData({ orderId: orderId, data: data });
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar(err.message, { variant: 'error' });
        };
    };
    const handleCheckDelete = (orderId: string) => { 
        const sure = window.confirm(`你確定要刪除 ${orderId} 嗎?`)
        if (sure) {
            handleDelete(orderId);
            return 
        }
    };
    const handleDelete = async (order_id:string) => {
        try {
            await api.delete(`/api/management/pending/${order_id}`);
            enqueueSnackbar("刪除成功", { variant: "success" });
            reload();
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar("刪除失敗", { variant: "error" });
        };
    };
     const handlePrint = async (data:any) => { 
        const { type, order_id } = data;
        try {
            const res = await api.get(`/api/print/${type}/${order_id}`);
            const inAction = res.data.filter((ele: any) => ele.action === 'in')[0];
            const { date,location,product_name,product_number,quantity,flag_number} = inAction;
            const data = {
                "date":date,
                "location":location,
                "flag_number":flag_number,
                "product_number":product_number,
                "product_name":product_name,
                "quantity":quantity
            };
            await api.post(`api/print/${type}`, data);
            enqueueSnackbar("已送出",{variant:'success'})
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar("送出失敗", { variant: 'error' });
        };
    };
    
    return (
        <Box sx={{ m: 'auto', width: { xs: "100%", sm: "100%", md: "90%" } }}>
            <CheckDataLog checkData={checkData} handleClose={() => { setCheckData(null) }} type={type?type:''} />
            <Box sx={sx}>
                <Typography variant={"h5"} sx={{ fontWeight: 'bold' }}>
                    {title}
                </Typography>
                <DataGrid
                    sx={{
                        fontSize: 16, bgcolor: 'white',
                        '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                            outline: 'none',
                        },
                    }}
                    density={'compact'}
                    rows={rows}
                    columns={displayPendingColumns(t, handleCheck, handleCheckDelete, handlePrint)}
                    showCellRightBorder
                    pageSize={15}
                    rowsPerPageOptions={[5]}
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
        </Box>
    );
};

export default OnPendingTable;


const displayPendingColumns = (
    t: TFunction,
    handleCheck: (orderId: string) => void,
    handleDelete: (orderId: string) => void,
    handlePrint:(data:any)=>void
): GridColDef[] => [
     { field: 'id', headerName: t("warehouse.main.no"), width: 90 ,align:'center',headerAlign:'center'},
  
  {
    field: 'date',
      headerName: t("warehouse.main.date"),
    align: 'right',
     headerAlign:'right',
    width: 200,
  },

  {
        field: 'type',
        headerName: t("warehouse.main.type"),
        align: 'right',
        headerAlign: 'right',
      width: 120,
        valueGetter:(params:GridValueGetterParams)=>t(`type.${params.row.type}`)  
        },

{
        field: 'order_id',
        headerName: t("warehouse.main.order"),
        align: 'right',
        headerAlign:'right',
        width: 250,
    },
    {
        field: 'employee_id',
        headerName: t("operator.pickingTable.filler"),
        align: 'right',
        headerAlign:'right',
        width: 120,

    },
    {
        field: 'print',
        headerName: t("warehouse.main.print"),
        align: 'right',
        headerAlign:'right',
        width: 120,
        hide:!Boolean(handlePrint),
        renderCell: (params: GridValueGetterParams) => {
            const data = params.row;
            return (
                <IconButton onClick={()=> handlePrint && handlePrint(data)} color="primary">
                    <LocalPrintshopOutlinedIcon />
                </IconButton>
            );
        }
    },
 
    {
        field: 'action',
        headerName: t("warehouse.main.action"),
        align: 'right',
        headerAlign: 'right',
        width:150,
        renderCell: (params: GridValueGetterParams) => {
            const { order_id} = params.row;
            return (
                <div>
                     <IconButton color="success" onClick = {()=> handleCheck(order_id)}>
                        <OpenInNewOutlinedIcon/>
                    </IconButton>
                    /
                    <IconButton color="error" onClick={()=>handleDelete(order_id)}>
                        <DeleteOutlineOutlinedIcon />
                    </IconButton>
                </div>
            )
        }
    }
];