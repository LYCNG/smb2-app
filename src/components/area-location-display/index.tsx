import { Box, Button, Checkbox,  Grid, IconButton,  Paper, Select, SelectChangeEvent, Stack, Typography, useMediaQuery, useTheme } from '@mui/material';
import { styled } from '@mui/material/styles';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useMemo, useState } from 'react'
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import moment from "moment";
import DialogTitle from '@mui/material/DialogTitle';
import Dialog from '@mui/material/Dialog';
import DialogContent from '@mui/material/DialogContent';
import DialogActions from '@mui/material/DialogActions';

import LaunchIcon from '@mui/icons-material/Launch';
import CloseIcon from '@mui/icons-material/Close';
import { useTranslation } from 'react-i18next';
import api from '../../api';

interface OrderType { 
    name: string;
    date: string;
    location: string;
    product: string;
    count: number;
};
const initOrderState: OrderType = {
    name: "",
    date: moment(Date.now()).format("YYYY-MM-DD"),
    location: "",
    product: "",
    count: 0
};
export interface PlaceType {  name: string, remark: string };
interface LocationType{
  num: string;
  place: PlaceType [];
};
export interface AreaType { 
  area: string;
  location: LocationType[];
};

const LocationData:AreaType[] = [
  {
    area: 'K',
    location: [
      {
        num: "01",
        place: [
          { name: "k01-36", remark: "" },
          { name: "k01-38", remark: "" },
          { name: "k01-42", remark: "" },
          {name:"k01-45",remark:""}
        ]
      },
      {
        num: "02",
        place: [
          { name: "k02-06", remark: "" },
          {name:"k02-12",remark:""}
        ]
      },
      {
        num: "03",
        place: [
          { name: "k03-16", remark: "" },
          {name:"k03-25",remark:""}
        ]
      },
    ]
  },
  {
    area: 'L',
    location: [
      {
        num: "01",
        place: [
        
        ]
      },
      {
        num: "02",
        place: [
          { name: "L02-06", remark: "" },
          {name:"L02-12",remark:""}
        ]
      },
      {
        num: "03",
        place: [
          { name: "L03-16", remark: "" },
          {name:"L03-25",remark:""}
        ]
      },
    ]
  },
];
const AreaDialog = (
  props: {
    open: boolean,
    handleClose: () => void,
    area: string,
    location: LocationType,
    handleSelectArea?:(area:PlaceType)=>void
  }
) => {
    const { t } = useTranslation();
  const { enqueueSnackbar } = useSnackbar();
  const { open, handleClose, area, location,handleSelectArea } = props;
  const { num } = location;
  const [placeCheck, setPlaceCheck] = useState<{ name: string, remark:string,check:boolean}[]>([]);
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => { 
    event.stopPropagation();
    const targetIndex = placeCheck.findIndex(ele => event.target.name === ele.name);
    const updateData = placeCheck.map(item => { return { ...item, check: false } });
    updateData.splice(targetIndex, 1, { ...placeCheck[targetIndex], check: event.target.checked });
    setPlaceCheck(updateData);
  };
  const handleFinish = () => { 
    const target = placeCheck.filter(ele => ele.check)[0];
    if (target && handleSelectArea) {
      handleSelectArea({
        name:target.name,
        remark:target.remark
      });
      handleClose();
    } else {
      enqueueSnackbar('please select one.', { variant: 'error' });
    };
  };
  useEffect(() => { 
    const { place } = location;
    setPlaceCheck(place.map(data => { return { ...data, check: false } }));
  }, [location]);

  //area - num from AreaType and LocationType
  return (
    <Dialog onClose={handleClose} open={open}  fullScreen={fullScreen}>
      <DialogTitle id="dialog-title" sx={{display:'flex',flexDirection:'row',justifyContent:'space-between'}}>
        {`${area}  ${num}`}
        <IconButton onClick={handleClose}>
            <CloseIcon />
        </IconButton>
      </DialogTitle>
      <DialogContent>
        <TableContainer component={Paper}>
          <Table sx={{ minWidth: 400 }} aria-label="simple table" size="small">
             <TableHead>
              <TableRow>
                              <TableCell style={{ width: "10%" }}>{t("warehouse.manage.choice") }</TableCell>
                <TableCell align="center" style={{width:"40%"}}>{t("warehouse.purchase.remainRack") }</TableCell>
                <TableCell align="center" style={{width:"50%"}}>{t("warehouse.manage.mark") }</TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {placeCheck.map(p =>
                <TableRow sx={{ '&:last-child td, &:last-child th': { border: 0 } }} key={p.name}>
                  <TableCell align="center">
                    <Checkbox checked={p.check} onChange={handleChange} name={p.name} />
                  </TableCell>
                  <TableCell align="center">{p.name}</TableCell>
                  <TableCell align="center">{p.remark}</TableCell>
                </TableRow>)}
            </TableBody>
          </Table>
         </TableContainer>
      </DialogContent>
      <DialogActions sx={{ alignItems:'center'}}>
          <Button onClick={handleFinish}>{t("warehouse.ok") }</Button>
        </DialogActions>
    </Dialog>
  )
 };


const StackBox = (
  props: {
    data: {
      num: string, place: { name: string, remark: string }[], check: boolean
    }[],
    handleChange: (data:LocationType) => void,
    handleSelectArea?: (area:PlaceType) => void;
  }
) => {
  const { t } = useTranslation();
  const { data, handleChange } = props;

  const getColor = (count: number) => {
    if (count > 3) return "rgb(81, 255, 42 ,0.8)";
    if (count > 0) return "rgb(64, 165, 255 ,0.8)";
    if (!count) return "rgb(151, 151, 151 ,0.8)";
  };

  return (
    <Stack
      direction="column" alignItems={{ xs: 'center', md: "flex-start" }} justifyContent="center" spacing={{ xs: 1, sm: 2 }}
    >
      {data.map(item =>
        <Box sx={{ width:180,pb:1,display: 'flex', alignItems: 'center', maxHeight: "60px",borderBottom:4,borderColor: "#DC7633" }} key={item.num}>
          {/* <FormControlLabel
            label={<Typography variant={"h6"}  component="h3">{item.num}</Typography>}
            control={<Checkbox checked={item.check} onChange={handleChange} name={item.num} />}
          /> */}
          <Button startIcon={<LaunchIcon />} variant="outlined" size="small" onClick={() => handleChange(item)}>
            <Typography variant={"h6"} component="h3">{item.num}</Typography>
          </Button>
          <Typography variant={"h6"} component="h3" sx={{border:3,borderColor:"#DC7633",
            borderRadius: 1, ml: 1, p: 0.5, height: 30, width: 90, boxShadow: 10, bgcolor: getColor(item.place.length)
          }}>
            {t("warehouse.purchase.remain") +": "+ item.place.length}
          </Typography>
        </Box>
      )}
    </Stack>
  );
 };

const initLocation: LocationType = {
    num: "",
    place: [{ name: "", remark:""}]
};
const Location = (
  props: {
    data: AreaType,
    handleSelectArea?: (area:PlaceType) => void;
  }
) => {
    const { t } = useTranslation();
  const { data,handleSelectArea } = props;
  const { area, location } = data;
  const [open, setOpen] = useState<boolean>(false);
  const [place, setPlace] = useState<LocationType>(initLocation);
  const [checkData, setCheckData] = useState(location.map(data => { return { ...data, check: false } }));

  const handleClose = () => setOpen(false);

  const handleShowArea = (data:LocationType) => { 
    const { num, place } = data;
    setPlace({ num: num, place: place });
    setOpen(true);
  };


  return (
     <Paper sx={{textAlign:'center',p:2,bgcolor:"rgb(0,0,0,0)",position:'relative'}} elevation={0}>
      <Typography variant={"h4"} component="h3" sx={{
        border: 4, width: 200, margin: 'auto', borderColor: "#DC7633", borderBottom: 0
      }}>
        {t("warehouse.purchase.area")}: {area}
      </Typography>
      <Grid container direction="row" rowSpacing={3} sx={{
        m: 'auto', p: 2, border: 4, bgcolor: '#FFD5C8', width: 450, borderColor: "#DC7633"
      }}>
        <Grid item xs={12} sm={12} md={6}>
          <StackBox data={checkData.slice(0, 3)} handleChange={handleShowArea} handleSelectArea={handleSelectArea}/>
        </Grid>
        <Grid item xs={12} sm={12} md={6}>
           <StackBox data={checkData.slice(3)} handleChange={handleShowArea} handleSelectArea={handleSelectArea}/>
        </Grid>
      </Grid>
      
      <AreaDialog open={open} handleClose={handleClose} area={area} location={place} handleSelectArea={handleSelectArea}/>
    </Paper>
  )
};
 
const AreaLocationDisplay = (
    props: {
        handleSelect?:(area:PlaceType)=>void
    }
) => {
    const {handleSelect } = props;
    const [locationData, setLocationData] = useState<AreaType[]>([]);   

     const handleSelectArea = (area: PlaceType) => { 
        
         if (handleSelect) {
             handleSelect(area);
         }
  };
  
  const getEmptyAreaAsync = async () => { 
    try {
      const res = await api.get("api/storage/empty");
       setLocationData(res.data);
    } catch (err: any) {
      console.log(err.message);
      alert(err.message);
    }
  };
    useEffect(() => {
       getEmptyAreaAsync()
    }, []);
    
    return (
        <Stack direction={{ xs: 'column', sm: 'row' }} spacing={2} sx={{ border: 3,borderRadius:1, width: "100%", overflow: 'auto',bgcolor:'#7FB3D5' }}>
            {locationData.map(data =>
              <Location data={data} key={data.area} handleSelectArea={handleSelectArea}/>
                )}
          
          </Stack>
    )
}

export default AreaLocationDisplay;

