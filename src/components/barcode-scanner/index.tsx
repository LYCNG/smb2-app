import CloseIcon from '@mui/icons-material/Close';
import {
  Button,
  Dialog, DialogActions, DialogContent,
  DialogTitle,
  Grow, IconButton, Stack, TextField, useMediaQuery,
  useTheme
} from '@mui/material';
import { TransitionProps } from '@mui/material/transitions';
import React, { useEffect } from "react";

import { Html5Qrcode } from "html5-qrcode";
import { useState } from 'react';

const Transition = React.forwardRef(function Transition(
  props: TransitionProps & {
    children: React.ReactElement<any, any>;
  },
  ref: React.Ref<unknown>,
) {
  return <Grow  ref={ref} {...props} />;
});

const brConfig = { fps: 10, qrbox: { width: 400, height: 200 } };

let html5QrCode: any;

function BarcodeCamera(props:{
    showCamera: boolean; handleClose: () => void;
    onResult?: (text:string)=> void; barcode?: string; checked?: boolean;
}) {
    const { showCamera, barcode, checked, handleClose ,onResult} = props;
    const theme = useTheme();
    const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const [onCode, setOnCode] = useState<string>('');

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>)=> setOnCode(event.target.value);
  const qrCodeSuccessCallback = (decodedText: any, decodedResult: any) => setOnCode(decodedText as string);
  
  const handleFinish = () => {
    onResult && onResult(onCode);
    handleCloseAll();
  };

    const handleStop = () => {
        try {
            html5QrCode
                .stop()
                .then((res:any) => {
                    html5QrCode.clear();
                })
                .catch((err:any) => {
                    alert(err.message);
                });   
           
        } catch (err) {
            alert(err);
        };
    };

  
  const handleCloseAll = () => {
    handleStop();
    handleClose();
    return;
  };
    

    useEffect(() => { 
        if (showCamera) {
            html5QrCode = new Html5Qrcode("reader");
            html5QrCode.start(
            { facingMode: "environment" },
             brConfig,
            qrCodeSuccessCallback
        );
        };
    }, [showCamera]);


    return (
        <Dialog
            open={showCamera}
            maxWidth={"sm"}
            fullScreen={fullScreen}
            fullWidth={true}
            TransitionComponent={Transition}
            keepMounted
            onClose={handleCloseAll}
            aria-describedby="alert-dialog-slide-description"
        >
            <DialogTitle sx={{ display: 'flex', justifyContent: 'space-between', bgcolor: '', fontSize: 25, fontWeight: 'bold' }}>
              {"Barcode Checker"}
                <IconButton onClick={handleCloseAll}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
           <DialogContent>
            <Stack  spacing={2}>
                <TextField id="m-code-input" placeholder='手動輸入' value={onCode} variant="outlined" size="small" onChange={handleChange}/>
                <div id="reader" style={{width:'100%'}}></div>
            </Stack>
            </DialogContent>
        <DialogActions>
          <Button onClick={handleFinish} color='success'>
              ok
          </Button>
        </DialogActions>

        
        </Dialog>
    );
};

export default BarcodeCamera;


const Scanner = () => {
  useEffect(() => {
    html5QrCode = new Html5Qrcode("reader");
  }, []);

  const handleClickAdvanced = () => {
    const qrCodeSuccessCallback = (decodedText:string, decodedResult:string) => {
      alert(decodedText);
      handleStop();
    };
    html5QrCode.start(
      { facingMode: "environment" },
     brConfig,
      qrCodeSuccessCallback
    );
  };

  const handleStop = () => {
    try {
      html5QrCode
        .stop()
        .then((res:any) => {
          html5QrCode.clear();
        })
        .catch((err:any) => {
          console.log(err.message);
        });
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <div style={{ position:"relative"}}>
      <div id="reader" style={{width:'100%'}} />
      <button onClick={() => handleClickAdvanced()}>
        click pro 
      </button>
      <button onClick={() => handleStop()}>stop pro</button>
    </div>
  );
};
