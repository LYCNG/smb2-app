
import styled from "@emotion/styled";
import AddCircleOutlineIcon from '@mui/icons-material/AddCircleOutline';
import DeleteIcon from '@mui/icons-material/Delete';
import RestartAltIcon from '@mui/icons-material/RestartAlt';
import SaveAltIcon from '@mui/icons-material/SaveAlt';
import TabContext from '@mui/lab/TabContext';
import TabList from '@mui/lab/TabList';
import TabPanel from '@mui/lab/TabPanel';
import { Button, Divider, MenuItem, Stack, TextField } from '@mui/material';
import Autocomplete from '@mui/material/Autocomplete';
import Box from '@mui/material/Box';
import Select, { SelectChangeEvent } from '@mui/material/Select';
import Tab from '@mui/material/Tab';
import { useSnackbar } from 'notistack';
import * as React from 'react';
import { useContext, useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import api from "../../api";
import { AuthContext } from '../../provider/authProvider/index';
import { CheckListType, CheckTestType } from "../check-data-modal";

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: 300
    },
  },
};

interface ProductType{
    product_name: string;
    product_number: string;
};

const Item = styled.div`
    display:flex;
    align-items:center;
    font-weight:bold;
`;

const inputTitleStyle = { color: "black", width: 65, fontSize: 20 };

const containsText = (text:string, searchText:string) =>
    text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

function EditPage(props: {
    checkData:CheckListType
}) {
    const { checkData } = props;
    const { t } = useTranslation();
    const { enqueueSnackbar } = useSnackbar();
    const {auth} = useContext(AuthContext);
    const [currentData, setCurrentData]
        = useState<CheckTestType[]>([]);
    const [products, setProducts]
        = useState<ProductType[]>([]);//產品名稱集
    const [reasons, setReasons]
        = useState<string[]>([]);//for reason selectProps
    const [statuses, setStatuses]
        = useState<string[]>([]);//for status selectProps
    const [value, setValue] = useState('0');

    const handleChange = (event: React.SyntheticEvent, newValue: string) => {
        setValue(newValue);
    };
    
    const updateData = (index:number,update:CheckTestType) => {
        const updated = [...currentData];
        updated.splice(index, 1, update);
        setCurrentData([...updated]);
    };

    const handleSelect = (
        value:ProductType|null,
        key:  number
    ) => { 
        const data = currentData[key];
        const productData = {
            product_name:value?value.product_name:'',
            product_number:value?value.product_number:''
        };
        const newData:CheckTestType = {
            ...data,
            ...productData
        };
        return updateData(key,newData)
    };

    const handleChangeCount = (
        event: React.ChangeEvent<HTMLInputElement | HTMLTextAreaElement>,
        key:number
    ) => {
        let value = parseInt(event.target.value);
        const data = currentData[key];
         const newData:CheckTestType = {
            ...data,
            quantity:value
        };
        return updateData(key,newData)
    };
    
    const handleSelectReason = (
        event: SelectChangeEvent<string>,
        key:  number
    ) => { 
        const value = event.target.value;
         const data = currentData[key];
        const newData:CheckTestType = {
            ...data,
            reason:value
        };
        return updateData(key,newData)
    };
    const handleSelectStatus = (
        event: SelectChangeEvent<string>,
        key:  number
    ) => { 
        const value = event.target.value;
        const data = currentData[key];
        const newData:CheckTestType = {
            ...data,
            status:value
        };
        return updateData(key,newData)
    };

    const handleAddNew = () => { 
        const checkType = Boolean(currentData[0].reason && currentData[0].status);
        const newData: CheckTestType = {
            machinecode: currentData[0].machinecode,
            number: currentData.length,
            product_name: "",
            product_number:"",
            quantity: 0,
            status: checkType ? "":null,
            reason: checkType ?"":null
        };
        setCurrentData([...currentData,newData]);
    };
    const handleRemove = () => { 
        const index = parseInt(value);
        if (index === 0) {
            enqueueSnackbar("如果要全部刪除的話，請在首頁刪除。",{variant:'warning'})
            return;
        };
        const updated = [...currentData];
        updated.splice(index, 1);
        const newDataset = updated.map((data, index) => {
            return {
                ...data,
                number: index
            }
        });
        setCurrentData(newDataset);
        setValue("0")
    };
    const handleReset = () => {
        setCurrentData(checkData.data);
        setValue('0');
    };
    
    const handleSave = async () => { 
        const checkType = Boolean(currentData[0].reason && currentData[0].status);
        const checkSave = window.confirm("確定更新此單號?");
        const office = auth.office === 'WAREHOUSE' ? "management" : "operate";
        if (!checkSave) return;
        try { 
            await api.put(`api/operate/orders/wait/${checkType?'in':'out'}`,{
                "orderid": checkData.orderId,
                "orderitems": currentData.map((data,index)=> {
                    return {
                        ...data,
                        "item_number": index,
                    };
                })
            });
            enqueueSnackbar("更新成功。", { variant: 'success' });
        } catch (err: any) { 
            enqueueSnackbar("更新失敗。", { variant: 'error' });
        };
    };
    /**獲取產品項目、原因列表、狀態列表 */
    const getProductByAsync = async () => {
        try {
            const product_res = await api.get("/api/product/infos");
            const reason_res = await api.get('api/dropdown/items/inreason');
            const status_res = await api.get('api/dropdown/items/status');
            const productData = await product_res.data;
            const productName = Object.keys(productData);
            const result = productName.map(ele => {
                return {
                    product_name: ele,
                    product_number: productData[ele]
                };
            });
            setProducts([...result]);
            setReasons([...reason_res.data]);
            setStatuses(status_res.data);
        } catch (err: any) {
            console.log(err.message);
        }
    };

    useEffect(() => {
        getProductByAsync();
        setCurrentData(checkData.data);
   
    }, [checkData]);

    return (
        <Box sx={{ maxWidth: { xs: 320, sm: 480 }, bgcolor: 'background.paper' }}>
            
            <TabContext value={value}>
                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                    <TabList onChange={handleChange} aria-label="lab API tabs example" centered>
                        {currentData.map((_, index) =>
                            <Tab label={`No.${index + 1}`} key={index} value={index.toString()} />)}
                    </TabList>
                </Box>
                {currentData.map((data,index) =>
                    <TabPanel value={index.toString()} key={index}>
                        <Stack component="form"  >
                            <Item style={{justifyContent:'space-around'}}>
                                <h3>{`項目: ${data.number+1}`}</h3>
                                <div> </div>
                                <h3>{'機台碼: ' + data.machinecode}</h3>
                            </Item>
                            
                            <Item id="form-product-select">
                                <p style={{ color: "black", width: 80, fontSize: 20 }}>
                                    {t("operator.pickList.name")}:
                                </p>
                                <Autocomplete
                                    size='small'
                                    value={{
                                        product_name: data.product_name,
                                        product_number: data.product_number
                                    }}
                                    isOptionEqualToValue={(option, value) =>
                                        typeof option === typeof value
                                        }
                                    disablePortal
                                    onChange={(e,value)=>handleSelect(value,index)}
                                    id="combo-box-demo"
                                    options={products}
                                    getOptionLabel={(option) => option.product_name}
                                    sx={{ width: 350 }}
                                    renderInput={(params) => <TextField {...params} label="Product Name" />}
                                />
                             </Item>
                            
                       
                             <Item id="form-count-input">
                                <p style={{ color: "black", width: 65, fontSize: 20 }}>
                                    {t("operator.treasuryList.count")}:
                                </p>
                                <TextField
                                    sx={{ ml:2,width: { xs: 100, sm: 100, md: 100 } }}
                                    size="small"
                                    type="number"
                                    value={data.quantity}
                                    InputProps={{ inputProps: { min: 0, max: 100000 } }}
                                    onChange={(e)=>handleChangeCount(e,index)}
                                    inputProps={{ style: { textAlign: 'center' } }}
                                />
                            </Item> 
                            {data.reason!==null?<Item>
                                <p style={inputTitleStyle }> {t("operator.treasuryList.reason")}:</p>
                                <Select
                                    size="small"
                                    labelId="reason-select-label"
                                    id="reason-select-label"
                                    sx={{ ml:2,width: { xs: 150, sm: 180, md: 150,lg:150 },height:40 }}
                                    value={data.reason}
                                    onChange={(e)=>handleSelectReason(e,index)}
                                >
                                    <MenuItem value={""}>
                                        {"None"}
                                    </MenuItem>
                                    {reasons.map((reason,index) =>
                                        <MenuItem value={reason} key={index}>
                                            {reason}
                                        </MenuItem>
                                        )}
                                </Select>
                            </Item>:null}
                            {data.status !==null? (
                                <Item>
                                    <p style={inputTitleStyle}> {t("operator.treasuryList.status")}:</p>
                                    <Select
                                        sx={{ ml:2,width: { xs: 150, sm: 180, md: 150,lg:100 } ,height:40}}
                                        size="small"
                                        labelId="status-select-label"
                                        id="status-select-label"
                                        value={data.status}
                                        onChange={(e)=>handleSelectStatus(e,index)}
                                    >
                                        <MenuItem value={""}>
                                            {"None"}
                                        </MenuItem>
                                        {statuses.map((status,index) =>
                                            <MenuItem value={status} key={index}>{status}</MenuItem>
                                        )}
                                    </Select>
                                </Item>
                            ):null}
                            
                        </Stack>
                    </TabPanel>
                )}
            </TabContext>
            <Divider />
            <Stack direction={'row'} justifyContent='space-around' sx={{mt:2}}>
                <Button color="warning" variant="outlined" size="small" startIcon={<AddCircleOutlineIcon />} onClick={handleAddNew}>
                    add new
                </Button>
                <Button color="error" variant="outlined" size="small" onClick={handleRemove} startIcon={<DeleteIcon />}>
                    remove
                </Button>
                <Button color="error" variant="outlined" size="small" startIcon={<RestartAltIcon />} onClick={handleReset}>
                    reset
                </Button>
            </Stack>
            <Box sx={{m:'auto',width:60,mt:2}}>
                <Button color="success" variant="outlined" size="small" startIcon={<SaveAltIcon />} onClick={handleSave} >
                    save
                </Button>
            </Box>
            
        </Box>
    );
};

export default EditPage;
