

import CloseIcon from '@mui/icons-material/Close';
import EditIcon from '@mui/icons-material/Edit';
import HighlightOffIcon from '@mui/icons-material/HighlightOff';
import { Box, Button, Dialog, DialogActions, DialogContent, DialogTitle, IconButton, useMediaQuery, useTheme } from '@mui/material';
import Divider from '@mui/material/Divider';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';
import { useSnackbar } from 'notistack';
import { useLayoutEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { Navigation, Pagination, Scrollbar } from 'swiper';
import 'swiper/css';
import 'swiper/css/navigation';
import 'swiper/css/pagination';
import 'swiper/css/scrollbar';
import { Swiper, SwiperSlide } from 'swiper/react';
import api from '../../api';
import EditPage from '../edit-data-page';

export interface CheckTestType{
    machinecode: string
    number: number
    product_name: string
    product_number:string
    quantity: number,
    status: string | null;
    reason: string | null;
}
export interface CheckListType{
    orderId: string;
    data: CheckTestType[];
};


/**
 * * 查看頁面
 */
const CheckDataLog = (props: {
    checkData: CheckListType | null;
    handleClose: () => void;
    type?: string|null;//"merge" | "change" | "purchase" | "in"|
}) => {
    
    const { t } = useTranslation();
    const { checkData, handleClose, type } = props;
    const { enqueueSnackbar } = useSnackbar();
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down("sm"));

    const [manageData, setManageData]
        = useState<ManageCheckType[]>([]);
    
    const [onEdit, setOnEdit]
        = useState<boolean>(false);

    /**如果為合併類，重新撈取資料並設定manageData */
    const getDataByType = async () => { 
        try {
            const res = await api.get("api/management/orders/" + checkData?.orderId);
            const data = res.data;
            setManageData(data);
            return;
        } catch (err: any) {
            console.log(err.message);
            enqueueSnackbar(err.message, { variant: "error" });
            return;
        }
    };
    const handleLeave = () => {
        setManageData([]);
        handleClose();
          setOnEdit(false)
        return;
    };
    const handleEdit = () => { 
        setOnEdit(prev=>!prev);
    };

    useLayoutEffect(() => {
        if (type) {
            getDataByType();
        };
    }, [checkData]);
    
    return (
        <Dialog
            fullScreen={small}
            open={Boolean(checkData)}
            onClose={handleLeave }
            id="check-logout-modal"
            maxWidth={"sm"}
            fullWidth={true}
        >
            <DialogTitle id="alert-dialog-title"
                sx={{ bgcolor: '#34495E', color: 'white', fontWeight: 'bold', display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}
            >
                單號: {checkData?.orderId} {type && " - " + t(`warehouse.main.${type}`)}
                <IconButton aria-label="close" onClick={handleLeave}>
                    <CloseIcon />
                </IconButton>
            </DialogTitle>
            <DialogContent sx={{ m: 'auto' }}>
                {checkData && onEdit ?
                    <EditPage checkData={checkData} /> :
                    (type ? <MergeContent checkData={manageData} />
                    : ((!type && checkData) ?
                        <CheckListContent checkData={checkData} /> : null)
                )}
                
            </DialogContent>
            <DialogActions>
                {type ? null :
                    <Button onClick={handleEdit} autoFocus variant='contained'
                        color={onEdit ? 'error' : 'primary'}
                        sx={{ m: 'auto', mb: 2, fontWeight: 'bold' }}
                        startIcon={onEdit?<HighlightOffIcon/>:<EditIcon />}
                    >
                        {onEdit?'Cancel':'Go Edit'}
                    </Button>
                }
            </DialogActions>
        </Dialog>
    );
};

export default CheckDataLog;

export interface ManageCheckType{
    action: string;
    flag_number: string;
    item_number: number;
    location: string;
    product_name: string;
    quantity: number;
}

const textWidth = 200;

const MergeContent = (
    props: {
        checkData: ManageCheckType[]
    }
) => {
    
    const { checkData } = props;

    const getActionData = (action:"in"|"out", index: number) => {
        return checkData.filter(ele => ele.action === action)[index];
    };

    return (
        <Box sx={{  maxWidth: 450,mt: 3 }}>
            <Swiper
                className="mySwiper"
                modules={[Pagination, Scrollbar, Navigation]}
                grabCursor
                // navigation
                spaceBetween={50}
                // effect="fade"
                // keyboard={{ enabled: true }}
                pagination={{ clickable: true, type: "fraction" }}
                // parallax
                scrollbar={{ draggable: true }}
      
                style={{ height: 300, width: '100%' }}
            >
                {[0].map((_, index) =>
                     <SwiperSlide key={index}>
                    <List sx={{ width: { xs:360,sm:400,md:500} }}>
                        <ListItem alignItems="flex-start" >
                            <ListItemText primary="機台ID:" sx={{ width: 100 }} />
                            <ListItemText primary={getActionData("in",index)?.action} sx={{ color:'green',width: textWidth,textAlign:"center"}}/>
                            <Divider orientation="vertical" flexItem />
                            <ListItemText primary={getActionData("out",index)?.action} sx={{ color:'red',width: textWidth,textAlign:"center"}}/>
                        </ListItem>
                        <Divider />
                        <ListItem alignItems="flex-start" >
                            <ListItemText primary="旗號卡:" sx={{ width: 100 }} />
                            <ListItemText primary={getActionData("in",index)?.flag_number} sx={{ color:'green',width: textWidth,textAlign:"center" }}/>
                            <Divider orientation="vertical" flexItem />
                            <ListItemText primary={getActionData("out",index)?.flag_number} sx={{ color:'red',width: textWidth,textAlign:"center" }}/>
                        </ListItem>
                        <Divider />
                        <ListItem alignItems="flex-start" >
                            <ListItemText primary="架位:" sx={{ width: 100 }} />
                            <ListItemText primary={getActionData("in",index)?.location} sx={{ color:'green',width: textWidth,textAlign:"center" }}/>
                            <Divider orientation="vertical" flexItem />
                            <ListItemText primary={getActionData("out",index)?.location} sx={{ color:'red',width: textWidth,textAlign:"center" }}/>
                        </ListItem>
                        <Divider />
                        <ListItem alignItems="flex-start" >
                            <ListItemText primary="產品名稱:" sx={{ width: 100 }} />
                            <ListItemText primary={getActionData("in",index)?.product_name} sx={{ color:'green',width: textWidth,textAlign:"center" }}/>
                            <Divider orientation="vertical" flexItem />
                            <ListItemText primary={getActionData("out",index)?.product_name} sx={{ color:'red',width: textWidth,textAlign:"center" }}/>
                        </ListItem>
                        <Divider />
                        <ListItem alignItems="flex-start" >
                            <ListItemText primary="數量:" sx={{ width: 100 }} />
                            <ListItemText primary={getActionData("in",index)?.quantity} sx={{ color:'green',width: textWidth,textAlign:"center" }}/>
                            <Divider orientation="vertical" flexItem />
                            <ListItemText primary={getActionData("out",index)?.quantity} sx={{ color:'red',width: textWidth,textAlign:"center" }}/>
                        </ListItem>
                        <Divider />
                    </List>
                </SwiperSlide>
                )}
                
            </Swiper>
        </Box>
    );
};

/**
 * * 查看頁面 <領料> <繳庫>
 */
export const CheckListContent = (props: {
    checkData: CheckListType 
}) => {
    const { checkData} = props;
    const { t } = useTranslation();

    return (
        <Box sx={{ maxWidth: 350, mt: 3 }}>
            <Swiper
                className="mySwiper"
                modules={[Pagination, Scrollbar, Navigation]}
                grabCursor
                // navigation
                spaceBetween={50}
                // effect="fade"
                // keyboard={{ enabled: true }}
                pagination={{ clickable: true, type: "fraction" }}
                // parallax
                scrollbar={{ draggable: true }}
      
                style={{ height: 320, width: '100%' }}
            >
                {checkData.data.map((data, index) =>
                    <SwiperSlide key={index}>
                        <List sx={{ pt: 2 }}>
                            <ListItem alignItems="flex-start" >
                                <ListItemText primary="機台ID:" sx={{ width: 100 }} />
                                <ListItemText primary={data.machinecode} />
                            </ListItem>
                            <Divider sx={{ bgcolor: '#34495E' }} />
                            <ListItem alignItems="flex-start" >
                                <ListItemText primary="品名:" sx={{ width: 100 }} />
                                <ListItemText primary={data.product_name} />
                            </ListItem>
                            <Divider sx={{ bgcolor: '#34495E' }} />
                            <ListItem alignItems="flex-start" >
                                <ListItemText primary="品號:" sx={{ width: 100 }} />
                                <ListItemText primary={data.product_number} />
                            </ListItem>
                            <Divider sx={{ bgcolor: '#34495E' }} />
                            <ListItem alignItems="flex-start" >
                                <ListItemText primary="數量:" sx={{ width: 100 }} />
                                <ListItemText primary={data.quantity} />
                            </ListItem>
                            <Divider sx={{ bgcolor: '#34495E' }} />
                            <ListItem alignItems="flex-start" >
                                <ListItemText primary="狀態:" sx={{ width: 100 }} />
                                <ListItemText primary={data.status} />
                            </ListItem>
                            <Divider sx={{ bgcolor: '#34495E' }} />
                        </List>
                    </SwiperSlide>
                )}
            </Swiper>
        </Box>
    )
};