import { Box, Button, Divider, FormControl, Grid, IconButton, InputAdornment, ListSubheader, MenuItem, Select, SelectChangeEvent, TextField, Toolbar, Typography, useMediaQuery, useTheme } from '@mui/material';

import ClearSharpIcon from '@mui/icons-material/ClearSharp';
import { DataGrid, GridColDef, GridValueGetterParams } from '@mui/x-data-grid';
import moment from 'moment';
import { useSnackbar } from 'notistack';
import React, { useContext, useEffect, useMemo, useState } from 'react';
import { TFunction, useTranslation } from 'react-i18next';
import api from '../../../api';
// import { columns} from './column';
import styled from "@emotion/styled";
import AddCircleSharpIcon from '@mui/icons-material/AddCircleSharp';
import SearchIcon from "@mui/icons-material/Search";
import SendRoundedIcon from '@mui/icons-material/SendRounded';
import Stack from '@mui/material/Stack';
import { AuthContext } from '../../../provider/authProvider';
import BarcodeCamera from '../../barcode-scanner';

type StatusType = "qualified"|"defective"|"scrapped"|"none";
type ReasonType = "injure" | "superfluous" | "materialCome" | "none";


const Item = styled.div`
    display:flex;
    align-items:center;
    font-weight:bold;
`;
const inputTitleStyle = { color:"black" ,width:65,fontSize:20};
//global setting---------------------------------------------
const containsText = (text:string, searchText:string) =>
    text.toLowerCase().indexOf(searchText.toLowerCase()) > -1;

const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: 300
    },
  },
};
//產品名稱接口
interface ProductType{
  product_name: string,
  product_number:string
}
//選擇產品接口
interface DatasetType { 
    id: number;
    product_name: string;
    product_number: string;
    quantity:number
};
//單號接口
interface OrderType { 
  filler: string; //填單者
  mCode: string; //機台碼
  date: string; //填單日期
  orderId: string;// 單號
};

const titleStyle = {
  fontSize: { xs: 16, sm: 18, md: 20 },
  width:160,
    textAlign: 'center',
  fontWeight:'bold'
};
const OrderForm = (props: { title: string, value: string,onClick?:()=>void }) =>
    <FormControl sx={{
        display: 'flex',
        flexDirection: { xs: 'row', sm: 'row' },
        justifyContent: { xs: 'center', sm: "start" },
        width: "90%",
        alignItems: 'center',
        fontSize:{xs:10,sm:12,md:16}
    }}>
        <Typography variant={"h6"} gutterBottom component="div" sx={titleStyle}>
            {props.title}:
        </Typography>
        <TextField size="small" value={props.value} onClick={props.onClick} />
    </FormControl>;
    
const initOrderState: OrderType = {
    filler: "",
    mCode: "",
    date: "",
    orderId: ""// 單號
};
const initTempSelect: DatasetType = {
    id:0,
    product_name: "",
    product_number: "",
    quantity:0,
};

function  OperatePickPage() {
    const { t } = useTranslation();
    const theme = useTheme();
    const small = useMediaQuery(theme.breakpoints.down('sm'));
    const { enqueueSnackbar } = useSnackbar();
    const {auth} = useContext(AuthContext);
    const { username, employee_id } = auth; 
    
    //state---------------------------------------
    const [showCamera, setShowCamera] = useState<boolean>(false);
    const [searchText, setSearchText] = useState<string>("");
    const [tempSelect, setTempSelect]
        = useState<DatasetType>(initTempSelect);//暫存資料
    const [order, setOrder]
        = useState<OrderType>(initOrderState);//單號內容
    const [dataset, setDataset]
        = useState<DatasetType[]>([]);//已選擇的資料
    
    const [products, setProducts]
        = useState<ProductType[]>([]);//產品名稱集
    
    //dataset function--------------------------
    /**關鍵字查詢產品名稱*/
    const displayedOptions = useMemo(
        () => products.filter((option) => containsText(option.product_name, searchText)),
        [searchText, products]
    );
    /**更改數量*/
    const handleChangeCount = (event: React.ChangeEvent<HTMLInputElement>) => {
        let value = parseInt(event.target.value);
        setTempSelect({ ...tempSelect, quantity: value });
    };
    /**刪除已選擇的項目*/
     const handleRemove = (rowIndex: number) => {
        const data = [...dataset].filter((ele, index) => index !== rowIndex);
         const update = data.map((data, index) => {
             return {
                ...data,
                id: index
            }
         })
         setDataset(update);
        return;
    };
    /**將選擇的產品項目暫存至tempState*/
    const handleSelect = (event: SelectChangeEvent) => {
        let value_number = event.target.value;
        const index = products.findIndex(row => row.product_number === value_number);
        const target = products[index];
        setTempSelect({
            ...tempSelect,
            product_name: target.product_name,
            product_number: target.product_number
        });
        return; 
    };

    /**新增至已選擇資料*/
    const handleAddDataset = () => { 
        const update = [...dataset, tempSelect].map((data, index) => {
            return {
                ...data,
                id: index
            }
        });
        setDataset(update);
    };
    const handleWebcam = () => { 
        setShowCamera(true);
    };
    /**回傳barcode辨識結果，儲存至order */
    const handleBarcodeResult = (text:string) => { 
        setOrder({ ...order, mCode: text });
        return;
    };
    //api---------------------------------------
    /**查詢當日單號*/
    const getOperateOrder = async () => { 
             const date = moment(Date.now()).format("YYYY-MM-DD");
        try {
            const res = await api.get(`api/operate/neworderid/out/${date}`);
            const orderId: string = res.data;
            setOrder({  mCode: "", orderId:  orderId, filler: username,date:date});
        } catch (err: any) {
            if(err.response.status===0)return enqueueSnackbar('server error', { variant:"error" })
            enqueueSnackbar('fetch order id failed', { variant: "error" });
            return;
        }
    };
    /**獲取產品項目、原因列表、狀態列表 */
    const getProductByAsync = async () => {
        try {
            const product_res = await api.get("/api/product/infos");
            const productData = await product_res.data;
            const productName = Object.keys(productData);
            const result = productName.map(ele => {
                return {
                    product_name: ele,
                    product_number: productData[ele]
                };
            });
            setProducts([...result]);
    
        } catch (err: any) {
            console.log(err.message);
        }
    };
    /**送單 */
     const submit = async () => {
      
            try {
            const { filler, mCode, date, orderId } = order;
            if (!filler || !mCode || !date || !orderId) {
                enqueueSnackbar('something miss.',{ variant: "warning" });
                return;
            };
            const items = dataset.map((row,index) => {
                return {
                    "item_number": index,
                    ...row
            }})
            const body = {
                "applicant":employee_id,
                "machinecode": mCode,
                "date": date,
                "orderid": orderId,
                "orderitems": items
            };
       
            const res = await api.post("/api/operate/orders/wait/out", body);
            const { ack} = res.data;
            if (ack) {
                 enqueueSnackbar('Success!',{ variant: "success" });
                return;
            }
        } catch (err: any) {
            console.log(err.message)
            enqueueSnackbar('Something Error!', { variant: "error" });
            return;
        };
     };
    //useEffect---------------------------------------
    useEffect(() => { 
        getOperateOrder();
        getProductByAsync();
    }, []);


    return (
        <Box sx={{ bgcolor: 'white', width: {md:'100vw',lg:'80vw'}, height: "100vh", mt: { xs: 6, sm: 6, md: 6, lg: 0 } }}>
            <Toolbar sx={{pt:2,pb:2,display:'flex',justifyContent:'space-between'}}>
                <Typography variant="h4" sx={{ fontWeight: 'bold' }}>
                      {t("operator.pickList.title")}
                </Typography >
                <Button
                    disabled={dataset.length === 0}
                    color="success"
                    size="large"
                    sx={{width:120}}
                    variant="contained" startIcon={<SendRoundedIcon />} onClick={() => submit()}>
                     {t("operator.treasuryList.send")}
                </Button>
            </Toolbar>
            <BarcodeCamera
                showCamera={showCamera}
                handleClose={() => setShowCamera(false)}
                onResult={handleBarcodeResult}
                />
            <Box sx={{mt:5}}>
                <Grid container rowSpacing={2} columnSpacing={{ xs: 1, sm: 2, md: 3 }} sx={{ m: 'auto' }}>
                    <Grid item xs={12} sm={12} md={6}>
                        <OrderForm title={t("operator.pickList.filler")} value={order.filler} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        {/* {t("operator.treasuryList.machineCode")}:webcam */}
                        <OrderForm title={t("operator.pickList.machineCode")}  value={order.mCode} onClick={()=>setShowCamera(true)} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        <OrderForm title={t("operator.pickList.fillDate")} value={order.date} />
                    </Grid>
                    <Grid item xs={12} sm={12} md={6}>
                        <OrderForm title={t("operator.pickList.pickNumber")} value={order.orderId} />
                    </Grid>
                </Grid>
                
            </Box>
            
            <Box sx={{mt:5,ml:2,p:2}}>
                <Stack
                    direction={{ xs:'column',sm: 'column', md: 'column',lg:'row' }}
                    spacing={{ xs: 1, sm: 2, md: 4 }}
                >
                <Item id="form-product-select">
                    <p style={{color:"black" ,width:80,fontSize:20}}>{t("operator.pickList.name")}:</p>
                    <FormControl size="small">
                        <Select
                            sx={{ width: { xs: 200, sm: 250, md: 250 } }}
                            labelId="product-name-select"
                            id="product-name-select"
                            value={tempSelect.product_number}
                            onChange={handleSelect}
                            onClose={() => setSearchText("")}
                            MenuProps={ MenuProps}
                        >
                            <ListSubheader>
                            <TextField
                                size="small"
                                sx={{ overflow: 'hidden' }}
                                autoFocus
                                placeholder="Search"
                                fullWidth
                                InputProps={{
                                startAdornment: (
                                    <InputAdornment position="start">
                                        <SearchIcon />
                                    </InputAdornment>
                                )
                                }}
                                onChange={(e) => setSearchText(e.target.value)}
                                onKeyDown={(e) => {
                                if (e.key !== "Escape") {
                                    e.stopPropagation();
                                }
                                }}
                            />
                            </ListSubheader>
                            {displayedOptions.map((row,index) =>
                                <MenuItem
                                    key={index}
                                    value={row.product_number}
                                >
                                    {row.product_name}
                                </MenuItem>
                            )}
                        </Select>
                    </FormControl>
                </Item>
                <Item id="form-count-input">
                     <p style={inputTitleStyle}> {t("operator.treasuryList.count")}:</p>
                    <TextField
                        sx={{ ml:2,width: { xs: 100, sm: 100, md: 100 } }}
                        size="small"
                        type="number"
                        value={tempSelect.quantity}
                        InputProps={{ inputProps: { min: 0, max: 100000 } }}
                        onChange={handleChangeCount}
                        inputProps={{ style: { textAlign: 'center' } }}
                    />
                </Item>
                    <Item style={{width:200}}>
                        <Button
                            color="success"
                            sx={{
                                border:1.5,
                                m: { md: 'auto', lg: 'auto' }, '&:hover': {
                                    border: 2,
                                    fontWeight:'bold'
                            } }}
                             variant="outlined" 
                            onClick={handleAddDataset}
                            startIcon={<AddCircleSharpIcon fontSize="large" />}
                        >
                            新增
                        </Button>
                    </Item>
                </Stack>
                <Divider sx={{mt:2}} />
            </Box>
            <Box
                sx={{
                    bgcolor:'white',
                    pt: 2, m: 'auto', height: 450, width: { xs: '100%', sm: '100%', md: '95%', lg: '95%' },
                    '& .MuiDataGrid-columnHeaders': { bgcolor: "#5D6D7E",color:'white',fontWeight:'bold' }
                }}
            >
             
                <DataGrid
                     sx={{ 
                            fontSize:18,
                            '&.MuiDataGrid-root .MuiDataGrid-cell:focus': {
                                outline: 'none',
                            },
                        }}
                    density={small?'compact':'comfortable'}
                    rows={dataset}
                    columns={ columns(handleRemove,t)}
                    pageSize={20}
                    rowsPerPageOptions={[5]}   
                    experimentalFeatures={{ newEditingApi: true }}
                />
            </Box>
        </Box>
    );
};

export default OperatePickPage;


//columns type---------------------------------------------------------
const columns = (
    handleRemove: (id: number) => void,
    t: TFunction<"translation", undefined>
): GridColDef[] => [
        { field: 'id', headerName: 'ID', width: 80,align: 'center', headerAlign: 'center'},
        {
            field: 'product_name', headerName: t("operator.treasuryList.name"), minWidth: 300, maxWidth: 450, align: 'left', headerAlign: 'center'
        },
        { field: 'product_number', headerName: t("operator.treasuryList.odd"), width: 200, align: 'center', headerAlign: 'center' },
        { field: 'quantity', headerName: t("operator.treasuryList.count"), width: 120, align: 'center', headerAlign: 'center' },
        {
            field: 'action', headerName: t("operator.treasuryList.remove"), width: 120, align: 'center', headerAlign: 'center',
            renderCell: (params: GridValueGetterParams) => {
                const { id } = params;
                return (
                    <IconButton color='error'
                        onClick={() => handleRemove(id as number)}
                    >
                        <ClearSharpIcon />
                    </IconButton >
                );
            }
        },
    ];