import { lazy } from 'react';

export const LazyLoad = (loadComp: Promise<any>) => lazy(()=>loadComp.then(module => ({ default: module.default })));
 
