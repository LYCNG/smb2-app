import { Box, useMediaQuery, useTheme } from '@mui/material';
import { AnimatePresence } from "framer-motion";
import { Suspense, useContext, useLayoutEffect } from 'react';
import { BrowserRouter } from 'react-router-dom';
import logo from "./assets/logobinch.png";
import { AuthContext, OfficeType } from './provider/authProvider';
import WareProvider from './provider/wareProvider';
import { AuthRouter, OpRouter, UnAuthRouter, WarehouseRouter } from './router';
import DrawerSide from './ui/drawer';

export const imageSrc = logo;

function App() {

  const theme = useTheme();
  const small = useMediaQuery(theme.breakpoints.down('md'));
  const authContext = useContext(AuthContext);
  const { auth,checkAuth } = authContext;
  const { isAuth,office } = auth;

  const handleRouter = (office:OfficeType) => { 
    switch (office) {
      case 'ADMIN':
        return <AuthRouter />;
      case 'OPERATOR':
        return <OpRouter />;
      case 'WAREHOUSE':
        return (
          <WareProvider>
            <WarehouseRouter />
          </WareProvider>
        );
      case 'MASTER':
        return (
          <>
            <OpRouter />
            <WarehouseRouter />
          </>
        );
      default:
        return <>no office</>
    }
  };

  useLayoutEffect(() => { 
    if (!isAuth) {
      checkAuth();
    }
  }, [isAuth]);
 
  return (
    <div>
          <BrowserRouter>
            <Suspense fallback={<>Loading...</>}>
              <Box sx={{display:{lg:'flex'},justifyContent:'center'}}>
                <DrawerSide />
                <AnimatePresence initial={false}>
                  <Box sx={{width:'100%' }}>
                    {isAuth ? (
                      handleRouter(office)
                      ) : <UnAuthRouter />}
                  </Box>
                </AnimatePresence>
              </Box>
              </Suspense>
           </BrowserRouter>  
      </div>
  );
};

export default App;
