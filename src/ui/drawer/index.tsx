import { AppBar, Dialog, DialogActions, DialogTitle, IconButton, Toolbar, useMediaQuery, useTheme } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Drawer from '@mui/material/Drawer';
import List from '@mui/material/List';
import React, { useContext, useEffect, useState } from 'react';

import Divider from '@mui/material/Divider';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';
import { imageSrc } from '../../App';

import DescriptionOutlinedIcon from '@mui/icons-material/DescriptionOutlined';
import ExitToAppIcon from '@mui/icons-material/ExitToApp';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';
import HomeIcon from '@mui/icons-material/Home';
import MenuIcon from '@mui/icons-material/Menu';
import PeopleAltIcon from '@mui/icons-material/PeopleAlt';
import ShoppingBasketIcon from '@mui/icons-material/ShoppingBasket';
import TranslateIcon from '@mui/icons-material/Translate';
import Collapse from '@mui/material/Collapse';
import Popover from '@mui/material/Popover';
import { useTranslation } from 'react-i18next';
import { BsCardChecklist } from 'react-icons/bs';
import { FaBoxes, FaListAlt } from 'react-icons/fa';
import { GiBoxTrap } from 'react-icons/gi';
import { MdOutlineManageSearch } from 'react-icons/md';
import { RiLuggageDepositFill } from 'react-icons/ri';
import { Link, useNavigate } from 'react-router-dom';
import { AuthContext, OfficeType } from '../../provider/authProvider';


const adminList = [{title: "", icon: <PeopleAltIcon />,root:"/"}];//權限管理
const operatorList = [
     { title: "main", icon: <HomeIcon />,root:"/"  },
    { title: "pickList", icon: <FaListAlt />,root:"/operator/picking"  },
    { title: "treasuryList", icon: <RiLuggageDepositFill />,root:"/operator/treasury" }
];//領料單 繳庫單
// const warehouseList = [
//     { title: "main", icon: <HomeIcon /> ,root:"/"},
//     { title: "purchase", icon: <ShoppingBasketIcon />,root:"/purchase" },
//     { title: "stockIn", icon: <FaBoxes /> ,root:"/stockin"},
//     {title:"order",icon:<BsCardChecklist />,root:"/order"},
//     { title: "stockOut",icon:<GiBoxTrap />,root:"/stockout" },
//     {title:"management",icon:<MdOutlineManageSearch />,root:"/management"}
// ];//進貨 入庫 接單 出庫 庫存管理

const rwdStyle = { xs: "block", sm: "block", md: 'none' };

const drawerWidth = 300;
const drawColor = '#373e47';

interface Props {
  window?: () => Window;
};
interface GroupType { 
    title: string, root: string
};
const GroupMenu = (
    props: {
        anchor:HTMLDivElement | null,
        handleClose: () => void,
        data: GroupType[]
    }) => {
    const navigate = useNavigate();
    const { auth } = useContext(AuthContext);

    const { anchor, handleClose, data } = props;
    
    const { t } = useTranslation();
    return (
        <Popover 
             open={Boolean(anchor)}
            anchorEl={anchor}
            onClose={handleClose}
            anchorOrigin={{
                vertical: 'center',
                horizontal: 'right',
            }}
            transformOrigin={{
                vertical: 'center',
                horizontal: 'left',
            }}
        >
            <List sx={{width:150}}>
                {data.map((ele, index) =>
                    <ListItem key={`group-list-`+index} disablePadding >
                        <ListItemButton key={`group-list-`+index} onClick={()=>navigate(ele.root)} >
                            <ListItemText
                                primaryTypographyProps={{fontWeight:'bold'}} 
                                primary={t(`${auth.office.toLowerCase()}.manage.${ele.title}`)} />
                        </ListItemButton>
                    </ListItem>  
                )}
            </List>
        </Popover>
    );
};

function DrawerSide(props:Props) {
    const {
        auth,logout,changeOffice
    } = useContext(AuthContext);
    const { isAuth,office } = auth;
    const { t, i18n } = useTranslation();
     
    const theme = useTheme();
    const side = useMediaQuery(theme.breakpoints.down('lg'));
    const { window } = props;
    const [open, setOpen] = React.useState(true);
    const [showDrawer, setShowDrawer] = useState<boolean>(false);
    const [remind, setRemind] = useState<boolean>(false);
    const [officeList, setOfficeList]
        = useState<{title: string, icon: any, root: string,group?: GroupType[]}[]>([]);
    const [groupAnchor, setGroupAnchor]
        = useState<HTMLDivElement | null>(null);
  
    const container = window !== undefined ? () => window().document.body : undefined;

    const handleDrawerToggle = () => {
        setShowDrawer(!showDrawer);
    };
    const handleClick = () => {
        setOpen(!open);
    };
    
    const handleLogout = () => { 
        logout();
        setRemind(false);
        return;
    };
    const changeLanguage = (lng:string) => {
        if (lng !== i18n.language) {
            i18n.changeLanguage(lng);
        };
    };

    const handleClickGroup = (event: React.MouseEvent<HTMLDivElement>) => {
        setGroupAnchor(event.currentTarget);
    };

    const warehouseList = [
        { title: "main", icon: <HomeIcon /> ,root:"/"},
        { title: "purchase", icon: <ShoppingBasketIcon />,root:"/purchase" },
        { title: "stockIn", icon: <FaBoxes /> ,root:"/stockin"},
        {title:"order",icon:<BsCardChecklist />,root:"/order"},
        { title: "stockOut",icon:<GiBoxTrap />,root:"/stockout" },
        {
            title: "manage", icon: <MdOutlineManageSearch />, root: "/management",
            group: [
                { title: "combine" ,root:"/management/combine"},
                { title: "change" ,root:"/management/change"},
                { title: "inventor", root: "/management/inventory" },
                { title: "storage", root: "/management/storage" },
            ]
        },
        { title: "file",icon:<DescriptionOutlinedIcon />,root:"/file" },
    ];
   
    const drawer = () => (
        <React.Fragment>
            <Toolbar sx={{bgcolor:"#E67E22",pt:2,pb:2}}>
                <img src={imageSrc} alt="logo" style={{width:'90%'}}/>       
            </Toolbar>
            <Divider />
            <Box sx={{ pl:2,ml:0,mt:2}}>
                <h1 className="office-title">{office}</h1>
                {officeList.length > 0 &&
              
                    <List >
                        {officeList.map((data, index) =>
                            data.group ? (
                                <ListItem key={data.title} >
                                    <ListItemButton sx={{fontSize:25}} onClick={handleClickGroup}>
                                        <ListItemIcon sx={{color:'white'}}>
                                            {data.icon}
                                        </ListItemIcon>
                                        <ListItemText
                                            primaryTypographyProps={{fontWeight:'bold'}} 
                                      
                                            primary={t(`${office.toLowerCase()}.${data.title}.title`)}
                                        />
                                    </ListItemButton>
                                    {data.group&&<GroupMenu anchor={groupAnchor} handleClose={() => setGroupAnchor(null)} data={data.group} />}
                                </ListItem>
                            ): (
                                <ListItem key={data.title} >
                                    <ListItemButton sx={{fontSize:25}} component={Link} to={data.root} >
                                        <ListItemIcon sx={{color:'white'}}>
                                            {data.icon}
                                        </ListItemIcon>
                                            <ListItemText
                                                primaryTypographyProps={{ fontWeight: 'bold' }}
                                                primary={t(`${office.toLowerCase()}.${data.title}.title`)} />
                                    </ListItemButton>
                                </ListItem>
                            )
                        
                        )}
                    </List>
                }    
              </Box>
              <Divider />
              <Box sx={{textAlign:'center'}}>
                <List>
                    <ListItemButton onClick={handleClick}>
                        <ListItemIcon  sx={{color:'white'}}>
                            <TranslateIcon  />
                        </ListItemIcon>
                        <ListItemText primary={t("drawer.language")} />
                        {open ? <ExpandLess /> : <ExpandMore />}
                    </ListItemButton>
                
                    <Collapse in={open} timeout="auto" unmountOnExit>
                        <List component="div" >
                            <ListItemButton sx={{ pl: 4 }} onClick={()=>changeLanguage("en")}>
                                <ListItemText primary={t("drawer.english")} />
                            </ListItemButton>
                            <ListItemButton sx={{ pl: 4 }}  onClick={()=>changeLanguage("tw")}>
                                <ListItemText primary={t("drawer.taiwan")} />
                            </ListItemButton>
                            {/* <ListItemButton sx={{ pl: 4 }}  onClick={()=>changeLanguage("en")}>
                                <ListItemText primary={t("drawer.vietnamese")} />
                            </ListItemButton> */}
                        </List>
                    </Collapse>
                    <ListItemButton onClick={()=>setRemind(true)}>
                        <ListItemIcon>
                            <ExitToAppIcon  sx={{color:'white'}}/>
                        </ListItemIcon>
                        <ListItemText primary={t("drawer.logout")} />
                    </ListItemButton>
                    {office === OfficeType.ADMIN ? (
                        <>
                            <ListItemButton onClick={()=>changeOffice(OfficeType.ADMIN)}>
                                <ListItemText primary={'admin'} />
                            </ListItemButton>
                            <ListItemButton onClick={()=>changeOffice(OfficeType.OPERATOR)}>
                                <ListItemText primary={"operator"} />
                            </ListItemButton>
                            <ListItemButton onClick={()=>changeOffice(OfficeType.WAREHOUSE)}>
                                <ListItemText primary={"warehouse"} />
                            </ListItemButton>
                        </>
                    ):null}
                     
                 
                </List>
            </Box>
        </React.Fragment>
    );

    useEffect(() => {
        if (isAuth) {
            switch (office) {
                case "ADMIN":
                    setOfficeList([]);
                    break
                case "OPERATOR":
                    // setOfficeList([]);
                    setOfficeList(operatorList);
                    break
                case "WAREHOUSE":
                    setOfficeList(warehouseList);
                    break
                case "MASTER":
                    setOfficeList(warehouseList);
                    break
                default:
                    setOfficeList([]);
                    break;
            };
        } else {
            setOfficeList([]);
        };
    }, [isAuth, office]);
   
    return (
        <Box sx={{ display: isAuth ? "block" : "none" }}>
            
            <AppBar
                position="fixed"
                sx={{
                    width: { xl: `calc(100% - ${drawerWidth}px)` },
                    ml: { lg: `${drawerWidth}px` },
                    display: isAuth ?( side ? "block":'none'): "none",
                    bgcolor:"#E67E22"
                }}
            >
                <Toolbar>
                    <IconButton
                        color="inherit"
                        aria-label="open drawer"
                        edge="start"
                        onClick={handleDrawerToggle}
                        sx={{ mr: 2, display: { xl: 'none' } }}
                    >
                        <MenuIcon />
                    </IconButton>
                     <img src={imageSrc} alt="logo" style={{width:100}}/>   
                </Toolbar>
            </AppBar>
            <Box component="nav" sx={{ width: { lg: drawerWidth }, flexShrink: { sm: 0 } }}>
                <Drawer
                    container={container}
                    anchor={"left"}
                    variant="temporary"
                    open={showDrawer}
                    onClose={handleDrawerToggle}
                    ModalProps={{
                        keepMounted: true, // Better open performance on mobile.
                    }}
                    sx={{
                        display: isAuth ? (side?"block":'none'):'none',
                        '& .MuiDrawer-paper': { bgcolor: drawColor, boxSizing: 'border-box', width: drawerWidth ,color:'white'},
                    }}
                >
                    {drawer()}
                </Drawer>
                <Drawer
                    variant="permanent"
                    sx={{
                        display: isAuth ? (side?"none":'block'):'none',
                        '& .MuiDrawer-paper': { bgcolor: drawColor, boxSizing: 'border-box', width: drawerWidth,color:'white' },
                    }}
                    open
                >
                    {drawer()}
                </Drawer>
            </Box>
            <Dialog
                open={remind}
                onClose={() => setRemind(false)}
                id="check-logout-modal"
            >
                <DialogTitle id="alert-dialog-title">
                    {"Are you sure to logout ?"}
                </DialogTitle>
                {/* <DialogContent>
                    <DialogContentText id="alert-dialog-description">
                    Are you sure to logout?
                    </DialogContentText>
                    </DialogContent> */}
                <DialogActions>
                    <Button onClick={() => setRemind(false)} autoFocus variant='outlined'>
                        Cancel
                    </Button>
                    <Button onClick={handleLogout} color="error">Yes</Button>
                  
                </DialogActions>
            </Dialog>
        </Box>  
    );
};

export default DrawerSide;

