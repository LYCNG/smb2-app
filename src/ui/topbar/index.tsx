import React from 'react'
import AppBar from '@mui/material/AppBar';
import {useState,useEffect} from "react";
import Button from '@mui/material/Button';
import IconButton from '@mui/material/IconButton';
import useMediaQuery from '@mui/material/useMediaQuery';
import { Toolbar, Typography } from '@mui/material';
import MenuIcon from '@mui/icons-material/Menu';
import DrawerSide from '../drawer';

const TopBar: React.FC = () => {

    return (
        <AppBar position="static" sx={{display:{xs:"block",sm:'block',md:"none"}}}>
            <Toolbar>
            <IconButton
                size="large"
                edge="start"
                color="inherit"
                aria-label="menu"
                sx={{ mr: 2 }}
            >
                <DrawerSide />
            </IconButton>
            <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                News
            </Typography>
            <Button color="inherit">Login</Button>
            </Toolbar>
        </AppBar>
    )
};

export default  TopBar