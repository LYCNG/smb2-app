import { Navigate, Route, Routes, useLocation } from "react-router-dom";
import AuthManagePage from "./page/authManage";
import Login from "./page/Login";

import { ReactNode } from "react";
import { OperateDepositPage, OperatePickPage } from "./components/operate/index";
import {
    ChangePage, ImportFilePage, InventoryPage, MergePage, OrderPage, PurchasePage,
    StockinPage, StockoutPage, StorageOverview
} from "./components/ware/index";
import ErrorPage from "./page/errorPage";
import MainOperate from './page/main-operate/index';
import MainWare from "./page/main-ware";
import Transition from "./ui/transition";

const setTransition = (element: ReactNode) => <Transition children={element} />;

export const UnAuthRouter = () => (
    <Routes>
        <Route path={"/"} element={<Login  />} />
        <Route path={"/login"} element={<Login  />} />
        <Route path={"*"} element={ <Navigate to="/" />}/>
    </Routes>
);

export const AuthRouter = () => (
    <Routes>
        <Route path={"/"} element={setTransition(<AuthManagePage />)} />
        
         <Route path={"/unauthor"} element={<ErrorPage />} />
    </Routes>
);

export const OpRouter = () => (
    <Routes>
        <Route path={"/"} element={setTransition(<MainOperate/>)} />
        <Route path={"/op"} element={setTransition(<MainOperate />)} />
        <Route path={"/operator/picking"} element={setTransition(<OperatePickPage />)} />
        <Route path={"/operator/treasury"} element={setTransition(<OperateDepositPage />)} />
        <Route path={"/unauthor"} element={<ErrorPage />} />
        <Route path={"*"} element={ <Navigate to="/" />}/>
    </Routes>
);

export const WarehouseRouter = () => {
    const location = useLocation();
    return (

            <Routes key={location.pathname} location={location}>
            <Route path={"/"}  >
                    <Route path="" element={setTransition(<MainWare />)} />
                     <Route path={"/purchase"} element={setTransition(<PurchasePage />)} />
                    {/* <Route path={"/purchase"} element={<StockInComp />} /> */}
                    <Route path={"/stockin"} element={setTransition(<StockinPage />)} />
                    {/* <Route path={"/order"} element={<OrderComp />} /> */}
                    <Route path={"/order"} element={setTransition(<OrderPage />)} />
                    <Route path={"/file"} element={setTransition(<ImportFilePage />)} />
                    <Route path={"/stockout"} element={setTransition(<StockoutPage />)} />
                    <Route path={"/management/combine"} element={setTransition(<MergePage />)} />
                    <Route path={"/management/change"} element={setTransition(<ChangePage />)} />
                    <Route path={"/management/inventory"} element={setTransition(<InventoryPage />)} />
                    <Route path={"/management/storage"} element={setTransition(<StorageOverview />)} />
                    <Route path={"/unauthor"} element={setTransition(<ErrorPage />)} />
                </Route>
                {/* <Route path={"*"} element={ <Navigate to="/" />}/> */}
            </Routes>
      
    );
};