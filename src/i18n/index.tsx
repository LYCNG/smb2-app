import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import Backend from "i18next-http-backend";
import LanguageDetector from 'i18next-browser-languagedetector';
import en from "./locales/en.json";
import tw from "./locales/tw.json";

const resources = {
    en: {
        translation:en
    },
    tw: {
        translation:tw
    },

    "vi": {
        //  translation:vi
    }
};

i18n
    .use(Backend)
    .use(LanguageDetector)
    .use(initReactI18next)
    .init({
        resources,
        lng: localStorage.getItem("i18nextLng")||"en",
        // backend: {
        //     loadPath: '/locales/{{lng}}/{{ns}}.json',
        // },
        // supportedLngs:['en'],
        debug: false,
        fallbackLng: 'en',  
        interpolation: {
            escapeValue: false,
            },
        react: {
            useSuspense: false
            }

});
export default i18n;